import React from 'react';
// import UserContext from '../UserContext';



import {Card,Col, ButtonGroup,  Button} from 'react-bootstrap'
import Swal from 'sweetalert2'

import food1 from "./../images/food/1.jpg"
import food2 from "./../images/food/2.jpg"
import food3 from "./../images/food/3.jpg"
import food4 from "./../images/food/4.jpg"
import food5 from "./../images/food/5.jpg"
import food6 from "./../images/food/6.jpg"
import food7 from "./../images/food/7.jpg"

import { trackPromise } from 'react-promise-tracker';


export default function Cart(props){
	console.log(props)

	const { productProp, fetchData } = props;


	let token = localStorage.getItem('token')

	let subtotal =0
	let subtotalString = "";

	
	const foodImages = [
	  food1,
	  food2,
	  food3,
	  food4,
	  food5,
	  food6,
	  food7
	];

	const randomImage =
    foodImages[Math.floor(Math.random() * foodImages.length)];

	const decQuantity = (productId,quantity) => {
				console.log(productId)
				if (quantity <= 1 ) {
					deleteToggle(productId)
				} else {
					trackPromise(
					fetch(`https://fast-bastion-88049.herokuapp.com/api/orders/decQuantity`,{
						method: "POST",
						headers: {
							"Content-Type": "application/json",
							"Authorization": `Bearer ${token}`
						},
						body: JSON.stringify({
							productId: productId
						})
					})
					.then(result => result.json())
					.then(result => {
						console.log(result)
						fetchData()

						// setProductId(result._id);
						// setName(result.name);
						// setDescription(result.description);
						// setPrice(result.price)
					})
					.catch(error => window.location.replace("./serverError")))

				}
			
				// setShowEdit(true);
			}

	const incQuantity = (productId) => {
				console.log(productId)
			trackPromise(fetch(`https://fast-bastion-88049.herokuapp.com/api/orders/incQuantity`,{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					productId: productId
				})
			})
			.then(result => result.json())
			.then(result => {
				console.log(result)
				fetchData()
				// setProductId(result._id);
				// setName(result.name);
				// setDescription(result.description);
				// setPrice(result.price)
			}).catch(error => window.location.replace("./serverError")))

				// setShowEdit(true);
			}

	const deleteToggle = (productId) => {
		Swal.fire({
					  title: 'Are you sure?',
					  text: "You won't be able to revert this!",
					  icon: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, remove it!'
					}).then((result) => {
					  if (result.isConfirmed) {
					    trackPromise(fetch(`https://fast-bastion-88049.herokuapp.com/api/orders/removeProduct`, 
					    	{
					    		method: "POST",
					    		headers: {
					    			"Content-Type": "application/json",
					    			"Authorization": `Bearer ${token}`
					    		},
					    		body: JSON.stringify({
					    			productId: productId
					    		})
					    	
					    })
					    .then(result => result.json())
					    .then(result => {
					    	console.log(result)
					    	fetchData()
					    	// allProductData();
					    	if(result === true){ 
					    		Swal.fire(
					    		  'Removed!',
					    		  'Product has been removed.',
					    		  'success'
					    		)
					    		
					    	} else {
					    		// allProductData();
					    		Swal.fire({
					    			title: "Something went wrong",
					    			icon: "error",
					    			"text": "Please try again"
					    		})
					    	}
					    }).catch(error => window.location.replace("./serverError")))
					  }
					})
			}

	const checkOut = (totalAmount) => {

					trackPromise(fetch('https://fast-bastion-88049.herokuapp.com/api/orders/checkOut',{ //pang user lang to
						method: "POST",
						headers: {
							"Content-Type": "application/json",
							"Authorization": `Bearer ${token}`
						},
						body: JSON.stringify({
							totalAmount: totalAmount
						})
					})
					.then(result => result.json())
					.then(result => {
						
						Swal.fire(
					    		  'Order Confirmed!',
					    		  'Your order has been placed.',
					    		  'success'
					    		)
						// console.log(result.products[0].name)
						// console.log(result.products[0].description)
						// console.log(result.products[0].price)
						// console.log(result.products[0].name)
						// setProductArray(result.products)
					}).catch(error => window.location.replace("./serverError")))

						// setShowEdit(true);
					}




subtotal = productProp.price*productProp.quantity
subtotalString =  subtotal.toLocaleString("en-US")
console.log(productProp.imageUrl)


return(
	

	<Col xs = {12} className ="pt-3">
	  	<Card className="shadow">
	  		<Card.Header className="wrapEllipsis">{productProp.name}</Card.Header>
	  	  <Card.Body>
	  	  
		  	    {/*<h3 className="text-center ">{productProp.name}</h3>*/}
			  	    {/*<h5>Description:</h5>*/}
			  	    <div class="row align-items-center" id="cartrow1">
				  	        <div class="col-5 col-md-2 col-lg-2">
				  	         	<Card.Img variant="top" style={{height:"5rem",objectFit: "contain"}} src={productProp.imageUrl} />
				  	        </div>
					  	    <div class="col-7 col-md-3 col-lg-3" id="cartrow">
					  	    	<p class="wrapEllipsis">{productProp.description}</p>
					  	    </div>
				  	    {/*<h5>Price:</h5>*/}

				  	    <div class="col-5 col-sm-0 d-block d-sm-none"></div>
					  	    <div class="col-7 col-sm-4 ml-auto pt-3 pt-md-0  col-md-2 col-lg-2 ">
					  	    	<h5 className="push">Php {productProp.price}</h5>
					  	    </div>
					  	    <div class="col-5 col-sm-0 d-block d-sm-none"></div>
					  	    <div class="col-7 col-sm-4 pt-3 pt-md-0 col-md-2 col-lg-2">
					  	    {/*<ButtonGroup>*/}
						  	    <Button className="px-2" variant="outline-secondary" size="sm" 
						  	    onClick={ ()=> decQuantity(productProp.productId,productProp.quantity) }>
						  	    	<i class="fa fa-minus" aria-hidden="true"></i>
						  	    </Button>
						  	    <Button disabled id="quant" className="px-2" variant="outline-secondary" size="sm">
						  	    	{productProp.quantity}
						  	    </Button>
						  	    	
						  	    <Button className="px-2"  variant="outline-secondary" size="sm"
						  	    onClick={ () => incQuantity(productProp.productId)}>
						  	    	<i class="fa fa-plus" aria-hidden="true"></i>
						  	    </Button>
						  	    {/*</ButtonGroup>*/}
					  	    </div>
					  	    <div class="col-5 col-sm-0 d-block d-sm-none"></div>
					  	    <div class="col-7 col-sm-4 pt-3 pt-md-0 col-md-3 col-lg-3" id="sub">
					  	    <p>SubTotal:</p>
					  	    	<h5 >Php {subtotalString}</h5>
					  	    </div>
				  	    
			  	    </div>
			  	    
			  	    <div class="row pull-right">
				  	    <Button className="m-1" id="remove" variant="outline-danger" size="sm" 
				  	    onClick={ ()=> deleteToggle(productProp.productId) }>
				  	    	
				  	    	<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
				  	    	  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
				  	    	  <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
				  	    	</svg>
				  	    </Button>	
				  	</div>
		  	    {/*<Button className="buttonprod" variant="danger" size="lg" block onClick={ ()=> openAdd(productId)} >Add to Cart</Button>*/}
	  	  </Card.Body>
	  	</Card>
	 </Col>


)
}