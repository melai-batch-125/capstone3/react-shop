import React from 'react';

import {
	Row,
	Col,
	Card,
	ListGroupItem,
	ListGroup

} from 'react-bootstrap';

import ship from "./../images/ship.jpg"
import shop from "./../images/shop.jpg"
import del from "./../images/del.jpg"
export default function Highlights() {
  return (
  	/*
  	<Card>
  	  <Card.Body>
	  	    <Card.Title>Card Title</Card.Title>
		  	    <Card.Text>
		  	      Some quick example text to build on the card title and make up the bulk of the card's content.
		  	    </Card.Text>
	  	    <Button variant="primary">Go somewhere</Button>
  	  </Card.Body>
  	</Card>
*/
<Row id="highlights" className = "px-3 justify-content-between d-flex align-items-stretch ">
	<Col xs = {12} >
		<h1 className="text-center  mt-lg-5 delivery" >Delivery Options</h1>
	</Col>
			<Col xs = {12} md = {4} className="pb-5">
				  	<Card bg="dark" text="light" className="cards shadow-lg">
				  	  <Card.Img variant="top" src={del} className="cardimage"/>
				  	  <Card.Body>
				  	    <Card.Title>Delivery</Card.Title>
				  	    <Card.Text>
				  	      We can delivery the products directly to your door. 
				  	    </Card.Text>
				  	  </Card.Body>
				  	  <ListGroup className="list-group-flush">
				  	    <ListGroupItem variant="warning">Lalamove</ListGroupItem>
				  	    <ListGroupItem variant="warning">Angkas</ListGroupItem>
				  	    <ListGroupItem variant="warning">Toktok</ListGroupItem>
				  	  </ListGroup>
				  	</Card>
			</Col>
			<Col xs = {12} md = {4} className="pb-5">
				  	<Card bg="dark" text="light" className="cards shadow-lg">
				  	  <Card.Img variant="top" src={shop} className="cardimage"/>
				  	  <Card.Body>
				  	    <Card.Title>Pick up</Card.Title>
				  	    <Card.Text>
				  	      We are open Monday to Saturday (9:00AM to 6:00PM) for you to pick up your order.
				  	    </Card.Text>
				  	  </Card.Body>
				  	  <ListGroup className="list-group-flush">
				  	    <ListGroupItem variant="warning">Monday (2:00PM - 3:00PM)</ListGroupItem>
				  	    <ListGroupItem variant="warning">Wednesday (1:00PM - 5:00PM)</ListGroupItem>
				  	    <ListGroupItem variant="warning">Saturday (9:00AM - 4:00PM)</ListGroupItem>
				  	  </ListGroup>
				  	</Card>
			</Col>
			<Col xs = {12} md = {4}>
				  	<Card  bg="dark" text="light" className="cards shadow-lg"> 
				  	  <Card.Img variant="top" src={ship} className="cardimage" />
				  	  <Card.Body>
				  	    <Card.Title>Shipping</Card.Title>
				  	    <Card.Text>
				  	      We ship nationwide every Thursday and Saturday.
				  	    </Card.Text>
				  	  </Card.Body>
				  	  <ListGroup className="list-group-flush">
				  	    <ListGroupItem variant="warning">Partas</ListGroupItem>
				  	    <ListGroupItem variant="warning">Ship</ListGroupItem>
				  	    <ListGroupItem variant="warning">Cubao</ListGroupItem>
				  	  </ListGroup>
				  	</Card>
			</Col>



		</Row>



  )
}