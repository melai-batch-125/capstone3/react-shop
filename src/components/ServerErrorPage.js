import React, {useEffect} from 'react';
// import {Container,Row,Col,Jumbotron} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import oops from "./../images/err.jpg"
export default function ServerErrorPage(){
	useEffect(()  => {
	document.body.style.backgroundColor = "#f5f5f5";
	/*document.body.style.backgroundRepeat = "no-repeat";
	document.body.style.backgroundSize = "cover";
	document.body.style.backgroundPosition = "top center";*/
	document.body.style.minWidth = "100vw";
	document.body.style.minHeight = "100vh";


    return () => {
        document.body.style.backgroundColor = "white";
    };
});
	return (
		<div class="center-screen-error">
		<img src={oops} class="robot"/>	
		<h2 class="px-1" style={{ color: '#ff996b' }}>Sorry.. it's not you, it's us.</h2>
						    	<h6 class="px-2" style={{ color: '#999999' }}>Please try to refresh the page to solve the problem.</h6>
			<Link  to={"./"}>
	         <a id="buy" style={{borderRadius: "2rem"}} class="btn btn-primary mt-3" id="back" href="#about" role="button">TRY AGAIN <i class="fa fa-refresh"></i></a>
	          </Link>
			</div>

		)
}