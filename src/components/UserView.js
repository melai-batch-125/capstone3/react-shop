import React, {useState,useEffect} from 'react';

import {Row,Container,Button,Dropdown,DropdownButton,ButtonGroup} from 'react-bootstrap';
import { MDBInput } from "mdbreact"
import Product from './Product';
import emptycart from "./../images/emptycart.png"
export default function UserView({productData}) {
	console.log(productData)
	const [products, setProducts] = useState([]);
	const [fetchDone, setfetchDone] = useState(false)
	const [isSortPrice, setIsSortPrice] = useState(null); //latest first
	const [isSortProductName, setIsSortProductName] = useState(null); //Z-A
	const [filterText, setFilter] = useState("");

	const filterProducts = (event) => {
	    setFilter(event.target.value);
	}
	const filtered = React.useMemo(() => {
	    return productData.filter(product => {
	    	let name = product.name.toLowerCase();
	    	let filterLower = filterText.toLowerCase();
	    	console.log(name)
	    	console.log(name.includes(filterLower))
	      return filterText.length > 0 ? name.includes(filterLower) : true;
	    })
	}, [filterText, productData]);

	const productNameSortInc = () => {
		setIsSortPrice(null);
		setIsSortProductName(true);
	}
	const productNameSortDec = () => {
		setIsSortPrice(null);
		setIsSortProductName(false);
	}
	const priceSortInc = () => {
		setIsSortPrice(false);
		setIsSortProductName(null);
	}
	const priceSortDec = () => { //highest
		setIsSortPrice(true);
		setIsSortProductName(null);
		
	}

	useEffect(()=>{
		if (isSortPrice==false) {
		const productArr = filtered.sort((a, b) => a.price - b.price).map((product)=>{
			if (product.isActive === true ) {
				return <Product key={product._id} productProp={product}/>
			} else {
				return null
			}
		})
		setProducts(productArr);
	} else if (isSortPrice==true) {
		const productArr = filtered.sort((a, b) => b.price - a.price).map((product)=>{
			if (product.isActive === true ) {
				return <Product key={product._id} productProp={product}/>
			} else {
				return null
			}
		})
		setProducts(productArr);
	}
	else if (isSortProductName==true) {
		const productArr = filtered.sort(function(a, b) {
		  var nameA = a.name.toUpperCase(); // ignore upper and lowercase
		  var nameB = b.name.toUpperCase(); // ignore upper and lowercase
		  if (nameA < nameB) {
		    return -1;
		  }
		  if (nameA > nameB) {
		    return 1;
		  }

		  // names must be equal
		  return 0;
		}).map((product)=>{
			if (product.isActive === true ) {
				return <Product key={product._id} productProp={product}/>
			} else {
				return null
			}
		})
		setProducts(productArr);
	}
	else if (isSortProductName==false) {
			const productArr = filtered.sort(function(a, b) {
			  var nameA = b.name.toUpperCase(); // ignore upper and lowercase
			  var nameB = a.name.toUpperCase(); // ignore upper and lowercase
			  if (nameA < nameB) {
			    return -1;
			  }
			  if (nameA > nameB) {
			    return 1;
			  }

			  // names must be equal
			  return 0;
			}).map((product)=>{
				if (product.isActive === true ) {
					return <Product key={product._id} productProp={product}/>
				} else {
					return null
				}
			})
			setProducts(productArr);
		}
	 else if (isSortPrice==null || isSortProductName==null) {
		const productArr = filtered.map((product)=>{
			if (product.isActive === true ) {
				return <Product key={product._id} productProp={product}/>
			} else {
				return null
			}
		})
		setProducts(productArr);
	}

		
		setfetchDone(true)

	},[filtered,isSortPrice,isSortProductName])

	/*const refresh = () => {
		window.location.reload();
	}*/
	return (
		(productData.length===0 && fetchDone === true)
		?<div class="center-screen">
		    		<img src={emptycart} class="emptycart" />	
		    		<h2 class="mt-3" style={{ color: '#e63c49' }}>No products avaible</h2>
		    		<h6 style={{ color: '#5f87bb' }}>Looks like there is no products available yet</h6>

		   			{/*<Link to={"./addProduct"}>
		    			         <a id="buy" style={{borderRadius: "2rem"}} class="btn btn-primary mt-3" href="#about" role="button">ADD PRODUCT <i class="fa fa-long-arrow-alt-right"></i></a>
		    			          </Link>*/}
		    </div>

		:<Container id="containerProduct2">
		<h1 class="text-center avail" >Available Products</h1>
		{/*<Button variant="outline-danger" size="sm" block onClick={ ()=> refresh()} >Try Again</Button>*/}
		<div class="row">
			<div class="col-12 col-md-4">
			{/*<input type="text" maxLength={30} placeholder="Search Facebook Name"  onChange={(e)=>filterProducts(e)} />*/}
		      <MDBInput label="Search a product" icon="search" maxLength={30} onChange={(e)=>filterProducts(e)}/>
		      </div>
		      	<div class="col-12 col-md-8 text-right align-self-center">
		            <ButtonGroup aria-label="buttonSort">
	  	    	        <DropdownButton as={ButtonGroup} title=<i class="fas fa-sort-amount-up"></i> variant="outline-primary" id="bg-nested-dropdown">
	  	    	            <Dropdown.Item eventKey="1" onClick={ ()=> productNameSortInc()}>Product Name (A-Z)</Dropdown.Item>
	  	    	            <Dropdown.Item eventKey="2" onClick={ ()=> productNameSortDec()}>Product Name (Z-A)</Dropdown.Item>
	  	    	            <Dropdown.Item eventKey="3" onClick={ ()=> priceSortInc()}>Price (Lowest First)</Dropdown.Item>
	  	    	            <Dropdown.Item eventKey="4" onClick={ ()=> priceSortDec()}>Price (Highest First)</Dropdown.Item>
	  	    	         </DropdownButton>
	  	    	       </ButtonGroup>
		         </div>
		 </div>
		{/*<div class="input-container">
		<MDBInput label="Search a product" icon="search" maxLength={30} onChange={(e)=>filterProducts(e)}/>
  	    	        
		      </div>*/}
		          {(products.length === 0)
		      	  ?<div class="center-screen">
		      	    		<img src={emptycart} class="emptycart" />	
		      	    		<h5 class="mt-3" style={{ color: '#e63c49' }}>No product found</h5>
		      	    	{/*<h6 style={{ color: '#5f87bb' }}>Looks like you haven't archived <br/>any product in your shop yet</h6>*/}
		      	    </div>
		      	    :
		<Row className = "pt-1 " id="no-gutters">
		
				{products}
			
		</Row>
	}
		</Container>
		)
}