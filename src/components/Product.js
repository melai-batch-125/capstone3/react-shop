import React, {useState,useEffect,useContext} from 'react';
 import {
 	Card,
 	Button,
 	Modal,
 	Form,
 	Col,
 	Image,
 	InputGroup,
 	FormControl
 } from 'react-bootstrap';
import { MDBInput} from "mdbreact"
 import PropTypes from 'prop-types';

//context
import UserContext from './../UserContext';
import { trackPromise } from 'react-promise-tracker';
import Swal from "sweetalert2";
export default function Product({productProp}) {
	// console.log(props.key)
	// let product = props.productProp

	//login
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	//destructure context object
	const {user,setUser} = useContext(UserContext);

	const {name,description,price,_id,imageUrl,stocks} = productProp //why destructing
	const productObj = {_id};
	const productId = productObj._id;

	const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);
	const [randomImage, setRandomImage] = useState("");
	const [count, setCount] = useState(1);
	const [isDisabled, setIsDisabled] = useState(true);

	let token = localStorage.getItem('token')
	// let count = 0

	function login(e) {
		e.preventDefault();
		closeAdd();
		trackPromise(
		fetch('https://fast-bastion-88049.herokuapp.com/api/users/login',
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					username: email,
					password: password
				})

			}
		)
		.then(result => result.json())
		.then(result => {
			console.log(result); //access:token

			if(typeof result.access !== "undefined") 
			{
				localStorage.setItem("token", result.access);
				setUser({email: email});
				localStorage.setItem('email',email);

				setEmail('');
				setPassword('');
				userDetails(result.access);
			} else {
				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "The email address or password you’ve entered is incorrect."
				})
			}


		}).catch(error => window.location.replace("./serverError")))

		const userDetails = (token) => {
			trackPromise(
			fetch('https://fast-bastion-88049.herokuapp.com/api/users/details', 
				{
					method: "GET",
					headers: {
						"Authorization": `Bearer ${token}`
					}
				}
			)
			.then(result => result.json())
			.then(result => {
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				})



				//update
				// setUser({email: email});
				localStorage.setItem('email',email);

				setEmail('');
				setPassword('');
				setShowAdd(false);


			

			}).catch(error => window.location.replace("./serverError")))
		}	

	}


/*	//end of login

	const openEdit = (productId) => {
			
		}
*/
	const closeEdit = () => {

		setShowEdit(false);
		/*setName("")
		setDescription("")
		setPrice(0)*/
	}
	const cancelEdit = () => {

		setShowEdit(false);
		setCount(1)
	}

	/*edit product function*/
	const editProduct = (e, productId,quantity) => {
		closeEdit();
		e.preventDefault()

		// console.log(name)
		 console.log(productId);
		 // window.location.reload();
		 
		 trackPromise(
		fetch('https://fast-bastion-88049.herokuapp.com/api/orders/addToOrder',{ 
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
			productId: productId,
			quantity: quantity
		})
		})
		.then(result => result.json())
		.then(result => {
			
			if(typeof result !== "undefined"){
			Swal.fire({
				title: 'Added!',
				icon: 'success',
				text: 'Added to Cart Successfully!'
			}).then((result) => {window.location.reload(false);

  			
			
})


			} else {

				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Something went wrong!"
				})
			}

		}).catch(error => window.location.replace("./serverError")))

		
	}

    const openAdd = () => {
    	if (localStorage.getItem('email') ===null) {
    		setShowAdd(true);
    	} else {
    		setShowEdit(true);
    	}
    }


	const closeAdd = () => setShowAdd(false);



	useEffect(()=>{
		if (count>1) 
		{
			setIsDisabled(false)
		} else if (count===1) {
			setIsDisabled(true)
		}
	},[count])

	const [windowDimension, setWindowDimension] = useState(null);

	  useEffect(() => {
	    setWindowDimension(window.innerWidth);
	  }, []);

	  useEffect(() => {
	    function handleResize() {
	      setWindowDimension(window.innerWidth);
	    }

	    window.addEventListener("resize", handleResize);
	    return () => window.removeEventListener("resize", handleResize);
	  }, []);

	  const isMobile = windowDimension <= 768;
	  const isMobileXS = windowDimension <= 320;

	    const maxStock = (e) => {
	  		const re = /^[1-9][0-9]*$/;
	        if (e.target.value === '' || re.test(e.target.value)) {
	        	if (e.target.value > stocks) {
	           setCount(stocks)
	        } else 
	        {
	           setCount(e.target.value)
	        }
	        }
	    }
	   

	return (

		<Col xs = {6} sm = {4}  lg = {3} className ="columnProduct p-sm-3 p-1  d-flex align-items-stretch">
		{/*modal ng quantity and description of product*/}
			{(!isMobile)
				?<Card className="shadow"  id="productCard" >
					{/*<div class="embed-responsive"> */}
					{/*embed-responsive-16by9*/}
					{(stocks===0)
					?<div class="card-img-caption">
					    <p class="card-text">Out of Stock</p>
					    <Card.Img className="img-fluid" style={{height:"11rem",objectFit: "contain"}} variant="top" src={imageUrl}/>
					  </div>

					  :<Card.Img className="img-fluid" style={{height:"11rem",objectFit: "contain"}} variant="top" src={imageUrl}/>
		  		}
		  		{/*</div>*/}
		  		<Card.Body>
		  		<Card.Title id="price">₱{price.toFixed(2)}</Card.Title>
		  		    <Card.Subtitle className="mb-2 wrapEllipsis" id="prodName">{name}</Card.Subtitle>
		  		   <Card.Text className="pull-right mb-2" style={{fontSize:"1rem"}}>{stocks} left</Card.Text>
						  		      	
		  		    </Card.Body>

		  	  <Card.Footer id="cardFooter">
		  	  	<Button className="buttonprod" variant="success" disabled={(stocks===0) ?true :false} size="md" block onClick={ ()=> openAdd()} ><i class="fa fa-plus" aria-hidden="true"></i> Add to Cart</Button>
		  	  </Card.Footer>
		  	</Card>
				:<Card  onClick={ ()=> openAdd(productId)} style={{ cursor: "pointer" }} className="shadow"  id="productCardMobile" >
				
		  		
		  		{(stocks===0)
					?<div class="card-img-caption">
					    <p class="card-text">Out of Stock</p>
					    <Card.Img className="img-fluid" style={{height:"6.5rem",objectFit: "contain"}} variant="top" src={imageUrl}/>
					  </div>

					  :<Card.Img className="img-fluid" style={{height:"6.5rem",objectFit: "contain"}} variant="top" src={imageUrl}/>
		  		}
		  		<Card.Body>
		  		<Card.Title id="price">₱{price.toFixed(2)}</Card.Title>
		  		    <Card.Subtitle className="mb-2 wrapEllipsis" id="prodName">{name}</Card.Subtitle>
		  		    <Card.Text className="pull-right mb-2" style={{fontSize:"0.8rem"}}>{stocks} left</Card.Text>
		  		    </Card.Body>

		  	  <Card.Footer id="cardFooter">
		  	  {(!isMobileXS) 
		  	  	? <Button className="buttonprod" variant="success" size="sm" disabled={(stocks===0) ?true :false} block onClick={ ()=> openAdd()} ><i class="fa fa-plus" aria-hidden="true"></i> Add to Cart</Button>
		  	  	:<Button className="buttonprod" variant="success" size="sm" disabled={(stocks===0) ?true :false} block onClick={ ()=> openAdd()}>Add to Cart</Button>
		  	  }
		  	  	
		  	  </Card.Footer>
		  	</Card>
			}
		  	
		  	<Modal style={{ overlay: { background: 'black' } }} centered show={showAdd} onHide={closeAdd}>
		  	       <div class=" p-5  shadow-lg" >
		  				<h3 className="text-center pb-2">Login</h3>
		  				<Form  onSubmit={(e) => login(e)}>
		  				<MDBInput label="Email address/FB Name" icon="user" maxLength={50} onChange={(e)=>setEmail(e.target.value)}/>
		  				  <MDBInput label="Password" icon="lock" maxLength={20} type="password" validate onChange={(e)=>setPassword(e.target.value)}/>
		  				  <div className="text-center pt-3">
		  				  <Button id="buttonlog" variant="primary" type="submit" >
		  				    Login
		  				  </Button>
		  				  <p className="font-small grey-text  pt-2">
		  				                  Don't have an account?
		  				                  <a
		  				                    href="/register"
		  				                    className="dark-grey-text font-weight-bold ml-1"
		  				                  >
		  				                    Sign up
		  				                  </a>
		  				                </p>
		  				  </div>
		  				</Form>
		  				
		  			</div>
  	      </Modal>
  	      {/*Edit Product Modal*/}
  	      	<Modal centered  show={showEdit} size="md" onHide={cancelEdit} >
  	      		<Form onSubmit={ (e) => editProduct(e, productId, count) }>
  	      			<Modal.Header>
  	      				<Modal.Title className="wrapEllipsis">{name}</Modal.Title>
  	      			</Modal.Header>
  	      			<Modal.Body className="scrollbar scrollbar-dusty-grass " style={{'max-height': 'calc(100vh - 300px)', 'overflow-y': 'auto', scrollbarWidth: 'thin' }}>
  	      			<Image style={{height:"15rem",objectFit: "contain"}}  src={imageUrl} rounded fluid className="photo" />
  	      			<h4 id="price" class="mt-3 text-center">₱{price.toFixed(2)}</h4>
  	      			
  	      			<h4 class="mt-3">Description: <br/></h4>
  	      			{description}<br/>
  	      			
  	      			{/* Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et illum corporis provident consequatur adipisci minus, illo quibusdam deserunt delectus officiis quas. Dignissimos nemo beatae tenetur earum repellendus impedit, excepturi alias
    Lorem ipsum, dolor, sit amet consectetur adipisicing elit. Neque nisi, et consequuntur iusto quas mollitia quos fugiat, corporis doloremque voluptas dolor dolorum sunt amet optio autem sed maiores accusantium numquam.
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa, libero error velit iusto in quasi deserunt inventore excepturi distinctio fugit vero, accusamus sed eligendi rerum, magnam dicta beatae voluptas totam.
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias debitis et, qui necessitatibus aliquid, culpa nostrum soluta sunt reiciendis, labore non animi possimus, quaerat tempora dicta blanditiis impedit vel veniam!*/}
    
  	      				
  	      				</Modal.Body>
  	      				<Modal.Body id="quantBody">
  	      				<div class="row">
  	      					<div class="col-5 col-md-7">
  	      						<h4>Quantity: </h4>
  	      					</div>
  	      					
  	      					<div class="col-7 col-md-5 text-right">
  	      					<InputGroup className="">
  	      					    <Button variant="outline-secondary" id="button-addon1" disabled={isDisabled} onClick={ ()=> setCount(count-1) }>
  	      					      <i class="fa fa-minus" aria-hidden="true"></i>
  	      					    </Button>
  	      					     <Form.Control
  	      					   	type="number"
  	      					   	value={count}
  	      					   	id="quantForm"
  	      					   	required
  	      					   	onChange={ (e)=> maxStock(e)}
  	      					   />
  	      					 
  	      					    <Button variant="outline-secondary" id="button-addon2" disabled={(count===stocks) ?true :false} onClick={ ()=> setCount(parseInt(count)+1) }>
  	      					      <i class="fa fa-plus" aria-hidden="true"></i>
  	      					    </Button>
  	      					  </InputGroup>

  	      						{/*<Button className="px-2" variant="outline-secondary" size="sm" 
  	      						onClick={ ()=> setCount(count-1) }>
  	      							- setCount(e.target.value)
  	      						</Button>
  	      						<Button disabled id="quant" className="px-2" variant="outline-secondary" size="sm">
  	      							{count}
  	      						</Button>
  	      							
  	      						<Button className="px-2"  variant="outline-secondary" size="sm"
  	      						onClick={ () => setCount(count+1) }>
  	      							+
  	      						</Button>*/}
  	      					</div>
  	      				</div>
  	      				<div class="row mt-3">
  	      				<div class="col-5 col-md-7">
  	      						<h5>Subtotal: </h5>
  	      					</div>
  	      					<div class="col-7 col-md-5 text-right">
  	      					<h5>₱{price*count} </h5>
  	      					</div>
  	      				</div>

  	      				
  	      				</Modal.Body>
  	      			<Modal.Footer  style={{borderBottom: '0 none'}}>
  	      				<Button variant="secondary" onClick={cancelEdit}>Cancel</Button>
  	      				<Button variant="success" type="submit"><i class="fa fa-plus" aria-hidden="true"></i> Add to Cart</Button>
  	      			</Modal.Footer>
  	      		</Form>
  	      	</Modal>
  	      
		 </Col>
		
		)
}

Product.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}