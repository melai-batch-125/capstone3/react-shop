import React, {useContext,useEffect,useState} from 'react';


import UserContext from './../UserContext';


import {Nav, Navbar,Badge,Container} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import logo from "./../images/logo.png";
import logowhite from "./../images/logowhite.png";
import { trackPromise } from 'react-promise-tracker';
/*app navbar*/

export default function AppNavbar(){

  // console.log(props);

  const {user,unsetUser} = useContext(UserContext);
  // const [cartCount, setCartCount] = useState(0);
  // const [isMobile, setIsMobile] = useState(false);
  const loginEmail = localStorage.getItem('email')
  // console.log(user)

  const [navbar,setNavbar] = useState(false);

  // let history = useHistory();

  const [windowDimension, setWindowDimension] = useState(null);

    useEffect(() => {
      setWindowDimension(window.innerWidth);
    }, []);

    useEffect(() => {
      function handleResize() {
        setWindowDimension(window.innerWidth);
      }

      window.addEventListener("resize", handleResize);
      return () => window.removeEventListener("resize", handleResize);
    }, []);

    const isMobile = windowDimension <= 768;



  const logout = () => {
    unsetUser();
    // Navbar.Collapse.collapse('hide');
    window.location.href='/login';
    // router.history.push(href);
  }

  let token = localStorage.getItem('token')
  const fetchData = () => {

    trackPromise(
    fetch('https://fast-bastion-88049.herokuapp.com/api/orders/single',{ 
      method: "POST",
      headers: {
        "Authorization": `Bearer ${token}`
      }
    })
    .then(result => result.json())
    .then(result => {
      console.log(result)
      // console.log(result.products[0].name)
      // console.log(result.products[0].description)
      // console.log(result.products[0].price)
      // console.log(result.products[0].name)
      // setProducts(result.products)
      // setCartCount(result.products.length)
      localStorage.setItem("cartCount", result.products.length);
    }).catch(error => window.location.replace("./serverError"))
    );
  }
//should not have cart count on admin account
  useEffect( () => {
    if (loginEmail !== null) {
       if (user.isAdmin !== true){
        fetchData()}
    }
  
    
  }, [loginEmail,user.isAdmin])

  let cartCount = window.localStorage.getItem('cartCount')
  // console.log(cartCount)

  if (cartCount===null) {
    cartCount = 0;
  } else {
    cartCount = window.localStorage.getItem('cartCount')
  }
  let leftNav = (user.id !== null || loginEmail !==null ) 
      ? 
          (user.isAdmin === true) ?
            (isMobile===false) ?
                    <Nav> {/*id parepareho ah*/}
                      <Nav.Link as={NavLink} to="/history" id="nav3">Orders</Nav.Link>
                      <Navbar.Text className="navline">|</Navbar.Text>
                      <Nav.Link as={NavLink} to="/users" id="nav3">Users</Nav.Link>
                      <Navbar.Text className="navline">|</Navbar.Text>
                     {/* <Nav.Link as={NavLink} to="/addProduct" id="nav3">Add Product</Nav.Link>
                      <Navbar.Text className="navline">|</Navbar.Text>*/}
                      <Nav.Link onClick={logout} as={NavLink} to="/login" id="nav4">Logout</Nav.Link>
                    </Nav>
                    :<Nav className="text-center">
                      <Nav.Link as={NavLink} to="/history" id="nav3" className="text-center"><i class="fa fa-3x fa-cart-arrow-down" aria-hidden="true"></i><br/>Orders</Nav.Link>
                      <Nav.Link as={NavLink} to="/users" id="nav3"><i class="fa fa-3x fa-users" aria-hidden="true"></i><br/>Users</Nav.Link>
                      {/*<Nav.Link as={NavLink} to="/addProduct" id="nav3"><i class="fa fa-3x fa-cart-plus" aria-hidden="true"></i><br/>Add Product</Nav.Link>*/}
                      <Nav.Link onClick={logout} as={NavLink} to="/login" id="nav4"><i class="fa fa-3x fa-sign-out-alt" aria-hidden="true"></i><br/>Logout</Nav.Link>
                    </Nav>
          :
            (isMobile===false) ?
                <Nav>
                   <Nav.Link as={NavLink} to="/cart">Cart <Badge className="badgeAll" pill>{cartCount}</Badge></Nav.Link>
                   <Navbar.Text className="navline">|</Navbar.Text>
                   {/*<Nav.Link href="/history">History</Nav.Link>
                   <Navbar.Text className="navline">|</Navbar.Text>*/}
                   <Nav.Link as={NavLink} to="/profile">Profile</Nav.Link>
                   <Navbar.Text className="navline">|</Navbar.Text>
                   
                  <Nav.Link onClick={logout} as={NavLink} to="/login" className="navbar-collapse" data-toggle="collapse" data-target=".navbar-collapse.show" id="nav5">Logout</Nav.Link>
                  
                </Nav>
                : <Nav className="text-center">
                   <Nav.Link activeClassName="active" as={NavLink} to="/cart" ><i style={{position:'relative',right:"-0.4rem"}} class="fa fa-3x fa-shopping-cart pl-1" aria-hidden="true"></i><sup class="align-top" style={{fontSize:"0.6rem"}}><Badge className="badgeSup" pill>{cartCount}</Badge></sup><br/>Cart</Nav.Link>
                   {/*<Navbar.Text className="navline">|</Navbar.Text>*/}
                   {/*<Nav.Link href="/history"><i class="fa fa-3x fa-history" aria-hidden="true"></i><br/>History</Nav.Link>*/}
                   <Nav.Link activeClassName="active" as={NavLink} to="/profile"><i class="fa fa-3x fa-user" aria-hidden="true"></i><br/>Profile</Nav.Link>
                   {/*<Navbar.Text className="navline">|</Navbar.Text>*/}
                   
                  <Nav.Link activeClassName="active" onClick={logout} as={NavLink} to="/login" id="nav5"><i class="fa fa-3x fa-sign-out-alt" aria-hidden="true"></i><br/>Logout</Nav.Link>
                  
                </Nav>
      :
        (isMobile===false) ?
          <Nav>
              <Nav.Link as={NavLink} to="/register" id="nav6">Sign Up</Nav.Link>
              <Navbar.Text className="navline">|</Navbar.Text>
              <Nav.Link as={NavLink} to="/login" id="nav7">Login</Nav.Link>
            </Nav>
            :<Nav>
              <Nav.Link as={NavLink} to="/register" activeClassName="active" id="nav6" className="text-center"><i class="fa fa-3x fa-user-plus" aria-hidden="true"></i><br/>Sign Up</Nav.Link>
              {/*<Navbar.Text className="navline">|</Navbar.Text>*/}
              <Nav.Link as={NavLink} to="/login" activeClassName="active" id="nav7" className="text-center"><i class="fa fa-3x fa-sign-in-alt" aria-hidden="true"></i><br/>Login</Nav.Link>
            </Nav>

        

        let shopName = (user.id !== null || loginEmail !==null ) 
            ? 
                (user.isAdmin === true) ?
                  <Navbar.Brand href="/" id="navtitle">Pick Me Shop <i class="fa fa-lg fa-lock" aria-hidden="true"></i></Navbar.Brand>
                :
                  <Navbar.Brand href="/" id="navtitle">Pick Me Shop</Navbar.Brand>
            :
              (
                <Navbar.Brand href="/" id="navtitle">Pick Me Shop</Navbar.Brand>

              )
        const changeBackground =() => {
          if (window.scrollY >= 20) {
            setNavbar(true);
          } else {
            setNavbar(false);
          }

        }

        window.addEventListener('scroll',changeBackground);

  return (
    // bg="danger" variant="dark" fixed="bottom"
    (isMobile===false) 
    ? 
    <Navbar collapseOnSelect expand="lg"  sticky="top"  className={navbar ? "navtext active" : "navtext"}>
      
      <img class="mx-1 navlogo" src={logo} width="50px"/>

      {/*<Navbar.Brand href="/" id="navtitle">Pick Me Shop</Navbar.Brand>*/}
      {shopName}
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse className="navbar-collapse" id="responsive-navbar-nav" data-toggle="collapse" data-target=".navbar-collapse.show">       
        <Nav className="ml-auto">
         <Nav.Link  as={NavLink} to="/products"  id="nav2">Shop</Nav.Link>
         <Navbar.Text className="navline">|</Navbar.Text>
          {leftNav}
        </Nav>
      </Navbar.Collapse>
     
    </Navbar>
    :<>
  <Navbar fixed="bottom" id="mobileBar" >
    <Container>
    <Nav.Link href="/" className="p-0"><a href="/"><img class="mx-1 navlogo" src={logo} width="50px"/></a></Nav.Link>
      

    <Nav className="me-auto" id="mobileSticky" >
    <Nav.Link as={NavLink} to="/products" activeClassName="active" className="text-center"  id="nav2"><i class="fa fa-3x fa-shopping-bag" aria-hidden="true"
    ></i><br/>Shop</Nav.Link>
    {leftNav}
     {/* <Nav.Link href="#home" className="text-center"><i class="fa fa-3x fa-home fa-lg"></i><br/>
    Hom</Nav.Link>
      <Nav.Link href="#features">Features</Nav.Link>
      <Nav.Link href="#pricing">Pricing</Nav.Link>*/}
    </Nav>
    </Container>
  </Navbar>

</>

    )
}

