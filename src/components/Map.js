import React from 'react';
import Iframe from 'react-iframe'

import {
	Row,
	Col
} from 'react-bootstrap';



export default function Map() {
  return (

    
    <Row className=" justify-content-center" id="map">
    <Col  xs={12} md={4} lg={4} className=" pt-5 pt-xs-1">
    	<h2 class="text-center">You can come to our shop!</h2>
    	<p>Pick Me Retailers and Resellers Shop</p>
      <p><i class="fas fa-map-marker-alt fa-lg" style={{color:"red"}}></i> Blk 2 Lot 38, Rosaflor Subdivision, Brgy. Tagapo, Santa Rosa, Laguna</p>
      <p><i class="far fa-calendar-check fa-lg"></i> We are open Monday to Saturday.</p>
      <p>8:00 AM to 5:00 PM ONLY</p>
      <p>Closed every Sunday.</p>
      <h5>Contact us here:</h5>
      <p><i class="fas fa-mobile-alt fa-lg" aria-hidden="true"></i> 09182255313</p>
      <p><i class="fas fa-mobile-alt fa-lg" aria-hidden="true"></i> 09673693852</p>
      <p><i class="fas fa-phone-alt fa-lg"></i> 049-2530325</p>
      <p><i class="fab fa-facebook-square fa-lg"></i><a href="https://www.facebook.com/pickmeretailerandresellershop" target="_blank"> Pick Me Retailers and Resellers Shop</a></p>
    </Col>
    <Col xs= {12} md={8} lg={7} className=" pb-5 mt-5 mt-xs-1 mapplace" >
    	<Iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3755.77764107611!2d121.09767678263384!3d14.321288003195978!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x72d796c0175944a!2sPick%20Me%20Retailers%20and%20Resellers%20Shop!5e0!3m2!1sen!2sph!4v1632957713680!5m2!1sen!2sph" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"/>
    </Col>
    
    </Row>

  )
}