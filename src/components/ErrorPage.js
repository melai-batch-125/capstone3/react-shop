import React from 'react';
// import {Container,Row,Col,Jumbotron} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import robot from "./../images/robot.png"

export default function ErrorPage(){

	return (
		<div class="center-screen">
		<img src={robot} class="robot"/>	
			<Link to={"./"}>
	         <a id="buy" style={{borderRadius: "2rem"}} class="btn btn-primary mt-3" id="back" href="#about" role="button">GO BACK HOME <i class="fa fa-long-arrow-alt-right"></i></a>
	          </Link>
			</div>

		)
}