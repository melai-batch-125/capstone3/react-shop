import React, {useState,useEffect} from 'react';

// import {Row,Container} from 'react-bootstrap';

import Cart from './Cart';

import {Card,Row, Button,Modal, Form} from 'react-bootstrap'
import Swal from 'sweetalert2'
// import food7 from "./../images/food/7.jpg"
import { Link } from 'react-router-dom'
import { trackPromise } from 'react-promise-tracker';
export default function UserCart(props){
	console.log(props)

	const { productData, fetchData } = props;
	const [displayprod, setDisplayprod] = useState([]);
	const [totalState, setTotalState] = useState(0);
	const [showEdit, setShowEdit] = useState(false);
	let token = localStorage.getItem('token')
	let total = 0 

	useEffect(()=>{
		const list = productData.map( (element) => {
			// console.log(product)

			total = total + (element.price*element.quantity)
			return <Cart key={element._id} productProp={element} fetchData={fetchData}/>
			

		})
		setTotalState(total)
		setDisplayprod(list)

		// fetchData()


	},[productData])
	const openEdit = () => {
			/*console.log(productId)
		trackPromise(fetch(`https://fast-bastion-88049.herokuapp.com/api/products/single`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				id: productId
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			setProductId(result._id);
			setName(result.name);
			setDescription(result.description);
			setPrice(result.price)
		}).catch(error => window.location.replace("./serverError")))
*/
			setShowEdit(true);
		}
	const closeEdit = () => {

		setShowEdit(false);
		/*setName("")
		setDescription("")
		setPrice(0)*/
	}

	const totalValue = (total) => {

		localStorage.setItem('total',total);
	}

	const checkOut = (totalAmount) => {
		setShowEdit(false);
		trackPromise(
					fetch('https://fast-bastion-88049.herokuapp.com/api/orders/checkOut',{ 
						method: "POST",
						headers: {
							"Content-Type": "application/json",
							"Authorization": `Bearer ${token}`
						},
						body: JSON.stringify({
							totalAmount: totalAmount
						})
					})
					.then(result => result.json())
					.then(result => {
						console.log(result)
						localStorage.setItem("cartCount", 0);
						Swal.fire(
					    		  'Order Confirmed!',
					    		  'Your order has been placed.',
					    		  'success'
					    		).then((result) => {window.location.reload(false);})
						// console.log(result.products[0].name)
						// console.log(result.products[0].description)
						// console.log(result.products[0].price)
						// console.log(result.products[0].name)
						// setProductArray(result.products)
					}).catch(error => window.location.replace("./serverError")))

						// setShowEdit(true);

					}

	return (
		
		<div>
			<div class="row">
				<div class="col-12 col-md-6">
					<h2>Shopping Cart</h2>
				</div>
				<div class="col col-md-6">
				<Link to={"./history"}>
						<div className="d-flex justify-content-end mb-2">
							<Button className="shadow" variant="outline-primary" >View Purchase History</Button>
						</div></Link>
				</div>
			</div>
				<Row className = "px-3 pt-3">
					
						{displayprod}
					
					<div class = "footers">
						  	<Card className="shadow" border="danger">			  		
						  	  <Card.Body>
						  	  <div class="text-right">
						  	  	<h5  class="pr-3 m-auto">Total: Php {totalState.toLocaleString("en-US")}
						  	  	<Link to={"./placeOrder"}>
						
							<Button style={{display: "inline-block"}} id="checkButton" className="ml-3 p-3 " variant="danger" size="sm" 
										  	    	/*onClick={ ()=> checkOut(totalState) }*/
										  	    	/*onClick={()=> openEdit()}*/
										  	    	onClick={()=> totalValue(totalState)}>
										  	    		Checkout
										  	    	</Button>
						</Link>
						  	  		
						  	  	</h5>
						  	  	
						  	  </div>
								  	    
								  	    
								  	    
							  	    {/*<Button className="buttonprod" variant="danger" size="lg" block onClick={ ()=> openAdd(productId)} >Add to Cart</Button>*/}
						  	  </Card.Body>
						  	</Card>

					</div>
				</Row>
				{/*Edit Product Modal*/}
			<Modal centered show={showEdit} onHide={closeEdit}>
				<Form /*onSubmit={ (e)=> checkOut(totalState) }*/>
					<Modal.Header>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
					{totalState}
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success"/* type="submit"*/ onClick={()=> checkOut(totalState)}>Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
				</div>
		
		




		)








	}
