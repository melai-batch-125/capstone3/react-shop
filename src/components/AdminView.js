import React, {useState, useEffect} from 'react'

import {Table, Button, Modal, Form, Row, Col,Image,Tabs, Tab,Card,Badge} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2';
import { trackPromise } from 'react-promise-tracker';
import emptycart from "./../images/emptycart.png"
import { MDBInput } from "mdbreact"
// import '@fortawesome/fontawesome-free/css/all.min.css';
// import 'bootstrap-css-only/css/bootstrap.min.css';
// import 'mdbreact/dist/css/mdb.css';
export default function AdminView(props){

	const { productData, allProductData } = props;
	const [productId, setProductId] = useState('');
	const [products, setProducts] = useState([]);
	const [productsMobileActive, setProductsMobileActive] = useState([]);
	const [productsMobileInactive, setProductsMobileInactive] = useState([]);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);

	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);
	const [isDisabled, setIsDisabled] = useState(true);
	const [isDisabledEdit, setIsDisabledEdit] = useState(true);

	const [image, setImage ] = useState("");
	const [ url, setUrl ] = useState("");

	let token = localStorage.getItem('token');

	const [filterText, setFilter] = useState("");

	const filterProducts = (event) => {
	    setFilter(event.target.value);
	}
	const filtered = React.useMemo(() => {
	    return productData.filter(product => {
	    	let name = product.name.toLowerCase();
	    	let filterLower = filterText.toLowerCase();
	    	console.log(name)
	    	console.log(name.includes(filterLower))
	      return filterText.length > 0 ? name.includes(filterLower) : true;
	    })
	}, [filterText, productData]);

	const [windowDimension, setWindowDimension] = useState(null);

	  useEffect(() => {
	    setWindowDimension(window.innerWidth);
	  }, []);

	  useEffect(() => {
	    function handleResize() {
	      setWindowDimension(window.innerWidth);
	    }

	    window.addEventListener("resize", handleResize);
	    return () => window.removeEventListener("resize", handleResize);
	  }, []);

	  const isMobile = windowDimension <= 768;


	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);


	const openEdit = (productId) => {
			console.log(productId)
		trackPromise(fetch(`https://fast-bastion-88049.herokuapp.com/api/products/single`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				id: productId
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			setProductId(result._id);
			setName(result.name);
			setDescription(result.description);
			setPrice(result.price)
			setStocks(result.stocks)
		}).catch(error => window.location.replace("./serverError")))

			setShowEdit(true);
		}

	const closeEdit = () => {

		setShowEdit(false);
		setName("")
		setDescription("")
		setPrice(0)
	}
	const cancelAdd = () => {

		setShowAdd(false);
		setIsDisabled(true);
		setName("")
		setDescription("")
		setPrice(0)
		setImage("");
	}

    const numberOnly = (e) => {
			 const re = /^[0-9\b]+$/;
	      if (e.target.value === '' || re.test(e.target.value)) {
	         setPrice(e.target.value)
	      }
    }
    const numberOnlyStocks = (e) => {
			 const re = /^[0-9\b]+$/;
	      if (e.target.value === '' || re.test(e.target.value)) {
	         setStocks(e.target.value)
	      }
    }

	useEffect( () => {



		const productsArr = filtered.map( (product) => {
			console.log(product)
			return(
			
				<tr key={product._id} className="text-center">
					<td><Image style={{height:"4rem",objectFit: "contain"}}  src={product.imageUrl} rounded fluid className="photo" /></td>
					<td className="wrapEllipsis">{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>
						{
							(product.isActive === true) ?
								<span>Available</span>
							:
								<span>Unavailable</span>
						}
					</td>
					<td className="buttons">
						
							<Button className="m-1" variant="outline-primary" size="sm" 
							onClick={ ()=> openEdit(product._id) }>
								<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							</Button>
							{
								(product.isActive === true) ?
									<Button className="m-1"  variant="outline-dark" size="sm"
									onClick={()=> archiveToggle(product._id, product.isActive)}>
										<i class="fa fa-ban" aria-hidden="true"></i>
									</Button>
								:
									<Button className="m-1"  variant="outline-success" size="sm"
									onClick={ () => unarchiveToggle(product._id, product.isActive)}>
										<i class="fa fa-unlock" aria-hidden="true"></i>
									</Button>
							}
							<Button className="m-1"  variant="outline-danger" size="sm"
							onClick={ () => deleteToggle(product._id)}>
								<i class="fa fa-trash" aria-hidden="true"></i>
							</Button>
						

						

					</td>
				</tr>
		)
		})
		const productsArrMobileActive = filtered.filter(elem => elem.isActive === true).map( (product) => {
			console.log(product)
			return(
<Col xs = {6} sm = {4}  lg = {3} className ="columnProduct p-sm-3 p-1  d-flex align-items-stretch">
		
				<Card  /*onClick={ ()=> openEdit(product._id)}*/ style={{ cursor: "pointer" }} className="shadow"  id="productCardMobile" >
								{(product.stocks===0)
					?<div class="card-img-caption">
					    <p class="card-text">Out of Stock</p>
					    {(!isMobile)
					  		?<Card.Img className="img-fluid" style={{height:"10rem",objectFit: "contain"}} variant="top" src={product.imageUrl} />
					  		:<Card.Img className="img-fluid" style={{height:"6.5rem",objectFit: "contain"}} variant="top" src={product.imageUrl} />
					  	
					    }
					  </div>

					  :(!isMobile)
						  		?<Card.Img className="img-fluid" style={{height:"10rem",objectFit: "contain"}} variant="top" src={product.imageUrl} />
						  		:<Card.Img className="img-fluid" style={{height:"6.5rem",objectFit: "contain"}} variant="top" src={product.imageUrl} />
						  	
		  		}
						  	
						  		<Card.Body>
						  		<Card.Title id="price">₱{product.price.toFixed(2)}</Card.Title>
						  		    <Card.Subtitle className="mb-2 wrapEllipsis" id="prodName">{product.name}</Card.Subtitle>
						  		    {(!isMobile)
						  		      		?  <Card.Text className="pull-right" style={{fontSize:"1rem"}}>{product.stocks} left</Card.Text>
						  		      		:  <Card.Text className="pull-right" style={{fontSize:"0.8rem"}}>{product.stocks} left</Card.Text>
						  		      	}
						  		    </Card.Body>

						  	{/*  <Card.Footer id="cardFooter">
						  	  	<Button className="buttonprod" variant="success" size="sm" block onClick={ ()=> openEdit()} ><i class="fas fa-edit"></i> Edit</Button>
						  	  </Card.Footer>*/}
<Card.Footer id="cardFooter" className="d-flex justify-content-around align-items-center">
						  	{/* <div class="text-center row">
						  	    <div class="col">*/}
		  	    	  	    		<Button className="m-1" variant="outline-primary" size="sm" 
		  	    	  	    		onClick={ ()=> openEdit(product._id) }>
		  	    	  	    			<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
		  	    	  	    		</Button>
		  	    	  	    		
		  	    	  	    				<Button className="m-1"  variant="outline-dark" size="sm"
		  	    	  	    				onClick={()=> archiveToggle(product._id, product.isActive)}>
		  	    	  	    					<i class="fa fa-ban" aria-hidden="true"></i>
		  	    	  	    				</Button>
		  	    	  	    		
		  	    	  	    		<Button className="m-1"  variant="outline-danger" size="sm"
		  	    	  	    		onClick={ () => deleteToggle(product._id)}>
		  	    	  	    			<i class="fa fa-trash" aria-hidden="true"></i>
		  	    	  	    		</Button>
		  	    	  	    {/*	  </div>
		  	    	  	    	 
					  	    	
					  	    </div>*/}
					  	    </Card.Footer>
					  	  
						  	</Card>
		  	</Col>
		  )
		})

		const productsArrMobileInactive = filtered.filter(elem => elem.isActive === false).map( (product) => {
			console.log(product)
			return(
<Col xs = {6} sm = {4}  lg = {3} className ="columnProduct p-sm-3 p-1  d-flex align-items-stretch">
		
				<Card  /*onClick={ ()=> openEdit(product._id)}*/ style={{ cursor: "pointer" }} className="shadow"  id="productCardMobile" >
								{(!isMobile)
						  		?<Card.Img className="img-fluid" style={{height:"10rem",objectFit: "contain"}} variant="top" src={product.imageUrl} />
						  		:<Card.Img className="img-fluid" style={{height:"6.5rem",objectFit: "contain"}} variant="top" src={product.imageUrl} />
						  	}
						  		<Card.Body>
						  		<Card.Title id="price">₱{product.price.toFixed(2)}</Card.Title>
						  		    <Card.Subtitle className="mb-2 wrapEllipsis" id="prodName">{product.name}</Card.Subtitle>
						  		    		{(!isMobile)
						  		      		?  <Card.Text className="pull-right" style={{fontSize:"1rem"}}>{product.stocks} left</Card.Text>
						  		      		:  <Card.Text className="pull-right" style={{fontSize:"0.8rem"}}>{product.stocks} left</Card.Text>
						  		      	}
						  		  
						  		    </Card.Body>

						  	{/*  <Card.Footer id="cardFooter">
						  	  	<Button className="buttonprod" variant="success" size="sm" block onClick={ ()=> openEdit()} ><i class="fas fa-edit"></i> Edit</Button>
						  	  </Card.Footer>*/}
<Card.Footer id="cardFooter" className="d-flex justify-content-around align-items-center">
						  	{/* <div class="text-center row">
						  	    <div class="col">*/}
		  	    	  	    		<Button className="m-1" variant="outline-primary" size="sm" 
		  	    	  	    		onClick={ ()=> openEdit(product._id) }>
		  	    	  	    			<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
		  	    	  	    		</Button>
		  	    	  	    		
		  	    	  	    				<Button className="m-1"  variant="outline-success" size="sm"
		  	    	  	    				onClick={ () => unarchiveToggle(product._id, product.isActive)}>
		  	    	  	    					<i class="fa fa-unlock" aria-hidden="true"></i>
		  	    	  	    				</Button>
		  	    	  	    		
		  	    	  	    		<Button className="m-1"  variant="outline-danger" size="sm"
		  	    	  	    		onClick={ () => deleteToggle(product._id)}>
		  	    	  	    			<i class="fa fa-trash" aria-hidden="true"></i>
		  	    	  	    		</Button>
		  	    	  	    {/*	  </div>
		  	    	  	    	 
					  	    	
					  	    </div>*/}
					  	    </Card.Footer>
					  	  
						  	</Card>
		  	</Col>
		  )
		})
	
		setProducts(productsArr)
		setProductsMobileActive(productsArrMobileActive)
		setProductsMobileInactive(productsArrMobileInactive)
	}, [productData,filtered,isMobile])


	const editProduct = (e, productId) => {

		e.preventDefault()
		closeEdit();
		trackPromise(fetch(`https://fast-bastion-88049.herokuapp.com/api/products/updateProduct`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				id: productId,
				name: name,
				description: description,
				stocks: stocks,
				price: price
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result) //updated product document

			allProductData()

			if(typeof result !== "undefined"){
				// alert("success")

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully updated!"
				})
				setIsDisabledEdit(true)
				
			} else {

				allProductData()

				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Something went wrong!"
				})
			}
		}).catch(error => window.location.replace("./serverError")))
	}

	/*update product*/
	const archiveToggle = (productId, isActive) => {

		trackPromise(fetch(`https://fast-bastion-88049.herokuapp.com/api/products/archivedProduct`,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					id: productId
				})
			}
		)
		.then(result => result.json())
		.then(result => {
			console.log(result)

			allProductData();
			if(result === true){
				Swal.fire({
					title: "Success",
					icon: "success",
					"text": "Product successfully archived"
				})
			} else {
				allProductData();
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					"text": "Please try again"
				})
			}
		}).catch(error => window.location.replace("./serverError")))
	}

	const unarchiveToggle = (productId, isActive) => {

		trackPromise(fetch(`https://fast-bastion-88049.herokuapp.com/api/products/unarchiveProduct`,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					id: productId
				})
			}
		)
		.then(result => result.json())
		.then(result => {
			console.log(result)

			allProductData();
			if(result === true){
				Swal.fire({
					title: "Success",
					icon: "success",
					"text": "Product successfully unarchived"
				})
			} else {
				allProductData();
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					"text": "Please try again"
				})
			}
		}).catch(error => window.location.replace("./serverError")))
	}




	const deleteToggle = (productId) => {
	Swal.fire({
				  title: 'Are you sure?',
				  text: "You won't be able to revert this!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
				  if (result.isConfirmed) {
				    trackPromise(fetch(`https://fast-bastion-88049.herokuapp.com/api/products/deleteProduct`, 
				    	{
				    		method: "DELETE",
				    		headers: {
				    			"Content-Type": "application/json",
				    			"Authorization": `Bearer ${token}`
				    		},
				    		body: JSON.stringify({
				    			id: productId
				    		})
				    	
				    })
				    .then(result => result.json())
				    .then(result => {
				    	console.log(result)

				    	allProductData();
				    	if(result === true){ 
				    		Swal.fire(
				    		  'Deleted!',
				    		  'Product has been deleted.',
				    		  'success'
				    		)
				    		// Swal.fire({
				    		// 	title: "Success",
				    		// 	icon: "success",
				    		// 	"text": "Product successfully deleted"
				    		// })
				    	} else {
				    		allProductData();
				    		Swal.fire({
				    			title: "Something went wrong",
				    			icon: "error",
				    			"text": "Please try again"
				    		})
				    	}
				    }).catch(error => window.location.replace("./serverError")))
				  }
				})
		}
	
		
	
		function addProduct(e){
			e.preventDefault();
			closeAdd();
			// const uploadImage = () => {
				const data = new FormData()
				data.append("file", image)
				data.append("upload_preset", "productImage")
				data.append("cloud_name","lexusnexus")

				trackPromise(
				fetch("  https://api.cloudinary.com/v1_1/lexusnexus/image/upload",{
						method:"post",
						body: data
					})
				.then(resp => resp.json())
				.then(data => {
					setUrl(data.url)
					console.log(data.url)
					saveProduct(data.url)
				})
				.catch(error => window.location.replace("./serverError")))
			// }

			const saveProduct = (url) => {
				trackPromise(
				fetch('https://fast-bastion-88049.herokuapp.com/api/products/addProduct', {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${token}`
					},
					body: JSON.stringify({
						name: name,
						description: description,
						stocks: stocks,
						price: price,
						imageUrl: url
					})
				})
				.then(res => res.json())
				.then(data => {
				

					if(data === true){
						allProductData()
						Swal.fire({
							title: "Success",
							icon: "success",
							text: "Product successfully added"
						})
						setName('');
						setDescription('');
						setPrice(0);
						setImage("");

						// history.push('/products');

					} else {

						Swal.fire({
							title: "Failed",
							icon: "error",
							text: "Something went wrong"
						})

					}
				}).catch(error => window.location.replace("./serverError")))
					}
			
		};

		useEffect(()=>{

			console.log(price);
			console.log(showEdit);
			if ((showAdd===true && image === ''|| price === '') || (showEdit===true && price === "")) {
				setIsDisabled(true)
				 setIsDisabledEdit(true)
				console.log("tryyy1");
			}
			else if ((showAdd===true && image !== null && price !==0)) {
				setIsDisabled(false)
				console.log("tryyy2");
			} else if ( showEdit===true && price > 0  ) {		
				setIsDisabledEdit(false) //should be disabled if there is no changes
				console.log("tryyy");
			}

		}, [image,price,showEdit,showAdd]);

	return(
		(productData.length===0)
		?<div class="center-screen">
		    		<img src={emptycart} class="emptycart" />	
		    		<h2 class="mt-3" style={{ color: '#e63c49' }}>No products yet</h2>
		    		<h6 style={{ color: '#5f87bb' }}>Looks like there is no products yet</h6>

		   			<Button variant="primary" onClick={openAdd} style={{borderRadius: "2rem"}}><i class="fa fa-plus" aria-hidden="true"></i> Add Product</Button>

		    			          {/*Add Product Modal*/}
		    			          		<Modal centered show={showAdd} onHide={closeAdd}>
		    			          			<Form onSubmit={ (e) => addProduct(e) }>
		    			          				<Modal.Header>Add Product</Modal.Header>
		    			          				<Modal.Body style={{wordBreak:"break-word"}}>
		    			          					<Form.Group productId="productName">
		    			          						<Form.Label>Name</Form.Label>
		    			          						<Form.Control 
		    			          							type="text"
		    			          							maxLength={100}
		    			          							value={name}
		    			          							onChange={(e)=> setName(e.target.value)}
		    			          							required
		    			          						/>
		    			          					</Form.Group>
		    			          					<Form.Group productId="productDescription">
		    			          						<Form.Label>Description</Form.Label>
		    			          						<Form.Control
		    			          							as="textarea"
		    			          							maxLength={100}
		    			          							value={description}
		    			          							onChange={(e)=> setDescription(e.target.value)}
		    			          							required
		    			          							 style={{ height: '100px' }}
		    			          						/>
		    			          					</Form.Group>
		    			          					<Row>
		    			          						<Form.Group as={Col}  className="xs-4" controlId="productStocks">
		    			          							<Form.Label>Stocks</Form.Label>
		    			          							<Form.Control
		    			          								type="text"
		    			          								maxLength={5}
		    			          								value={stocks}
		    			          								required
		    			          								onChange={ (e)=> numberOnlyStocks(e)}
		    			          							/>
		    			          						</Form.Group>
		    			          						<Form.Group as={Col} className="xs-4" productId="productStocks">
		    			          							
		    			          						</Form.Group>
		    			          					</Row>
		    			          					<Row>
		    			          						<Form.Group as={Col} className="xs-4" productId="productPrice">
		    			          							<Form.Label>Price</Form.Label>
		    			          							<Form.Control 
		    			          								type="text"
		    			          								maxLength={5}
		    			          								value={price}
		    			          								onChange={(e)=> numberOnly(e)}
		    			          								required
		    			          							/>
		    			          						</Form.Group>
		    			          						<Form.Group as={Col} className="xs-4" productId="productPrice">
		    			          							
		    			          						</Form.Group>

		    			          					</Row>

		    			          					<Form.Group controlId="formFile" className="mb-3">
		    			          					   
		    			          							<input class="wrapEllipsis" type="file" onChange= {(e)=> setImage(e.target.files[0])} required></input>
		    			          							{/*<button onClick={uploadImage}>Upload</button>*/}
		    			          					
		    			          						{/*<div>
		    			          							<h6>Preview: </h6>
		    			          							<img src={url} width="50px"/>
		    			          						</div>*/}
		    			          					  </Form.Group>
		    			          				</Modal.Body>
		    			          				<Modal.Footer>
		    			          					<Button variant="secondary" onClick={cancelAdd}>Cancel</Button>
		    			          					<Button variant="success" type="submit" disabled={isDisabled}>Submit</Button>
		    			          				</Modal.Footer>
		    			          			</Form>
		    			          		</Modal>
		    </div>

		:
		<div>
			<div class='pb-3'>
				{(!isMobile) 
				  	    	  	    	 ?<><h2 className="text-center">Products</h2>
				  	    	  	    	{/* <div className="d-flex justify-content-end mb-2">
				  	    	  	    	 	<Button variant="primary" onClick={openAdd}><i class="fa fa-plus" aria-hidden="true"></i> Add Product</Button>
				  	    	  	    	 </div>*/}</>
									:<><h4 className="pt-1 text-center">Products</h4>
									{/*<div className="d-flex justify-content-end mb-2">
					<Button variant="primary"  size="sm" onClick={openAdd}><i class="fa fa-plus" aria-hidden="true"></i> Add Product</Button>
				</div> */}</>
								}
				<div class="row">
					<div class="col-12 col-md-4">
					{/*<input type="text" maxLength={30} placeholder="Search Facebook Name"  onChange={(e)=>filterProducts(e)} />*/}
				      <MDBInput label="Search a product" icon="search" maxLength={30} onChange={(e)=>filterProducts(e)}/>
				      </div>
				      	<div class="col-12 col-md-8 text-right align-self-center">
				            <Button variant="primary"  size="sm" onClick={openAdd}><i class="fa fa-plus" aria-hidden="true"></i> Add Product</Button>
				            </div>
				 </div>
			</div>
			
				    {(products.length === 0)
					  ?<div class="center-screen">
					    		<img src={emptycart} class="emptycart" />	
					    		<h5 class="mt-3" style={{ color: '#e63c49' }}>No product found</h5>
					    	{/*<h6 style={{ color: '#5f87bb' }}>Looks like you haven't archived <br/>any product in your shop yet</h6>*/}
					    </div>
					    :
	<Tabs defaultActiveKey="active" id="uncontrolled-tab-example" className="mb-3 historyTab">
				  <Tab eventKey="active" title={<div>Active <Badge className="badgeAll" pill>{productsMobileActive.length}</Badge></div>}>
				  {(productsMobileActive.length === 0)
				  ?<div class="center-screen">
				    		<img src={emptycart} class="emptycart" />	
				    		<h5 class="mt-3" style={{ color: '#e63c49' }}>No product found</h5>
				    	{/*<h6 style={{ color: '#5f87bb' }}>Looks like you haven't add any <br/>products in your shop yet</h6>*/}
				    </div>
				  :
				  <>
				 
				    <Row className = "pt-1 " id="no-gutters">
		
				{productsMobileActive}
			
		</Row>
		</>
				    }
				  </Tab>
				  <Tab eventKey="inactive" title={<div>Inactive <Badge className="badgeAll" pill>{productsMobileInactive.length}</Badge></div>}>
				    {(productsMobileInactive.length === 0)
					  ?<div class="center-screen">
					    		<img src={emptycart} class="emptycart" />	
					    		<h5 class="mt-3" style={{ color: '#e63c49' }}>No product found</h5>
					    	{/*<h6 style={{ color: '#5f87bb' }}>Looks like you haven't archived <br/>any product in your shop yet</h6>*/}
					    </div>
					  :
					    <Row className = "pt-1 " id="no-gutters">
		
				{productsMobileInactive}
			
		</Row>
					    }
				  </Tab>
				</Tabs>
}
			
			
		{/*Edit Product Modal*/}
			<Modal centered show={showEdit} onHide={closeEdit}>
				<Form onSubmit={ (e) => editProduct(e, productId) }>
					<Modal.Header>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								type="text"
								maxLength={100}
								value={name}
								onChange={ (e)=> setName(e.target.value)}
							/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								as="textarea"
								maxLength={100}
								value={description}
								onChange={ (e)=> setDescription(e.target.value)}
								style={{ height: '100px' }}
		    			          					
							/>
						</Form.Group>
						<Row>
							<Form.Group as={Col}  className="xs-4" controlId="productStocks">
								<Form.Label>Stocks</Form.Label>
								<Form.Control
									type="text"
									maxLength={5}
									value={stocks}
									onChange={ (e)=> numberOnlyStocks(e)}
								/>
							</Form.Group>
							<Form.Group as={Col} className="xs-4" productId="productStocks">
								
							</Form.Group>
						</Row>
						<Row className="mb-3">
							<Form.Group as={Col} className="xs-4" productId="productPrice">
								<Form.Label>Price</Form.Label>
								<Form.Control 
									type="text"
									maxLength={5}
									value={price}
									onChange={(e)=> numberOnly(e)}
								/>
							</Form.Group>
							<Form.Group as={Col} className="xs-4" productId="productPrice">
								
							</Form.Group>
						</Row>
						{/*<Form.Group controlId="productStocks">
							<Form.Label>Stocks</Form.Label>
							<Form.Control
								type="text"
								maxLength={5}
								value={stocks}
								onChange={ (e)=> numberOnlyStocks(e)}
							/>
						</Form.Group>*/}
						{/*<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								type="text"
								maxLength={5}
								value={price}
								onChange={ (e)=> numberOnly(e)}
							/>
						</Form.Group>*/}
						

					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit" disabled={isDisabledEdit}>Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		
		{/*Add Product Modal*/}
				<Modal centered show={showAdd} onHide={closeAdd}>
					<Form onSubmit={ (e) => addProduct(e) }>
						<Modal.Header>Add Product</Modal.Header>
						<Modal.Body>
							<Form.Group productId="productName">
								<Form.Label>Name</Form.Label>
								<Form.Control 
									type="text"
									maxLength={100}
									value={name}
									onChange={(e)=> setName(e.target.value)}
									required
								/>
							</Form.Group>
							<Form.Group productId="productDescription">
								<Form.Label>Description</Form.Label>
								<Form.Control
									as="textarea"
									maxLength={100}
									value={description}
									onChange={(e)=> setDescription(e.target.value)}
									required
									 style={{ height: '100px' }}
								/>
							</Form.Group>
							
							<Row>
								<Form.Group as={Col}  className="xs-4" controlId="productStocks">
									<Form.Label>Stocks</Form.Label>
									<Form.Control
										type="text"
										maxLength={5}
										value={stocks}
										required
										onChange={ (e)=> numberOnlyStocks(e)}
									/>
								</Form.Group>
								<Form.Group as={Col} className="xs-4" productId="productStocks">
									
								</Form.Group>
							</Row>
							<Row>
								<Form.Group as={Col} className="xs-4" productId="productPrice">
									<Form.Label>Price</Form.Label>
									<Form.Control 
										type="text"
										maxLength={5}
										value={price}
										onChange={(e)=> numberOnly(e)}
										required
									/>
								</Form.Group>
								<Form.Group as={Col} className="xs-4" productId="productPrice">
									
								</Form.Group>
							</Row>

							<Form.Group controlId="formFile" className="mb-3">
							   
									<input class="wrapEllipsis d-block" style={{width:"100%"}} type="file" onChange= {(e)=> setImage(e.target.files[0])} required></input>
									{/*<button onClick={uploadImage}>Upload</button>*/}
							
								{/*<div>
									<h6>Preview: </h6>
									<img src={url} width="50px"/>
								</div>*/}
							  </Form.Group>
						</Modal.Body>
						<Modal.Footer>
							<Button variant="secondary" onClick={cancelAdd}>Cancel</Button>
							<Button variant="success" type="submit" disabled={isDisabled}>Submit</Button>
						</Modal.Footer>
					</Form>
				</Modal>
		</div>
	)
}