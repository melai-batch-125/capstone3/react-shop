import React, {useState, useEffect} from 'react'

import {Row, Tabs, Tab,Badge} from 'react-bootstrap'
import { MDBInput } from "mdbreact"

import UsersCard from './UsersCard';
import emptyhistory from "./../images/emptyhistory.png"
export default function AdminUsers(props){
	console.log(props)

	const { userData, allUsersData } = props;
	console.log(userData) //array of users

	const [customers, setCustomers] = useState([]);
	const [admins, setAdmins] = useState([]);

	const email = localStorage.getItem("email")
	const [filterText, setFilter] = useState("");

	const filterProducts = (event) => {
	    setFilter(event.target.value);
	}
	let filtered = React.useMemo(() => {
	    return userData.filter(order => {
	    	let name = order.facebookName.toLowerCase();
	    /*	let del = order.deliveryOption.toLowerCase();
	    	let nameDel = [name,del].join(" , ");*/
	    	let filterLower = filterText.toLowerCase();
	    	console.log(name)
	    	console.log(name.includes(filterLower))
	      return filterText.length > 0 ? name.includes(filterLower) : true;
	    })
	}, [filterText, userData]);

	const [windowDimension, setWindowDimension] = useState(null);

	  useEffect(() => {
	    setWindowDimension(window.innerWidth);
	  }, []);

	  useEffect(() => {
	    function handleResize() {
	      setWindowDimension(window.innerWidth);
	    }

	    window.addEventListener("resize", handleResize);
	    return () => window.removeEventListener("resize", handleResize);
	  }, []);

	  const isMobile = windowDimension <= 768;
	useEffect( () => {

		/*const usersArr = userData.filter(elem => elem.email !== email).map( (user) => {
			console.log(user)

			return <UsersCard key={user._id} usersProp={user}/>;
		})*/
		const custArr = filtered.filter((elem) => {return elem.email !== email && elem.isAdmin === false}).map( (user) => {
			console.log(user)

			return <UsersCard key={user._id} usersProp={user} allUsersData={allUsersData}/>;
		})
		const adminArr = filtered.filter((elem) => {return elem.email !== email && elem.isAdmin === true}).map( (user) => {
			console.log(user)

			return <UsersCard key={user._id} usersProp={user} allUsersData={allUsersData}/>;
		})

		// setUsers(usersArr);
		setCustomers(custArr);
		setAdmins(adminArr);
	}, [filtered])


	
	return(
		(userData.length===0)
		?null

		:<div>
			
    	  	    {(!isMobile) 
  	    	  	    	 ?<h2 className="text-center">Users</h2>
    	  	    	 	:<h4 className="pt-3 text-center">Users</h4>
    	  	    	 }
    	  	    	 <div class="row">
    	  	    	 	<div class="col-12 col-md-4"> <MDBInput label="Search a product" icon="search" maxLength={30} onChange={(e)=>filterProducts(e)}/></div>
    	  	    	 	{/*<div class="col-12 col-md-8"></div>*/}
    	  	    	 </div>
    	  	    	 	
				
				<Tabs defaultActiveKey="customer" id="uncontrolled-tab-example" className="mb-3 historyTab">
				  {/*<Tab eventKey="all" title={<div>All <Badge className="badgeAll" pill>{users.length}</Badge></div>}>
				    {(users.length === 0)
				  ?<div class="center-screen">
				    		<img src={emptyhistory} class="emptycart" />	
				    		<h2 class="mt-3" style={{ color: '#e63c49' }}>No Orders yet</h2>
				    	<h6 style={{ color: '#5f87bb' }}>Looks like they haven't ordered <br/>anything in your shop yet</h6>
				    </div>
				  :
				    <Row className = "px-3 pt-3">
				    	{users}
				    </Row>
				    }
				  </Tab>*/}
				  <Tab eventKey="customer" title={<div>Customers <Badge className="badgeAll" pill>{customers.length}</Badge></div>}>
				  {(customers.length === 0)
				  ?<div class="center-screen">
				    		<img src={emptyhistory} class="emptycart" alt="empty" />	
				    		<h2 class="mt-3" style={{ color: '#e63c49' }}>No customer users found</h2>
				    	<h6 style={{ color: '#5f87bb' }}>Looks like there is no customer users <br/>found in your shop</h6>
				    </div>
				  :
				    <Row className = "px-3 pt-3">
				    	{customers}
				    </Row>
				    }
				  </Tab>
				  <Tab eventKey="admin" title={<div>Admins <Badge className="badgeAll" pill>{admins.length}</Badge></div>}>
				    {(admins.length === 0)
					  ?<div class="center-screen">
					    		<img src={emptyhistory} class="emptycart" alt="empty"/>	
					    		<h2 class="mt-3" style={{ color: '#e63c49' }}>No admin users found</h2>
					    	<h6 style={{ color: '#5f87bb' }}>Looks like no admin users <br/>found in your shop</h6>
					    </div>
					  :
					    <Row className = "px-3 pt-3">
					    	{admins}
					    </Row>
					    }
				  </Tab>
				</Tabs>
			</div>
	)
}