import React, { useEffect, useState } from "react";
import up from "./../images/top.png"
export default function BackToTop() {
  const [isVisible, setIsVisible] = useState(false);

  // Show button when page is scorlled upto given distance
  const toggleVisibility = () => {
    if (window.scrollY >= 300) {
      setIsVisible(true);
    } else {
      setIsVisible(false);
    }
  };

  // Set the top cordinate to 0
  // make scrolling smooth
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  };

  useEffect(() => {
    window.addEventListener("scroll", toggleVisibility);
  }, []);

  return (
    <div id="scrollTop">
      {isVisible && 
        <div onClick={scrollToTop}>
          <img src={up} id="topImage"  alt='Go to top'/>

        </div>}
    </div>
  );
}
