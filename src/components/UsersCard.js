import React,{useState,useEffect} from 'react';
// import UserContext from '../UserContext';

import {Card,Col,Button,Row,Modal,Form,InputGroup} from 'react-bootstrap'
import { trackPromise } from 'react-promise-tracker';
import Swal from 'sweetalert2';
export default function UsersCard(props){
	// console.log(props)
	const { usersProp,allUsersData} = props;
	let token = localStorage.getItem('token')
	const [windowDimension, setWindowDimension] = useState(null);

	  useEffect(() => {
	    setWindowDimension(window.innerWidth);
	  }, []);

	  useEffect(() => {
	    function handleResize() {
	      setWindowDimension(window.innerWidth);
	    }

	    window.addEventListener("resize", handleResize);
	    return () => window.removeEventListener("resize", handleResize);
	  }, []);

	  const isMobile = windowDimension <= 768;

const adminToggle = (userId, isAdmin) => {

	  	trackPromise(fetch(`https://fast-bastion-88049.herokuapp.com/api/users/changeToAdmin`,
	  		{
	  			method: "PUT",
	  			headers: {
	  				"Content-Type": "application/json",
	  				"Authorization": `Bearer ${token}`
	  			},
	  			body: JSON.stringify({
	  				id: userId,
	  				isAdmin: isAdmin
	  			})
	  		}
	  	)
	  	.then(result => result.json())
	  	.then(result => {
	  		console.log(result)

	  		allUsersData();
	  		if(result === true){
	  			Swal.fire({
	  				title: "Success",
	  				icon: "success",
	  				"text": "The role has been changed."
	  			})
	  		} else {
	  			allUsersData();
	  			Swal.fire({
	  				title: "Something went wrong",
	  				icon: "error",
	  				"text": "Please try again"
	  			})
	  		}
	  	}).catch(error => window.location.replace("./serverError")))
	  }


return(
	
		<Card border={(usersProp.isAdmin==false) ?"success" :"danger"} className ="cardHistory shadow-lg" style={{width:"100vw", marginBottom:"1rem"}}>
				  	
				  	  <Card.Body>
				  	      	    	<div class="row">
				  	    	  	    	
				  	    	  	    	{(!isMobile) 
				  	    	  	    	 ?<><div class="col-4 text-right"><h6>Facebook Name</h6></div>
				  	    	  	    	 <div class="col-5"><h6><strong>{usersProp.facebookName}</strong></h6></div>
				  	    	  	    	 <div class="col-2 text-right">
				  	    	  	    	
				  	    	  	    	  </div></>
				  	    	  	    	 :<div class="col"><h6><strong>{usersProp.facebookName}</strong></h6></div>
				  	    	  	    	}
				  	      	    	</div>
				  	      	    	<div class="row">
				  	    	  	    	
				  	    	  	    	{(!isMobile) 
				  	    	  	    	 ?<><div class="col-4 text-right"><h6>Email</h6></div>
				  	    	  	    	 <div class="col-5"><h6><strong>{usersProp.email}</strong></h6></div>
				  	    	  	    	 <div class="col-2 text-right">
				  	    	  	    	
				  	    	  	    	  </div></>
				  	    	  	    	 :<div class="col"><h6><strong>{usersProp.email}</strong></h6></div>
				  	    	  	    	}
				  	      	    	</div>
				  	      	    	<div class="row">
				  	    	  	    	
				  	    	  	    	{(!isMobile) 
				  	    	  	    	 ?<><div class="col-4 text-right"><h6>Contact Number</h6></div>
				  	    	  	    	<div class="col-5"><h6><strong>{usersProp.contactNo}</strong></h6></div>
				  	    	  	    	 <div class="col-3 text-right">
				  	    	  	    	 {(usersProp.isAdmin==false) 
				  	    	  	    	 	?<Button className="m-1"  variant="primary" size="sm" onClick={()=> adminToggle(usersProp._id,usersProp.isAdmin)}>
					  	    		<i class="fas fa-user-cog"></i> Make Admin
					  	    		</Button>
				  	    	  	    	 	:<Button className="m-1"  variant="warning"size="sm" onClick={()=> adminToggle(usersProp._id,usersProp.isAdmin)}>
					  	    		<i class="fas fa-user"></i> Make Customer
					  	    		</Button>
				  	    	  	    	 }
				  	    	  	    		
				  	    	  	    	</div></>
				  	    	  	    	 :<div class="col"><h6><strong>{usersProp.contactNo}</strong></h6></div>
				  	    	  	    	}
				  	      	    	</div>
				  	      	    	
								  	{(isMobile) 
				  	    	  	    	 ?<div class="text-right row">
								  	    <div class="col">
				  	    	  	    		{(usersProp.isAdmin==false) 
				  	    	  	    	 	?<Button className="m-1"  variant="primary" size="sm" onClick={()=> adminToggle(usersProp._id,usersProp.isAdmin)}>
					  	    		<i class="fas fa-user-cog"></i> Make Admin
					  	    		</Button>
				  	    	  	    	 	:<Button className="m-1"  variant="warning"size="sm" onClick={()=> adminToggle(usersProp._id,usersProp.isAdmin)}>
					  	    		<i class="fas fa-user"></i> Make Customer
					  	    		</Button>
				  	    	  	    	 }
				  	    	  	    	  </div>
				  	    	  	    	 
							  	    	
							  	    </div>
							  	    :null
							  	    }	

				  	  </Card.Body>
				  	  
				  	</Card>

				 

	
)
}