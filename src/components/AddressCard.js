import React,{useState,useEffect} from 'react';
// import UserContext from '../UserContext';

import {Card,Col,Button,Row,Modal,Form,InputGroup} from 'react-bootstrap'
import { trackPromise } from 'react-promise-tracker';
import Swal from 'sweetalert2';
import {regions, provinces, cities, barangays} from 'select-philippines-address';
export default function AddressCard(props){
	// console.log(props)
	const { addressProp, getDetails} = props;
	let token = localStorage.getItem('token')
	const [addressId, setAddressId] = useState(addressProp._id);
	const [fullName, setFullName] = useState(addressProp.fullName);
	const [mobileNo, setMobileNo] = useState(addressProp.mobileNo);
	const [street, setStreet] = useState(addressProp.addressLine.split(',')[0]);

	const firstFullName = addressProp.fullName;
	const firstMobileNo = addressProp.mobileNo;
	const firstAddressLine = addressProp.addressLine;
	const firstStreet = addressProp.addressLine.split(',')[0];
	let addressLine = addressProp.addressLine.split(',').slice(1);
	// const [verifyPassword, setVerifyPassword] = useState('');
	// let addresses = addressProp.addresses
	const [changeAdd, setChangeAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);
	const [isDisabled, setIsDisabled] = useState(true);

	const [windowDimension, setWindowDimension] = useState(null);


	  useEffect(() => {
	    setWindowDimension(window.innerWidth);
	  }, []);

	  useEffect(() => {
	    function handleResize() {
	      setWindowDimension(window.innerWidth);
	    }

	    window.addEventListener("resize", handleResize);
	    return () => window.removeEventListener("resize", handleResize);
	  }, []);

	  const isMobile = windowDimension <= 768;

		const [regionData, setRegion] = useState([]);
	    const [provinceData, setProvince] = useState([]);
	    const [cityData, setCity] = useState([]);
	    const [barangayData, setBarangay] = useState([]);

	    const [regionAddr, setRegionAddr] = useState("");
	    const [provinceAddr, setProvinceAddr] = useState("");
	    const [cityAddr, setCityAddr] = useState("");
	    const [barangayAddr, setBarangayAddr] = useState("");

	    const region = () => {
	        regions().then(response => {
	            setRegion(response);
	        });
	    }

	    const province = (e) => {
	    	document.getElementById("provinceSelect").style.display = "block";
	        setRegionAddr(e.target.selectedOptions[0].text);
	        provinces(e.target.value).then(response => {
	            setProvince(response);
	            setCity([]);
	            setBarangay([]);
	        });
	    }

	    const city = (e) => {
	    	document.getElementById("citySelect").style.display = "block";
	        setProvinceAddr(e.target.selectedOptions[0].text);
	        cities(e.target.value).then(response => {
	            setCity(response);
	        });
	    }

	    const barangay = (e) => {
	    	document.getElementById("barSelect").style.display = "block";
	        setCityAddr(e.target.selectedOptions[0].text);
	        barangays(e.target.value).then(response => {
	            setBarangay(response);
	        });
	    }

	    const brgy = (e) => {
	        setBarangayAddr(e.target.selectedOptions[0].text);
	    }

	    useEffect(() => {
	        region()
	    }, [])
	const openEdit = () => {
		
			setShowEdit(true);
		}
	const closeEdit = () => {
		setShowEdit(false);
		
		

	}
	const cancelEdit = () => {
		setShowEdit(false);
		setFullName(firstFullName);
		setMobileNo(firstMobileNo);
		setStreet(firstStreet);
		
		

	}
	const setAsDefault = (e) => {

		e.preventDefault()
		// closeEdit();
		trackPromise(fetch(`https://fast-bastion-88049.herokuapp.com/api/users/setAsDefault`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				addressId
			})
		})
		.then(result => result.json())
		.then(result => {
			
			getDetails()

			if(typeof result !== "undefined"){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Address successfully updated!"
				})

				
			} else {

			

				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Something went wrong!"
				})
			}
		}).catch(error => window.location.replace("./serverError")))
	}
	const editAddress = (e) => {
		if (changeAdd === true) {
			addressLine = [street,barangayAddr,cityAddr,provinceAddr,regionAddr].join(", ");
		} else if (firstStreet !== street){
			addressLine = [street,addressLine].join(", ");;
		} else {
			addressLine = firstAddressLine;
		}
			/*alert(fullName);
			alert(mobileNo);
			
			alert(addressLine);
			alert(isDefault);*/
			console.log("addressId:", addressId)
			console.log("fullName:", fullName)
			console.log("mobileNo:", mobileNo)
			console.log("addressLine:", addressLine)
			e.preventDefault()
			closeEdit();
			trackPromise(fetch('https://fast-bastion-88049.herokuapp.com/api/users/updateAddress', {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					addressId,
					fullName,
					mobileNo,
					addressLine
				})
			})
			.then(result => result.json())
			.then(result => {

				// console.log(result)
				

				if(result === true){
					

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Address successfully updated"
					})
					setIsDisabled(true);
					/*setFullName('');
					setMobileNo('');
					setStreet('');
					addressLine = "";*/
					
					// setIsDefault(false);
					
					

				} else {
					

					Swal.fire({
						title: "Failed",
						icon: "error",
						text: "Something went wrong"
					})
				}
				getDetails()

			}).catch(error => window.location.replace("./serverError")))

		}

	const showEditAddress = () => {
		document.getElementById("divAddress").style.display = "block";
		document.getElementById("displayedAdd").style.display = "none";
		setChangeAdd(true);
	}

	const deleteToggle = (e) => {
		e.preventDefault()
		Swal.fire({
					  title: 'Are you sure?',
					  text: "You won't be able to revert this!",
					  icon: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, remove it!'
					}).then((result) => {
					  if (result.isConfirmed) {
					    trackPromise(fetch(`https://fast-bastion-88049.herokuapp.com/api/users/removeAddress`, 
					    	{
					    		method: "POST",
					    		headers: {
					    			"Content-Type": "application/json",
					    			"Authorization": `Bearer ${token}`
					    		},
					    		body: JSON.stringify({
					    			addressId
					    		})
					    	
					    })
					    .then(result => result.json())
					    .then(result => {
					    	getDetails()
					    	if(result === true){ 
					    		Swal.fire(
					    		  'Removed!',
					    		  'Address has been removed.',
					    		  'success'
					    		)
					    		
					    	} else {
					    		
					    		Swal.fire({
					    			title: "Something went wrong",
					    			icon: "error",
					    			"text": "Please try again"
					    		})
					    	}
					    }).catch(error => window.location.replace("./serverError")))
					  }
					})
			}
			useEffect(()=>{
				if (barangayAddr !== '' && cityAddr !== '' && provinceAddr !== '' && regionAddr !== '') {
					setIsDisabled(false)


				}
				else if (changeAdd === true) {

									setIsDisabled(true)
								}
				else if ((firstFullName !== fullName || firstMobileNo !== mobileNo || firstStreet !== street) && fullName !== '' && mobileNo.length === 11 && street !== ''  ) {
					setIsDisabled(false)


				} 

				else {
					setIsDisabled(true)


				}

			}, [fullName,mobileNo,street,changeAdd,barangayAddr,cityAddr,provinceAddr,regionAddr]);

			/*useEffect(()=>{
				if (barangayAddr !== '' && cityAddr !== '' && provinceAddr !== '' && regionAddr !== '') {
					setIsDisabled(false)


				} else {
					setIsDisabled(true)


				}

			}, [barangayAddr,cityAddr,provinceAddr,regionAddr]);*/
				 const numberOnly = (e) => {
			const re = /^[0-9\b]+$/;
	      if (e.target.value === '' || re.test(e.target.value)) {
	      	if (e.target.value.length > 11) {
	  	           setMobileNo(e.target.value.substring(0,11))
	  	        } else 
	  	        {
	  	            setMobileNo(e.target.value)
	  	        }
	        
	      }
	  	}
return(
	<div class ="pb-3">
		{/*<Col xs = {12} className ="pb-3">*/}
		
		  	<Card border={(addressProp.isDefault===false) ?"success" :"danger"} className ="cardHistory shadow-lg">
		  	
		  	  <Card.Body>
		  	      	    	<div class="row">
		  	    	  	    	
		  	    	  	    	{(!isMobile) 
		  	    	  	    	 ?<><div class="col text-right"><h6>Full Name</h6></div>
		  	    	  	    	 <div class="col"><h6><strong>{addressProp.fullName}</strong></h6></div>
		  	    	  	    	 <div class="col text-right">
		  	    	  	    		<Button onClick={ ()=> openEdit() } className="m-1"  variant="primary" size="sm">
						  	    	<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
						  	      </Button>
		  	    	  	    	  </div></>
		  	    	  	    	 :<div class="col"><h6>{/*<i class="fa fa-user-circle-o" aria-hidden="true"></i>*/}<strong>{addressProp.fullName}</strong></h6></div>
		  	    	  	    	}
		  	      	    	</div>
		  	      	    	<div class="row">
		  	    	  	    	
		  	    	  	    	{(!isMobile) 
		  	    	  	    	 ?<><div class="col text-right"><h6>Mobile Number</h6></div>
		  	    	  	    	<div class="col"><h6><strong>{addressProp.mobileNo}</strong></h6></div>
		  	    	  	    	 <div class="col text-right">
		  	    	  	    		<Button disabled={addressProp.isDefault} className="m-1"  variant={(addressProp.isDefault===false) ?"danger" :"outline-dark"} size="sm" onClick={(e)=>{deleteToggle(e)}}>
					  	    		<i class="fa fa-trash" aria-hidden="true"></i> Delete
					  	    	</Button>
		  	    	  	    	</div></>
		  	    	  	    	 :<div class="col"><h6>{/*<i class="fa fa-mobile fa-lg" aria-hidden="true"></i>*/}<strong>{addressProp.mobileNo}</strong></h6></div>
		  	    	  	    	}
		  	      	    	</div>
		  	      	    	<div class="row">
		  	    	  	    	
		  	    	  	    	{(!isMobile) 
		  	    	  	    	 ?<><div class="col text-right"><h6>Address</h6></div>
		  	    	  	    	<div class="col"><h6>{addressProp.addressLine}</h6></div>
		  	    	  	    	 <div class="col text-right">
		  	    	  	    		<Button className="m-1"  variant={(addressProp.isDefault===false) ?"warning" :"outline-dark"} size="sm" disabled={addressProp.isDefault} onClick={(e)=>{setAsDefault(e)}}>
					  	    		<i class="fa fa-address-book" aria-hidden="true"></i> Set As Default
					  	    		</Button>
		  	    	  	    	 </div></>
		  	    	  	    	 :<div class="col"><h6>{/*<i class="fa fa-address-card-o" aria-hidden="true"></i> */}{addressProp.addressLine}</h6></div>
		  	    	  	    	}
		  	      	    	</div>
						  	{(isMobile) 
		  	    	  	    	 ?<div class="text-right row">
						  	    <div class="col">
		  	    	  	    		<Button onClick={ ()=> openEdit() } className="m-1"  variant="outline-primary" size="sm">
						  	    	<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
						  	      </Button>
						  	      <Button disabled={addressProp.isDefault} className="m-1"  variant={(addressProp.isDefault===false) ?"outline-danger" :"outline-dark"} size="sm" onClick={(e)=>{deleteToggle(e)}}>
					  	    		<i class="fa fa-trash" aria-hidden="true"></i> Delete
					  	    	</Button>
					  	    	<Button className="m-1"  variant={(addressProp.isDefault===false) ?"warning" :"outline-dark"} size="sm" disabled={addressProp.isDefault} onClick={(e)=>{setAsDefault(e)}}>
					  	    		<i class="fa fa-address-book" aria-hidden="true"></i> Set as Default
					  	    		</Button>
		  	    	  	    	  </div>
		  	    	  	    	 
					  	    	
					  	    </div>
					  	    :null
					  	    }	

		  	  </Card.Body>
		  	  
		  	</Card>
		  	  	

	{/*modal for edit address*/}
	<Modal centered show={showEdit} onHide={closeEdit}>
					<Form onSubmit={ (e)=> editAddress(e) }>
						<Modal.Header>
							<Modal.Title>Update Address</Modal.Title>
						</Modal.Header>
						<Modal.Body>
						<Row>
						<Col md>
						<Form.Group>
						<Form.Label>Full Name</Form.Label>
						<Form.Control type="text" placeholder="Full Name"  maxLength={30} value={fullName}
										onChange={(e)=> setFullName(e.target.value)}
										required/>
						</Form.Group>
						</Col>
						<Col md>
						<Form.Group>
						<Form.Label>Number</Form.Label>
						<Form.Control className="phone" type="number" placeholder="Mobile Number" value={mobileNo}
										onChange={(e)=> numberOnly(e)}
										required/>
						</Form.Group>
				   </Col>
				   </Row>
				      <Form.Group>
				      <Form.Label>Address</Form.Label>
				      <Form.Control type="text" maxLength={50} placeholder="Street Name, Building, House No." value={street}
				   						onChange={(e)=> setStreet(e.target.value)}
				   						required/>
				      </Form.Group>
				     
				      <Form.Group id="displayedAdd">
				      
				      <InputGroup className="mb-3">
				      <Form.Control type="text" disabled value={addressLine}/>
		      		  <Button onClick={ ()=> showEditAddress() } variant="primary" id="button-addon2"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
		              </Button>
				       </InputGroup>
				      </Form.Group>

				  {/*<Form.Control as="select">...</Form.Control>*/}
				  <div id="divAddress" style={{display: 'none' }}>
				  <Row>
					<Col md>
					 <Form.Group>
				  <Form.Control as="select" /*value={1}*/ onChange={province} onSelect={region}>
				      <option value='1'>Select Region</option>
				      {
				          regionData && regionData.length > 0 && regionData.map((item) => <option
				              key={item.region_code} value={item.region_code}>{item.region_name}</option>)
				      }
				  </Form.Control>
				  </Form.Group>
				  </Col>
				  <Col md id="provinceSelect" style={{display: 'none' }}>
				  <Form.Group>
				  <Form.Control as="select" onChange={city}>
				      <option>Select Province</option>
				      {

				          provinceData && provinceData.length > 0 && provinceData.map((item) => <option
				              key={item.province_code} value={item.province_code}>{item.province_name}</option>)
				      }
				  </Form.Control>
				  </Form.Group>
				  </Col>
				   </Row>
				  <Row>
				<Col md id="citySelect" style={{display: 'none' }}>
				<Form.Group>
				  <Form.Control as="select" onChange={barangay}>
				      <option>Select City</option>
				      {
				          cityData && cityData.length > 0 && cityData.map((item) => <option
				              key={item.city_code} value={item.city_code}>{item.city_name}</option>)
				      }
				  </Form.Control>
				  </Form.Group>
				  </Col>
				  <Col md id="barSelect" style={{display: 'none' }}>
				  <Form.Group>
				  <Form.Control as="select" onChange={brgy}>
				      <option>Select Barangay</option>
				      {
				          barangayData && barangayData.length > 0 && barangayData.map((item) => <option
				              key={item.brgy_code} value={item.brgy_code}>{item.brgy_name}</option>)
				      }
				  </Form.Control>
				  </Form.Group>
				  </Col>
				   </Row>
				   </div>
				   
						</Modal.Body>
						<Modal.Footer>
							<Button variant="secondary" onClick={cancelEdit}>Cancel</Button>
							<Button variant="success" type="submit" disabled={isDisabled}>Submit</Button>
						</Modal.Footer>
					</Form>
				</Modal>
		 {/*</Col>*/}
		 </div>
)
}