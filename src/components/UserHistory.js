import React, {useState,useEffect} from 'react';

// import {Row,Container} from 'react-bootstrap';
import { MDBInput } from "mdbreact"
import HistoryCard from './HistoryCard';

import {Row, Tabs, Tab,Badge,Card,Button,ButtonGroup,DropdownButton,Dropdown} from 'react-bootstrap'
import emptyhistory from "./../images/emptyhistory.png"
export default function UserHistory(props){
	console.log(props)

	const { productData, fetchData,allFalseOrders,user } = props;
	console.log(productData)
	// const {user} = useContext(UserContext);
	const [allOrder, setAllOrder] = useState([]);
	const [pendingOrder, setPendingOrder] = useState([]);
	const [completedOrder, setCompletedOrder] = useState([]);
	const [sum, setSum] = useState(0);
	const [isSortTime, setIsSortTime] = useState(false); //latest first
	const [isSortFacebookName, setIsSortFacebookName] = useState(false); //Z-A
	// const [totalState, setTotalState] = useState(0);
	const [filterText, setFilter] = useState("");

	const filterProducts = (event) => {
	    setFilter(event.target.value);
	}
	let filtered = React.useMemo(() => {
	    return productData.filter(order => {
	    	let name = order.facebookName.toLowerCase();
	    	let del = order.deliveryOption.toLowerCase();
	    	let nameDel = [name,del].join(" , ");
	    	let filterLower = filterText.toLowerCase();
	    	console.log(name)
	    	console.log(name.includes(filterLower))
	      return filterText.length > 0 ? nameDel.includes(filterLower) : true;
	    })
	}, [filterText, productData]);

	const pickupFiltered = () => {
		setFilter("Pickup");
	}
	const deliveryFiltered = () => {
		setFilter("Delivery");
	}
	const shippingFiltered = () => {
		setFilter("Shipping");
	}
	const facebookNameSortInc = () => {
		setIsSortTime(false);
		setIsSortFacebookName(false);
	}
	const facebookNameSortDec = () => {
		setIsSortTime(false);
		setIsSortFacebookName(true);
	}
	const timeSortInc = () => {
		setIsSortTime(true);
		setIsSortFacebookName(null);
	}
	const timeSortDec = () => { //latest
		setIsSortTime(false);
		setIsSortFacebookName(null);
		
	}

	const [windowDimension, setWindowDimension] = useState(null);

	  useEffect(() => {
	    setWindowDimension(window.innerWidth);
	  }, []);

	  useEffect(() => {
	    function handleResize() {
	      setWindowDimension(window.innerWidth);
	    }

	    window.addEventListener("resize", handleResize);
	    return () => window.removeEventListener("resize", handleResize);
	  }, []);

	  const isMobile = windowDimension <= 768;

	useEffect(()=>{
		if (isSortTime==false) {
			const list = filtered.sort((a, b) => new Date(b.purchasedOn).getTime() - new Date(a.purchasedOn).getTime()).map( (element) => {
				return <HistoryCard key={element._id} productProp={element} allFalseOrders={allFalseOrders} fetchData={fetchData} user = {user}/>
			})
			setAllOrder(list)
		} else {
			const listOld = filtered.sort((a, b) => new Date(a.purchasedOn).getTime() - new Date(b.purchasedOn).getTime()).map( (element) => {
				return <HistoryCard key={element._id} productProp={element} allFalseOrders={allFalseOrders} fetchData={fetchData} user = {user}/>
			})
			setAllOrder(listOld)
		}

		if (isSortFacebookName==false) {
							const list = filtered.sort(function(a, b) {
				  var nameA = a.facebookName.toUpperCase(); // ignore upper and lowercase
				  var nameB = b.facebookName.toUpperCase(); // ignore upper and lowercase
				  if (nameA < nameB) {
				    return -1;
				  }
				  if (nameA > nameB) {
				    return 1;
				  }

				  // names must be equal
				  return 0;
				}).map( (element) => {
								return <HistoryCard key={element._id} productProp={element} allFalseOrders={allFalseOrders} fetchData={fetchData} user = {user}/>
							})
							setAllOrder(list)
			} else if (isSortFacebookName==true) {
							const listOld = filtered.sort(function(a, b) {
				  var nameA = b.facebookName.toUpperCase(); // ignore upper and lowercase
				  var nameB = a.facebookName.toUpperCase(); // ignore upper and lowercase
				  if (nameA < nameB) {
				    return -1;
				  }
				  if (nameA > nameB) {
				    return 1;
				  }

				  // names must be equal
				  return 0;
				}).map( (element) => {
								return <HistoryCard key={element._id} productProp={element} allFalseOrders={allFalseOrders} fetchData={fetchData} user = {user}/>
							})
							setAllOrder(listOld)
		}
			
			

			const pending = filtered.filter(elem => elem.status === "pending").map( (element) => {
				// products = element.products

				
				// total = element.totalAmount
				// 	setTotalState(total.toLocaleString("en-US"))
				return <HistoryCard key={element._id} productProp={element} allFalseOrders={allFalseOrders} fetchData={fetchData} user = {user}/>
				// setTotalState(element.totalAmount)
			})
			console.log(pending)
			

			const completed = filtered.filter(elem => elem.status === "completed").map( (element) => {
				// products = element.products

				
				// total = element.totalAmount
				// 	setTotalState(total.toLocaleString("en-US"))
				return <HistoryCard key={element._id} productProp={element} allFalseOrders={allFalseOrders} fetchData={fetchData} user = {user}/>
				// setTotalState(element.totalAmount)
			})

			const totalCompleted = filtered.filter(elem => elem.status === "completed").reduce(function (acc, obj) { return acc + obj.totalAmount; }, 0);
			console.log(totalCompleted)
			setSum(totalCompleted)
			
			setPendingOrder(pending)
			setCompletedOrder(completed)
		}, [filtered,isSortTime,isSortFacebookName])

	
	return (
		
		
		(user.isAdmin===true) 
					? <div>
						<div>
		  	    	  	    	 
		  	    	  	    	{(!isMobile) 
		  	    	  	    	 ? 	
		  	    	  	    	 <h2 className="text-center">Succesful Orders</h2>
		  	    	  	    	 :<h4 className="pt-3 text-center">Succesful Orders</h4>
		  	    	  	    	}
		  	    	  	    	 
		  	    	  	    	 	<div class="row">
		  	    	  	    	 		<div class="col-12 col-md-4">
		  	    	  	    	 		{/*<input type="text" maxLength={30} placeholder="Search Facebook Name"  onChange={(e)=>filterProducts(e)} />*/}
		  	    	  	    	 	      <MDBInput label="Search a product" icon="search" maxLength={30} onChange={(e)=>filterProducts(e)}/>
		  	    	  	    	 	      </div>
		  	    	  	    	 	      	<div class="col-12 col-md-8 text-right align-self-center">
 	              	    	  	    	       <ButtonGroup aria-label="buttonSort">
 	              	    	  	    	       <DropdownButton as={ButtonGroup} title=<i class="fas fa-filter"></i> variant="outline-primary" id="bg-nested-dropdown">
 	              	    	  	    	           <Dropdown.Item eventKey="1" onClick={ ()=> pickupFiltered()}>Pickup</Dropdown.Item>
 	              	    	  	    	           <Dropdown.Item eventKey="2" onClick={ ()=> deliveryFiltered()}>Delivery</Dropdown.Item>
 	              	    	  	    	           <Dropdown.Item eventKey="3" onClick={ ()=> shippingFiltered()}>Shipping</Dropdown.Item>
 	              	    	  	    	           <Dropdown.Item eventKey="3" onClick={ ()=> setFilter("")}>All</Dropdown.Item>
 	              	    	  	    	        </DropdownButton>
 	              	    	  	    	        <DropdownButton as={ButtonGroup} title=<i class="fas fa-sort-amount-up"></i> variant="outline-primary" id="bg-nested-dropdown">
 	              	    	  	    	            <Dropdown.Item eventKey="1" onClick={ ()=> facebookNameSortInc()}>Facebook Name (A-Z)</Dropdown.Item>
 	              	    	  	    	            <Dropdown.Item eventKey="2" onClick={ ()=> facebookNameSortDec()}>Facebook Name (Z-A)</Dropdown.Item>
 	              	    	  	    	            <Dropdown.Item eventKey="3" onClick={ ()=> timeSortInc()}>Time (Oldest First)</Dropdown.Item>
 	              	    	  	    	            <Dropdown.Item eventKey="4" onClick={ ()=> timeSortDec()}>Time (Latest First)</Dropdown.Item>
 	              	    	  	    	         </DropdownButton>
 	              	    	  	    	         {(user.isAdmin===true) 
 	            										? <Button variant="outline-primary" onClick={ ()=> allFalseOrders()}><i class="fas fa-redo"></i></Button>
 	            										: <Button variant="outline-primary" onClick={ ()=> fetchData()}><i class="fas fa-redo"></i></Button>
 	            									}
 	              	    	  	    	        

 	              	    	  	    	       </ButtonGroup>
		  	    	  	    	 	         </div>
		  	    	  	    	 	 </div>
		  	    	  	    	
		  	    	  	    	 </div>
						
						
						<Tabs defaultActiveKey="all" id="uncontrolled-tab-example" className="mb-3 historyTab">
						  <Tab eventKey="all" title={<div>All <Badge className="badgeAll" pill>{allOrder.length}</Badge></div>}>
						    {(allOrder.length === 0)
						  ?<div class="center-screen">
						    		<img src={emptyhistory} class="emptycart" />	
						    		<h2 class="mt-3" style={{ color: '#e63c49' }}>No Orders found</h2>
						    	{/*<h6 style={{ color: '#5f87bb' }}>Looks like no orders found</h6>*/}
						    </div>
						  :
						    <Row className = "px-3 pt-3">
						    	{allOrder}
						    </Row>
						    }
						  </Tab>
						  <Tab eventKey="pending" title={<div>Pending <Badge className="badgeAll" pill>{pendingOrder.length}</Badge></div>}>
						  {(pendingOrder.length === 0)
						  ?<div class="center-screen">
						    		<img src={emptyhistory} class="emptycart" />	
						    		<h2 class="mt-3" style={{ color: '#e63c49' }}>No pending orders found</h2>
						    	{/*<h6 style={{ color: '#5f87bb' }}>Looks like they haven't ordered <br/>anything in your shop yet</h6>*/}
						    </div>
						  :
						    <Row className = "px-3 pt-3">
						    	{pendingOrder}
						    </Row>
						    }
						  </Tab>
						  <Tab eventKey="completed" title={<div>Completed <Badge className="badgeAll" pill>{completedOrder.length}</Badge></div>}>
						    {(completedOrder.length === 0)
    						  ?<div class="center-screen">
    						    		<img src={emptyhistory} class="emptycart" />	
    						    		<h2 class="mt-3" style={{ color: '#e63c49' }}>No completed orders found</h2>
    						    	{/*<h6 style={{ color: '#5f87bb' }}>Looks like you haven't completed <br/>any orders in your shop yet</h6>*/}
    						    </div>
    						  :
    						    <Row className = "px-3 pt-3">
    						    <h5  class="">Total Sales: Php {sum.toLocaleString("en-US")} 		
    						    		  	  	</h5>
    						    	{completedOrder}
    						    </Row>
    						    }
						  </Tab>
						</Tabs>
					</div>
					: <div>
					<div>
						{(!isMobile) 
		  	    	  	    	 ?<h2>Purchase History</h2>
							:<h4 className="pt-3">Purchase History</h4>
						}
								  	    	  	    	 	<div class="row">
								  	    	  	    	 		<div class="col-12 col-md-4">
								  	    	  	    	 		{/*<input type="text" maxLength={30} placeholder="Search Facebook Name"  onChange={(e)=>filterProducts(e)} />*/}
								  	    	  	    	 	      <MDBInput label="Search a product" icon="search" maxLength={30} onChange={(e)=>filterProducts(e)}/>
								  	    	  	    	 	      </div>
								  	    	  	    	 	      	<div class="col-12 col-md-8 text-right align-self-center">
						 	              	    	  	    	       <ButtonGroup aria-label="buttonSort">
						 	              	    	  	    	       <DropdownButton as={ButtonGroup} title=<i class="fas fa-filter"></i> variant="outline-primary" id="bg-nested-dropdown">
						 	              	    	  	    	           <Dropdown.Item eventKey="1" onClick={ ()=> pickupFiltered()}>Pickup</Dropdown.Item>
						 	              	    	  	    	           <Dropdown.Item eventKey="2" onClick={ ()=> deliveryFiltered()}>Delivery</Dropdown.Item>
						 	              	    	  	    	           <Dropdown.Item eventKey="3" onClick={ ()=> shippingFiltered()}>Shipping</Dropdown.Item>
						 	              	    	  	    	           <Dropdown.Item eventKey="3" onClick={ ()=> setFilter("")}>All</Dropdown.Item>
						 	              	    	  	    	        </DropdownButton>
						 	              	    	  	    	        <DropdownButton as={ButtonGroup} title=<i class="fas fa-sort-amount-up"></i> variant="outline-primary" id="bg-nested-dropdown">
						 	              	    	  	    	            <Dropdown.Item eventKey="1" onClick={ ()=> facebookNameSortInc()}>Facebook Name (A-Z)</Dropdown.Item>
						 	              	    	  	    	            <Dropdown.Item eventKey="2" onClick={ ()=> facebookNameSortDec()}>Facebook Name (Z-A)</Dropdown.Item>
						 	              	    	  	    	            <Dropdown.Item eventKey="3" onClick={ ()=> timeSortInc()}>Time (Oldest First)</Dropdown.Item>
						 	              	    	  	    	            <Dropdown.Item eventKey="4" onClick={ ()=> timeSortDec()}>Time (Latest First)</Dropdown.Item>
						 	              	    	  	    	         </DropdownButton>
						 	              	    	  	    	         {(user.isAdmin===true) 
						 	            										? <Button variant="outline-primary" onClick={ ()=> allFalseOrders()}><i class="fas fa-redo"></i></Button>
						 	            										: <Button variant="outline-primary" onClick={ ()=> fetchData()}><i class="fas fa-redo"></i></Button>
						 	            									}
						 	              	    	  	    	        

						 	              	    	  	    	       </ButtonGroup>
								  	    	  	    	 	         </div>
								  	    	  	    	 	 </div>
					</div>
						<Tabs defaultActiveKey="all" id="uncontrolled-tab-example" className="mb-3 historyTab">
						  <Tab eventKey="all" title={<div>All <Badge className="badgeAll" pill>{allOrder.length}</Badge></div>}>
						    {(allOrder.length === 0)
						  ?<div class="center-screen">
						    		<img src={emptyhistory} class="emptycart" />	
						    		<h2 class="mt-3" style={{ color: '#e63c49' }}>No Orders found</h2>
						    	{/*<h6 style={{ color: '#5f87bb' }}>Looks like you haven't ordered <br/>anything in yet</h6>*/}
						    </div>
						  :
						    <Row className = "px-3 pt-3">
						    	{allOrder}
						    </Row>
						    }
						  </Tab>
						  <Tab eventKey="pending" title={<div>Pending <Badge className="badgeAll" pill>{pendingOrder.length}</Badge></div>}>
						  {(pendingOrder.length === 0)
						  ?<div class="center-screen">
						    		<img src={emptyhistory} class="emptycart" />	
						    		<h2 class="mt-3" style={{ color: '#e63c49' }}>No pending orders found</h2>
						    	{/*<h6 style={{ color: '#5f87bb' }}>Looks like you haven't ordered <br/>anything yet</h6>*/}
						    </div>
						  :
						    <Row className = "px-3 pt-3">
						    	{pendingOrder}
						    </Row>
						    }
						  </Tab>
						  <Tab eventKey="completed" title={<div>Completed <Badge className="badgeAll" pill>{completedOrder.length}</Badge></div>}>
						    {(completedOrder.length === 0)
    						  ?<div class="center-screen">
    						    		<img src={emptyhistory} class="emptycart" />	
    						    		<h2 class="mt-3" style={{ color: '#e63c49' }}>No completed orders found</h2>
    						    	{/*<h6 style={{ color: '#5f87bb' }}>Looks like you haven't completed <br/>any orders yet</h6>*/}
    						    </div>
    						  :
    						    <Row className = "px-3 pt-3">
    						    	{completedOrder}

    						    </Row>
    						    }
						  </Tab>
						</Tabs>
					</div>
				

		)

	}
