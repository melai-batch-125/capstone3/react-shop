import React, {useState,useEffect} from 'react';
import {regions, provinces, cities, barangays} from 'select-philippines-address';
// import {Row,Container} from 'react-bootstrap';

import AddressCard from './AddressCard';
import emptyhistory from "./../images/emptyhistory.png"
import {Card,Row,Col, Button, Modal,Form} from 'react-bootstrap'
import Swal from 'sweetalert2'
// import food7 from "./../images/food/7.jpg"
import { Link } from 'react-router-dom'
import { trackPromise } from 'react-promise-tracker';
export default function UserAddress(props){
	console.log(props)

	const { addressData, getDetails } = props;
	const [fullName, setFullName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [street, setStreet] = useState('');
	const [displayAddress, setDisplayAddress] = useState([]);
	const [showEdit, setShowEdit] = useState(false);
	const [isDefault, setIsDefault] = useState(false);
	const [isDisabled, setIsDisabled] = useState(true);
	// const [totalState, setTotalState] = useState(0);
	let token = localStorage.getItem('token')
	// let total = 0 
	let addressLine = "";

	const [regionData, setRegion] = useState([]);
    const [provinceData, setProvince] = useState([]);
    const [cityData, setCity] = useState([]);
    const [barangayData, setBarangay] = useState([]);

    const [regionAddr, setRegionAddr] = useState("");
    const [provinceAddr, setProvinceAddr] = useState("");
    const [cityAddr, setCityAddr] = useState("");
    const [barangayAddr, setBarangayAddr] = useState("");

    const region = () => {
        regions().then(response => {
            setRegion(response);
        });
    }

    const province = (e) => {
    	document.getElementById("provinceSelect").style.display = "block";
        setRegionAddr(e.target.selectedOptions[0].text);
        provinces(e.target.value).then(response => {
            setProvince(response);
            setCity([]);
            setBarangay([]);
        });
    }

    const city = (e) => {
    	document.getElementById("citySelect").style.display = "block";
        setProvinceAddr(e.target.selectedOptions[0].text);
        cities(e.target.value).then(response => {
            setCity(response);
        });
    }

    const barangay = (e) => {
    	document.getElementById("barSelect").style.display = "block";
        setCityAddr(e.target.selectedOptions[0].text);
        barangays(e.target.value).then(response => {
            setBarangay(response);
        });
    }

    const brgy = (e) => {
        setBarangayAddr(e.target.selectedOptions[0].text);
    }

    useEffect(() => {
        region()
    }, [])

	useEffect(()=>{
		const list = addressData.sort((a, b) => b.isDefault- a.isDefault).map( (element) => {
			// console.log(product)

			// total = total + (element.price*element.quantity)
			return <AddressCard key={element._id} addressProp={element} getDetails={getDetails}/>
			

		})
		// setTotalState(total)
		setDisplayAddress(list)

		// fetchData()


	},[addressData])

	useEffect(()=>{
		if (fullName !== '' && mobileNo.length === 11 && street !== '' && barangayAddr !== '' && cityAddr !== '' && provinceAddr !== '' && regionAddr !== '') {
			setIsDisabled(false)


		} else {
			setIsDisabled(true)


		}

	}, [fullName,mobileNo,street,barangayAddr,cityAddr,provinceAddr,regionAddr]);

	const [windowDimension, setWindowDimension] = useState(null);

	  useEffect(() => {
	    setWindowDimension(window.innerWidth);
	  }, []);

	  useEffect(() => {
	    function handleResize() {
	      setWindowDimension(window.innerWidth);
	    }

	    window.addEventListener("resize", handleResize);
	    return () => window.removeEventListener("resize", handleResize);
	  }, []);

	  const isMobile = windowDimension <= 768;

	const openEdit = () => {
		
			setShowEdit(true);
		}

	const closeEdit = () => {
		setFullName("");
        setMobileNo("");
        setStreet("");
		setProvince("");
        setCity("");
        setBarangay("");
        region()
        setIsDefault(false);
		setShowEdit(false);
	}
	  const numberOnly = (e) => {
			const re = /^[0-9\b]+$/;
	      if (e.target.value === '' || re.test(e.target.value)) {
	      	if (e.target.value.length > 11) {
	  	           setMobileNo(e.target.value.substring(0,11))
	  	        } else 
	  	        {
	  	            setMobileNo(e.target.value)
	  	        }
	        
	      }
	  	}
	
	const defaultChecked = () => {setIsDefault(true);}
    
	const addNew = (e) => {
			addressLine = [street,barangayAddr,cityAddr,provinceAddr,regionAddr].join(", ");
			/*alert(fullName);
			alert(mobileNo);
			
			alert(addressLine);
			alert(isDefault);*/

			e.preventDefault()
			closeEdit();
			trackPromise(fetch('https://fast-bastion-88049.herokuapp.com/api/users/addNewAddress', {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					fullName,
					mobileNo,
					addressLine,
					isDefault
				})
			})
			.then(result => result.json())
			.then(result => {

				// console.log(result)
				getDetails()

				if(result === true){
					

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "New Address successfully added"
					})

					setFullName('');
					setMobileNo('');
					setStreet('');
					addressLine = "";
					
					setIsDefault(false);
					
					

				} else {
					

					Swal.fire({
						title: "Failed",
						icon: "error",
						text: "Something went wrong"
					})
				}
			}).catch(error => window.location.replace("./serverError")))

		}

	return (
		
		<div>
			<div class="row">
				<div class="col-12 col-md-6">
					
					{(!isMobile) 
	  	    	  	    	 ?<h2>My Address</h2>
						:<h4 className="pt-3">My Address</h4>
					}
				</div>
				<div class="col col-md-6">
				
						<div className="d-flex justify-content-end mb-2">
							<Button className="shadow" variant="outline-primary" onClick={ ()=> openEdit() }><i class="fa fa-plus" aria-hidden="true"></i> Add New Address</Button>
						</div>
				</div>
			</div>
			{(addressData.length === 0)
			?<div class="center-screen">
		    		<img src={emptyhistory} class="emptycart" />	
		    		<h2 class="mt-3" style={{ color: '#e63c49' }}>No Address Yet</h2>
		    		<h6 style={{ color: '#5f87bb' }}>Looks like there is no address yet</h6>
		   			{/*<Link to={"./products"}>
		    			         <a id="buy" style={{borderRadius: "2rem"}} class="btn btn-primary mt-3" href="#about" role="button">SHOP NOW <i class="fa fa-long-arrow-alt-right"></i></a>
		    			          </Link>*/}
		    </div>
		    :<Row className = "px-3 pt-3">
					
						{displayAddress}
				</Row>
		}
				
				{/*Add New Address Modal*/}
			<Modal centered show={showEdit} onHide={closeEdit}>
				<Form onSubmit={ (e)=> addNew(e) }>
					<Modal.Header>
						<Modal.Title>New Address</Modal.Title>
					</Modal.Header>
					<Modal.Body>
					<Row>
					<Col md>
					<Form.Group>
					<Form.Control type="text" maxLength={50} placeholder="Full Name" value={fullName}
									onChange={(e)=> setFullName(e.target.value)}
									required/>
					</Form.Group>
					</Col>
					<Col md>
					<Form.Group>
					<Form.Control className="phone" type="number"  placeholder="Mobile Number" value={mobileNo}
									onChange={(e)=> numberOnly(e)}
									required/>
					</Form.Group>
			   </Col>
			   </Row>
			  {/*<Form.Control as="select">...</Form.Control>*/}
			  <Row>
				<Col md>
				 <Form.Group>
			  <Form.Control as="select" /*value={1}*/ onChange={province} onSelect={region}>
			      <option value='1'>Select Region</option>
			      {
			          regionData && regionData.length > 0 && regionData.map((item) => <option
			              key={item.region_code} value={item.region_code}>{item.region_name}</option>)
			      }
			  </Form.Control>
			  </Form.Group>
			  </Col>
			  <Col md id="provinceSelect" style={{display: 'none' }}>
			  <Form.Group>
			  <Form.Control as="select" onChange={city}>
			      <option>Select Province</option>
			      {

			          provinceData && provinceData.length > 0 && provinceData.map((item) => <option
			              key={item.province_code} value={item.province_code}>{item.province_name}</option>)
			      }
			  </Form.Control>
			  </Form.Group>
			  </Col>
			   </Row>
			  <Row>
			<Col md id="citySelect" style={{display: 'none' }}>
			<Form.Group>
			  <Form.Control as="select" onChange={barangay}>
			      <option>Select City</option>
			      {
			          cityData && cityData.length > 0 && cityData.map((item) => <option
			              key={item.city_code} value={item.city_code}>{item.city_name}</option>)
			      }
			  </Form.Control>
			  </Form.Group>
			  </Col>
			  <Col md id="barSelect" style={{display: 'none' }}>
			  <Form.Group>
			  <Form.Control as="select" onChange={brgy}>
			      <option>Select Barangay</option>
			      {
			          barangayData && barangayData.length > 0 && barangayData.map((item) => <option
			              key={item.brgy_code} value={item.brgy_code}>{item.brgy_name}</option>)
			      }
			  </Form.Control>
			  </Form.Group>
			  </Col>
			   </Row>
			   <Form.Group>
			   <Form.Control type="text" maxLength={100} placeholder="Street Name, Building, House No." value={street}
									onChange={(e)=> setStreet(e.target.value)}
									required/>
			   </Form.Group>
			   {(addressData.length === 0)
			?null
			:<Form.Check type="checkbox" label="Set As Default" onClick={defaultChecked}/>
		}
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Cancel</Button>
						<Button variant="success" type="submit" disabled={isDisabled}>Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		
				</div>
		
		




		)








	}
