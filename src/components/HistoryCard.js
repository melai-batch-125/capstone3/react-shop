import React, {useState} from 'react';
// import UserContext from '../UserContext';

import {Card,Col,Button,Modal,Form} from 'react-bootstrap'
import Swal from 'sweetalert2'
import { trackPromise } from 'react-promise-tracker';
export default function HistoryCard(props){
	// console.log(props)

	const { productProp, fetchData, user,allFalseOrders } = props;
	const [showEdit, setShowEdit] = useState(false);

	
	let token = localStorage.getItem('token')
	let total = 0 
	let subtotal =0
	let subtotalString = "";

let products = productProp.products
const closeEdit = () => {

		setShowEdit(false);
		/*setName("")
		setDescription("")
		setPrice(0)*/
	}
// console.log(products)
const openAdd = () => {
/*	if (localStorage.getItem('email') ===null) {
		setShowAdd(true);
	} else {*/
		setShowEdit(true);
/*	}*/
}
const completeOrder = (id) => {
	setShowEdit(false);
		console.log(id)
		trackPromise(
					fetch('https://fast-bastion-88049.herokuapp.com/api/orders/completeOrder',{ //pang admin lang to
						method: "POST",
						headers: {
							"Content-Type": "application/json",
							"Authorization": `Bearer ${token}`
						},
						body: JSON.stringify({
							orderId: id
						})
					})
					.then(result => result.json())
					.then(result => {
						Swal.fire(
					    		  'Order Completed!',
					    		  'The order is now completed',
					    		  'success'
					    		)
						allFalseOrders()
						// console.log(result.products[0].name)
						// console.log(result.products[0].description)
						// console.log(result.products[0].price)
						// console.log(result.products[0].name)
						// setProductArray(result.products)
					}).catch(error => window.location.replace("./serverError")))
}
const prod = products.map( (element,index) => {

	
	// console.log(element)
	subtotal = element.price*element.quantity
	subtotalString =  subtotal.toLocaleString("en-US")
	
	
	return (

		<div class="row py-2 mb-1" style={{ backgroundColor: (index % 2 == 0) ? '#FFF4E6' : '#fff' }} id="cartrow1">
				  	        <div class="col-5 col-md-2 col-lg-2">
				  	         	<Card.Img variant="top" style={{height:"5rem",objectFit: "contain"}} src={element.imageUrl} />
				  	        </div>
					  	    <div class="align-items-center col-7 col-md-10" id="cartrow">
					  	    <div class="row"><p class="wrapEllipsis">{element.name}</p></div>
					  	    	
					  	    	<div class="row">
						  	    	<div class="col-sm-5 col-md-8 col-12"><h5 className="push">x{element.quantity}</h5></div>
						  	    	<div class="col-sm-7 col-md-4 col-12"><h5>Php {subtotalString}</h5></div>
					  	    	</div>
					  	    </div>

				  	  
				  	    
			  	    </div>



		)



})

return(
	
		(productProp.status == "pending" ) 
		?
		<Col xs = {12} className ="pt-3">
		  	<Card border="warning" onClick={ ()=> openAdd()} style={{ cursor: "pointer" }} className ="cardHistory shadow-lg">
		  		<Card.Header style={{backgroundColor: '#FFA900', color: "white"}}>Order Created Time: <br/>{productProp.purchasedOn}
		  		<br/>
		  		{(user.isAdmin == false)
		  			?
		  		<span bg="warning" class="pull-right">{productProp.status.toUpperCase()}</span>
		  		: <span bg="warning" class="pull-right"><strong>{productProp.facebookName} - {productProp.deliveryOption}</strong></span>
		  	}
		  		</Card.Header>
		  	  <Card.Body>
			  	{prod}

		  	  </Card.Body>
		  	  <Card.Body>
		  	  	<h5 class="orderTotal text-right">Order Total: <br/>Php {productProp.totalAmount.toLocaleString("en-US")}</h5>
		  	  </Card.Body>
		  	  {(user.isAdmin == true)
	  	   	  	    	?null
	  	   	  	    	:
		  	  <Card.Footer>
		  	  
		  	  	<Button  className=" ml-3 my-2 p-2 pull-right" variant="danger" size="sm" onClick={ ()=> openAdd() }
		  			  	   	  	    >
		  			  	   	  	    	View Details
		  			  	   	  	    </Button>
		  	 </Card.Footer>
	  	   	  	    }
		  	  
		  	  
		  	</Card>
		  	<Modal centered  show={showEdit} size="md" onHide={closeEdit} >
		  		<Form /*onSubmit={ (e) => editProduct(e, productId, count) }*/>
		  			<Modal.Header>
		  				<Modal.Title>Order Details</Modal.Title>
		  			</Modal.Header>
		  			<Modal.Body className="scrollbar scrollbar-dusty-grass " style={{'max-height': 'calc(100vh - 300px)', 'overflow-y': 'auto', scrollbarWidth: 'thin' }}>
  	      	    	<div class="row">
  	    	  	    	
  	    	  	    	<div class="col-5 text-right"><h6>Facebook Name</h6></div>
  	    	  	    	 <div class="col-7"><h6><strong>{productProp.facebookName}</strong></h6></div>
  	    	  	    	<div class="col-5 text-right"><h6>Delivery Method</h6></div>
  	    	  	    	 <div class="col-7"><h6><strong>{productProp.deliveryOption}</strong></h6></div>
  	    	  	    	  			{(productProp.deliveryOption === "Pickup")
  	    	  	    	  			?<>
  	    	  	    	  			<div class="col-5 text-right"><h6>Date and Time</h6></div>
  	    	  	    	  			 <div class="col-7"><h6><strong>{productProp.dateTime}</strong></h6></div>
  	    	  	    	  			<div class="col-5 text-right"><h6>Contact Number</h6></div>
  	    	  	    	  			 <div class="col-7"><h6><strong>{productProp.mobileNoOrder}</strong></h6></div>
  	    	  	    	  			
  	    	  	    	  			</>
  	    	  	    	  			:  <>{(productProp.deliveryOption === "Delivery")
  	    	  	    		  			?<>
  	    	  	    		  			<div class="col-5 text-right"><h6>Name</h6></div>
  	    	  	    		  			 <div class="col-7"><h6><strong>{productProp.fullNameOrder}</strong></h6></div>
  	    	  	    		  			<div class="col-5 text-right"><h6>Address</h6></div>
  	    	  	    		  			 <div class="col-7"><h6><strong>{productProp.addressLineOrder}</strong></h6></div>
  	    	  	    		  			 <div class="col-5 text-right"><h6>Contact Number</h6></div>
  	    	  	    		  			  <div class="col-7"><h6><strong>{productProp.mobileNoOrder}</strong></h6></div>
  	    	  	    		  			
  	    	  	    		  			</>
  	    	  	    		  			: <>
  	    	  	    		  			<div class="col-5 text-right"><h6>Branch</h6></div>
  	    	  	    		  			 <div class="col-7"><h6><strong>{productProp.addressLineOrder}</strong></h6></div>
  	    	  	    		  			 <div class="col-5 text-right"><h6>Contact Number</h6></div>
  	    	  	    		  			  <div class="col-7"><h6><strong>{productProp.mobileNoOrder}</strong></h6></div>
  	    	  	    		  			</>
  	    	  	    		  			}</>
  	    	  	    	  			}
  	    	  			  			<br/>
  	    	  			  			<div class="col-5 text-right"><h6>Payment Method</h6></div>
  	    	  			  			 <div class="col-7"><h6><strong>{productProp.payment}</strong></h6></div>
  	    	  			  			
  	    	  			  			{(productProp.payment === "Gcash")
  	    	  			  			?<>
  	    	  			  			<div class="col-5 text-right"><h6>Gcash Name</h6></div>
  	    	  			  			 <div class="col-7"><h6><strong>{productProp.referenceName}</strong></h6></div>
  	    	  			  			 <div class="col-5 text-right"><h6>Gcash Reference Number</h6></div>
  	    	  			  			  <div class="col-7"><h6><strong>{productProp.referenceNum}</strong></h6></div>
  	    	  			  			</>
  	    	  			  			:  <>{(productProp.payment === "Bpi")
  	    	  				  			?<>
  	    	  				  			<div class="col-5 text-right"><h6>BPI Name</h6></div>
  	    	  			  			 <div class="col-7"><h6><strong>{productProp.referenceName}</strong></h6></div>
  	    	  			  			 <div class="col-5 text-right"><h6>BPI Reference Number</h6></div>
  	    	  			  			  <div class="col-7"><h6><strong>{productProp.referenceNum}</strong></h6></div>
  	    	  				  			</>
  	    	  				  			: null
  	    	  				  			}</>
  	    	  			  			}
  	      	    	</div>
		  			
		  			
		  				</Modal.Body>
		  			<Modal.Footer  style={{borderBottom: '0 none'}}>
		  				<Button variant="secondary" onClick={closeEdit}>Close</Button>
		  				  	  {(user.isAdmin == true)
		  			  	   	  	    	?
		  				  	  
		  			  	     	 
		  			  	   	  	 <Button  className=" ml-3 my-2 p-2" variant="danger" size="sm" onClick={ ()=> completeOrder(productProp._id) }
		  			  	   	  	    >
		  			  	   	  	    	Complete Order
		  			  	   	  	    </Button>
		  			  	   	  	   
		  			  	   	  	  
		  			  	   	  	:null
		  			  	   	  	    }
		  				{/*<Button variant="success" type="submit"><i class="fa fa-plus" aria-hidden="true"></i> Add to Cart</Button>*/}
		  			</Modal.Footer>
		  		</Form>
		  	</Modal>
		 </Col>
		 	// onClick={ ()=> checkOut(totalState) }
		 :<Col xs = {12} className ="pt-3">
		  	<Card border="success" onClick={ ()=> openAdd()} style={{ cursor: "pointer" }} className ="cardHistory shadow-lg">
		  		<Card.Header  style={{backgroundColor: '#00B74A', color: "white" }}>Order Completed Time: <br/>{productProp.purchasedOn}
		  		{(user.isAdmin == false)
		  				  			?
		  				  		<span bg="warning" class="pull-right">{productProp.status.toUpperCase()}</span>
		  				  		: <span bg="warning" class="pull-right"><strong>{productProp.facebookName} - {productProp.deliveryOption}</strong></span>
		  				  	}
		  		</Card.Header>
		  	  <Card.Body>
			  	{prod}

		  	  </Card.Body>
		  	   <Card.Footer>
		  	  	<h5 class="orderTotal text-right">Order Total: <br/>Php {productProp.totalAmount.toLocaleString("en-US")}</h5>
		  	  </Card.Footer>
		  	 {/* <Card.Footer>
	  	     	<div class="row pull-right mr-1 justify-align-center align-items-center">
	  	   	  	    <h5 class="orderTotal text-right">Order Total: <br/>Php {productProp.totalAmount}</h5>
	  	   	  	</div>
	  	   	  	</Card.Footer>*/}
		  	</Card>
		  			  	<Modal centered  show={showEdit} size="md" onHide={closeEdit} >
		  			  		<Form /*onSubmit={ (e) => editProduct(e, productId, count) }*/>
		  			  			<Modal.Header>
		  			  				<Modal.Title>Order Details</Modal.Title>
		  			  			</Modal.Header>
		  			  			<Modal.Body className="scrollbar scrollbar-dusty-grass " style={{'max-height': 'calc(100vh - 300px)', 'overflow-y': 'auto', scrollbarWidth: 'thin' }}>
		  	  	      	    	<div class="row">
		  	  	    	  	    	
		  	  	    	  	    	<div class="col-5 text-right"><h6>Facebook Name</h6></div>
		  	  	    	  	    	 <div class="col-7"><h6><strong>{productProp.facebookName}</strong></h6></div>
		  	  	    	  	    	<div class="col-5 text-right"><h6>Delivery Method</h6></div>
		  	  	    	  	    	 <div class="col-7"><h6><strong>{productProp.deliveryOption}</strong></h6></div>
		  	  	    	  	    	  			{(productProp.deliveryOption === "Pickup")
		  	  	    	  	    	  			?<>
		  	  	    	  	    	  			<div class="col-5 text-right"><h6>Date and Time</h6></div>
		  	  	    	  	    	  			 <div class="col-7"><h6><strong>{productProp.dateTime}</strong></h6></div>
		  	  	    	  	    	  			<div class="col-5 text-right"><h6>Contact Number</h6></div>
		  	  	    	  	    	  			 <div class="col-7"><h6><strong>{productProp.mobileNoOrder}</strong></h6></div>
		  	  	    	  	    	  			
		  	  	    	  	    	  			</>
		  	  	    	  	    	  			:  <>{(productProp.deliveryOption === "Delivery")
		  	  	    	  	    		  			?<>
		  	  	    	  	    		  			<div class="col-5 text-right"><h6>Name</h6></div>
		  	  	    	  	    		  			 <div class="col-7"><h6><strong>{productProp.fullNameOrder}</strong></h6></div>
		  	  	    	  	    		  			<div class="col-5 text-right"><h6>Address</h6></div>
		  	  	    	  	    		  			 <div class="col-7"><h6><strong>{productProp.addressLineOrder}</strong></h6></div>
		  	  	    	  	    		  			 <div class="col-5 text-right"><h6>Contact Number</h6></div>
		  	  	    	  	    		  			  <div class="col-7"><h6><strong>{productProp.mobileNoOrder}</strong></h6></div>
		  	  	    	  	    		  			
		  	  	    	  	    		  			</>
		  	  	    	  	    		  			: <>
		  	  	    	  	    		  			<div class="col-5 text-right"><h6>Branch</h6></div>
		  	  	    	  	    		  			 <div class="col-7"><h6><strong>{productProp.addressLineOrder}</strong></h6></div>
		  	  	    	  	    		  			 <div class="col-5 text-right"><h6>Contact Number</h6></div>
		  	  	    	  	    		  			  <div class="col-7"><h6><strong>{productProp.mobileNoOrder}</strong></h6></div>
		  	  	    	  	    		  			</>
		  	  	    	  	    		  			}</>
		  	  	    	  	    	  			}
		  	  	    	  			  			<br/>
		  	  	    	  			  			<div class="col-5 text-right"><h6>Payment Method</h6></div>
		  	  	    	  			  			 <div class="col-7"><h6><strong>{productProp.payment}</strong></h6></div>
		  	  	    	  			  			
		  	  	    	  			  			{(productProp.payment === "Gcash")
		  	  	    	  			  			?<>
		  	  	    	  			  			<div class="col-5 text-right"><h6>Gcash Name</h6></div>
		  	  	    	  			  			 <div class="col-7"><h6><strong>{productProp.referenceName}</strong></h6></div>
		  	  	    	  			  			 <div class="col-5 text-right"><h6>Gcash Reference Number</h6></div>
		  	  	    	  			  			  <div class="col-7"><h6><strong>{productProp.referenceNum}</strong></h6></div>
		  	  	    	  			  			</>
		  	  	    	  			  			:  <>{(productProp.payment === "Bpi")
		  	  	    	  				  			?<>
		  	  	    	  				  			<div class="col-5 text-right"><h6>BPI Name</h6></div>
		  	  	    	  			  			 <div class="col-7"><h6><strong>{productProp.referenceName}</strong></h6></div>
		  	  	    	  			  			 <div class="col-5 text-right"><h6>BPI Reference Number</h6></div>
		  	  	    	  			  			  <div class="col-7"><h6><strong>{productProp.referenceNum}</strong></h6></div>
		  	  	    	  				  			</>
		  	  	    	  				  			: null
		  	  	    	  				  			}</>
		  	  	    	  			  			}
		  	  	      	    	</div>
		  			  			
		  			  			
		  			  				</Modal.Body>
		  			  			<Modal.Footer  style={{borderBottom: '0 none'}}>
		  			  				<Button variant="secondary" onClick={closeEdit}>Close</Button>
		  			  				{/*<Button variant="success" type="submit"><i class="fa fa-plus" aria-hidden="true"></i> Add to Cart</Button>*/}
		  			  			</Modal.Footer>
		  			  		</Form>
		  			  	</Modal>

		  		      	
		 </Col>
)
}