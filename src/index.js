import React,{useState,useEffect} from 'react';
import ReactDOM from 'react-dom';
import { usePromiseTracker } from "react-promise-tracker";
import Loader from 'react-loader-spinner';
import BackToTop from './components/BackToTop';

/*

bootstrap
*/
import App from './App';
// import React, {useState, useEffect} from 'react'
import { trackPromise } from 'react-promise-tracker';
/*import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';*/

const LoadingIndicator = props => {
  const { promiseInProgress } = usePromiseTracker();
 
   
   return promiseInProgress &&
    

       <div class="modal modal-fullscreen" id="loaderDiv" 
       style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "white"
          }}>
        <Loader type="ThreeDots" color="#47bcfe" height="5rem" width="5rem" />
       </div>

   
 };

//dynamic solution to not existing email to clear the localstorage
/*window.onload = function openApp() {
    const email = localStorage.getItem('email');

    if(email !== null){
       trackPromise(
       fetch("https://fast-bastion-88049.herokuapp.com/api/users/checkEmail", 
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            email: email
          })

        }
       )
       .then(result => result.json())
       .then(result => {
        // console.log(result)
        if (result === false) {
           localStorage.clear();
        }

      }).catch(error => window.location.replace("./serverError")));
    }
}*/
// const isClearOnce = 
/*const [isOldDevice,setIsOldDevice] = useState(true)

if (isOldDevice) {
  useEffect(() => {
    window.onload = function openApp() {
        const email = localStorage.getItem('email');

        if(email !== null){
           trackPromise(
           fetch("https://fast-bastion-88049.herokuapp.com/api/users/checkEmail", 
            {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify({
                email: email
              })

            }
           )
           .then(result => result.json())
           .then(result => {
            // console.log(result)
            if (result === false) {
               localStorage.clear();
            }

          }).catch(error => window.location.replace("./serverError")));
        } else {
          setIsOldDevice(false);
        }
    }
  }, [isNewDevice])
}*/

 // localStorage.clear();
ReactDOM.render( 
  <div>
  
  <App/>
  <BackToTop/>
  <LoadingIndicator/>
  </div>
  , document.getElementById('root')
);

