import React, {useState,useEffect} from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Profile from './pages/Profile';
import ErrorPage from './components/ErrorPage';
import ServerErrorPage from './components/ServerErrorPage';
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login'
import Users from './pages/Users'
import PlaceOrder from './pages/PlaceOrder'
import ImageUpload from './pages/ImageUpload';
import Cloud from './pages/Cloud';
import UserContext from './UserContext';

import { trackPromise } from 'react-promise-tracker';

import Swal from 'sweetalert2'
import AddProduct from './pages/AddProduct';
import Carts from './pages/Carts';
import History from './pages/History';
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";

export default function App() {
// localStorage.clear();
// const [open,setOpen] = useState(true)
	const [user,setUser] = useState({
		id: null,
		isAdmin: null

	});

	const unsetUser = () => {
		localStorage.clear();
		setUser({id: null,isAdmin: null});

	}

/* gamitin local storage
useEffect(()=>{
	if (open) {	unsetUser();
		setOpen(false)
	}


},[open==true])*/


// const [isOldDevice,setIsOldDevice] = useState(true)


// user deleted

if (localStorage.getItem("isOldDevice")==null) {
	localStorage.setItem("isOldDevice",true);
}

const isOldDevice = localStorage.getItem("isOldDevice");
console.log(isOldDevice)
/*if (isOldDevice=="true") {
console.log("try")*/
    window.onload = function openApp() {
        const email = localStorage.getItem('email');

        if(email !== null){
        	console.log("refreshing with fetching")
           trackPromise(
           fetch("https://fast-bastion-88049.herokuapp.com/api/users/checkEmail", 
            {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify({
                email: email
              })

            }
           )
           .then(result => result.json())
           .then(result => {
            // console.log(result)
            if (result === false) {
               localStorage.clear();
             /*  Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Your account has been deleted. Create a new one."
				})*/
               Swal.fire({
                 	title: "Failed",
					icon: "error",
					text: "Your account has been deleted. Create a new one.",
                /* showCancelButton: true,*/
                 confirmButtonColor: '#3085d6',
                /* cancelButtonColor: '#d33',*/
                 confirmButtonText: 'Sign up'
               }).then((result) => {
                 if (result.isConfirmed) {
                  window.location.replace("./register")
                 }
               })
            }  else {
          localStorage.setItem("isOldDevice",false);
        }

          }).catch(error => window.location.replace("./serverError")));
        } else {
         localStorage.setItem("isOldDevice",false);
        }
    }
 
// }


	useEffect(()=>{
		let token = localStorage.getItem('token')
		trackPromise(
		fetch('https://fast-bastion-88049.herokuapp.com/api/users/details',
			{
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			}
		)
		.then(result => result.json())
		.then( result => {
			// console.log(result)
			// console
			if (typeof result._id !== "undefined") {
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				})
			} else {
				setUser({
					id: null,
					isAdmin: null
				})
			}
		}).catch(error => window.location.replace("./serverError"))
    );


	},[])


	return(
		<UserContext.Provider value = {{user, setUser, unsetUser}}>
			<BrowserRouter>
				
					<Switch>
						{/*no navbar*/}
						{/*<Route exact path = "/rtrttr" component = {} />*/}
						<Route exact path= "/serverError" component={ServerErrorPage} />
						<>
						<AppNavbar/>
						<Route exact path = "/" component = {Home} />
						<Route exact path = "/products" component = {Products}/>
						<Route exact path = "/register" component = {Register}/>
						<Route exact path = "/login" component = {Login}/>
						<Route exact path= "/addProduct" component={AddProduct} />
						<Route exact path= "/cart" component={Carts} />
						<Route exact path= "/history" component={History} />
						<Route exact path= "/users" component={Users} />
						<Route exact path = "/profile" component = {Profile}/>
						<Route exact path= "/placeOrder" component={PlaceOrder} />
						<Route exact path= "/image" component={ImageUpload} />
						<Route exact path= "/cloud" component={Cloud} />
						
						
						</>

						<Route component = {ErrorPage}/>
					</Switch>
			</BrowserRouter>
		</UserContext.Provider>


		)



}