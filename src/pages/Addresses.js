import React, {useState,useEffect,useContext} from 'react';
/*import Banner from './../components/Banner';
import Highlights from './../components/Highlights';
import Map from './../components/Map';*/
import UserContext from './../UserContext';

import {
	Container,Form,Table,Button

} from 'react-bootstrap';
import { trackPromise } from 'react-promise-tracker';
import UserAddress from './../components/UserAddress';
export default function Addresses() {

	const {user} = useContext(UserContext);
	const [address, setAddress] = useState([]);
	const [fetchDone, setfetchDone] = useState(false)
	let token = localStorage.getItem("token")

	const getDetails = () => {

		trackPromise(
		fetch('https://fast-bastion-88049.herokuapp.com/api/users/details',{ 
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)
			setAddress(result.addresses)
			setfetchDone(true)
		}).catch(error => window.location.replace("./serverError"))
		);
	}

	useEffect(() => {
		getDetails()
	     }, []);


	return (
		(fetchDone === true) 
		?
		<Container>
		<UserAddress addressData = {address} getDetails={getDetails}  user = {user}/>
		</Container>

		:null
		


		)
}