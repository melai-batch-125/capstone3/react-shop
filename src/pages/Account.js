import React, {useState,useEffect} from 'react';
import Swal from 'sweetalert2';
import {
	Container,Form,Table,Button,InputGroup,Modal

} from 'react-bootstrap';
import { trackPromise } from 'react-promise-tracker';
export default function Account() {

	// const [details, setDetails] = useState*/
	const [email, setEmail] = useState('');
	const [facebookName, setFacebookName] = useState('');
	const [contactNo, setContactNo] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	const [isDisabledSave, setIsDisabledSave] = useState(true);
	let token = localStorage.getItem("token")
	const [showEdit, setShowEdit] = useState(false);
	const [facebookNameInput, setFacebookNameInput] = useState(false);
	const [contactNoInput, setContactNoInput] = useState(false);
	const [emailInput, setEmailInput] = useState(false);
	const getDetails = () => {

		trackPromise(
		fetch('https://fast-bastion-88049.herokuapp.com/api/users/details',{ 
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {

			setEmail(result.email)
			setFacebookName(result.facebookName)
			setContactNo(result.contactNo)
			setPassword(result.password)

		}).catch(error => window.location.replace("./serverError"))
		);
	}

	useEffect(() => {
		getDetails()
	     }, []);
	useEffect(()=>{
		if ( password !== '' && verifyPassword !== '' && password === verifyPassword) {
			setIsDisabled(false)


		} else {
			setIsDisabled(true)


		}

	}, [password, verifyPassword]);

	useEffect(()=>{
		if (emailInput === true || facebookNameInput === true || contactNoInput === true) {
			setIsDisabledSave(false);}
	}, [emailInput, facebookNameInput, contactNoInput]);

	const [windowDimension, setWindowDimension] = useState(null);

	  useEffect(() => {
	    setWindowDimension(window.innerWidth);
	  }, []);

	  useEffect(() => {
	    function handleResize() {
	      setWindowDimension(window.innerWidth);
	    }

	    window.addEventListener("resize", handleResize);
	    return () => window.removeEventListener("resize", handleResize);
	  }, []);

	  const isMobile = windowDimension <= 768;
	  const numberOnly = (e) => {
			const re = /^[0-9\b]+$/;
	      if (e.target.value === '' || re.test(e.target.value)) {
	      	if (e.target.value.length > 11) {
	  	           setContactNo(e.target.value.substring(0,11))
	  	        } else 
	  	        {
	  	            setContactNo(e.target.value)
	  	        }
	        
	      }
	  	}
	  	  
	const openEdit = () => {
		
			setShowEdit(true);
		}

	const closeEdit = () => {

		setShowEdit(false);
	}
	const editProduct = (e) => {

		e.preventDefault()

		trackPromise(fetch(`https://fast-bastion-88049.herokuapp.com/api/users/updateDetails`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				email,
				facebookName,
				contactNo
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result) //updated product document

			getDetails()
			setIsDisabledSave(true)
			if(typeof result !== "undefined"){
				// alert("success")
				localStorage.setItem("email",email)
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Account successfully updated!"
				})

				// closeEdit();
			} else {

			

				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Something went wrong!"
				})
			}
		}).catch(error => window.location.replace("./serverError")))
	}
	const editPass = (e) => {

		e.preventDefault()
		closeEdit();
		trackPromise(fetch(`https://fast-bastion-88049.herokuapp.com/api/users/changePassword`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				password: password
			})
		})
		.then(result => result.json())
		.then(result => {
			
			// getDetails()

			if(typeof result !== "undefined"){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Account successfully updated!"
				})

				
			} else {

			

				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Something went wrong!"
				})
			}
		}).catch(error => window.location.replace("./serverError")))
	}

	return (
		<Container fluid>
			{(!isMobile) 
	    	  	    	 ?<h2>Account</h2>
				:<h4 className="pt-3">Account</h4>
			}
			<Form onSubmit={ (e) => editProduct(e) }>
					
						<Form.Group controlId="email">
							<Form.Label>Email</Form.Label>
							<Form.Control
								type="text"
								value={email}
								maxLength={30}
								onChange={ (e)=> setEmail(e.target.value)}
								onInput={()=>setEmailInput(true)}
							/>
						</Form.Group>
						<Form.Group controlId="fbname">
							<Form.Label>Facebook Name</Form.Label>
							<Form.Control
								type="text"
								maxLength={30}
								value={facebookName}
								onChange={ (e)=> setFacebookName(e.target.value)}
								onInput={()=>setFacebookNameInput(true)}
							/>
						</Form.Group>
						<Form.Group controlId="conNum">
							<Form.Label>Contact Number</Form.Label>
							<Form.Control
								type="nuumber"
								// minLength={11} maxLength={11}
								value={contactNo}
								onChange={ (e)=> numberOnly(e)}
								onInput={()=>setContactNoInput(true)}
							/>
						</Form.Group>
						<Form.Group controlId="password">
							<Form.Label>Password</Form.Label>
							<InputGroup className="mb-3">
							<Form.Control
							disabled
								type="text"
								value="******"
								// onChange={ (e)=> setPrice(e.target.value)}
							/>
										  	        <Button onClick={ ()=> openEdit() } variant="primary" id="button-addon2"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							    </Button>
							  </InputGroup>
						</Form.Group>
						<Button className="pull-right" variant="danger" type="submit" disabled={isDisabledSave}>Save</Button>
						</Form>


					{/*modal for change password*/}
					<Modal style={{ overlay: { background: 'black' } }} centered show={showEdit} onHide={closeEdit}>
		  	        <div class=" logprod">
		  	        			{/*<h4 className="text-center">Change Password</h4>*/}
		  	        					{(!isMobile) 
		  	        			    	  	    	 ?<h4 className="text-center">Change Password</h4>
		  	        						:<h5 className="text-center">Change Password</h5>
		  	        					}
		  	        			<Form onSubmit={(e) => editPass(e)}>
		  	        			  <Form.Group className="mb-3 formlog" controlId="formBasicEmail">
		  	        			    <Form.Label className="formlog center-block">New Password</Form.Label>
		  	        			    <Form.Control type="password" minLength={5} maxLength={20} placeholder="Enter new password" onChange = {(e) => setPassword(e.target.value)}/>
		  	        			  </Form.Group> 

		  	        			  <Form.Group className="mb-3 formlog" controlId="formBasicPassword">
		  	        			    <Form.Label className="formlog">Verify Password</Form.Label>
		  	        			    <Form.Control type="password"  minLength={5} maxLength={20} placeholder="Enter verify password" onChange = {(e) => setVerifyPassword(e.target.value)}/>
		  	        			  </Form.Group>
		  	        		
		  	        			  <div className="text-center">
		  	        			  <Button className="mr-3" variant="secondary" onClick={closeEdit}>Cancel</Button>
		  	        			  <Button /*id="buttonlog"*/ variant="primary" type="submit" disabled={isDisabled}>
		  	        			    Submit
		  	        			  </Button>
		  	        			  </div>
		  	        			</Form>
		  	        			
		  	        		</div>
  	      </Modal>

		</Container>


		)
}