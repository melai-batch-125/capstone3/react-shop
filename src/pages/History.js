import React, {useState, useEffect,useContext} from 'react';
/*react-bootstrap component*/
import {Container} from 'react-bootstrap'
import emptyhistory from "./../images/emptyhistory.png"
import ErrorPage from './../components/ErrorPage';
import UserHistory from './../components/UserHistory';
import UserContext from './../UserContext';
import { trackPromise } from 'react-promise-tracker';
import { Link } from 'react-router-dom'
export default function History(){
	
	const [products, setProducts] = useState([]);
	const {user} = useContext(UserContext);
	const [render, setRender] = useState(false)
	const [fetchDone, setfetchDone] = useState(false)
	let token = localStorage.getItem('token')
	const fetchData = () => {
		trackPromise(
		fetch('https://fast-bastion-88049.herokuapp.com/api/orders/allPrevOrder',{ 
			method: "POST",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			// console.log(result)
			// console.log(result.products[0].name)
			// console.log(result.products[0].description)
			// console.log(result.products[0].price)
			// console.log(result.products[0].name)
			setProducts(result)
			setfetchDone(true)
		})
		)
	}

	const allFalseOrders = () => {

		trackPromise(
		fetch('https://fast-bastion-88049.herokuapp.com/api/orders/all',{ 
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)
			result = result.filter(elem => elem.isCurrent != true)
			console.log(result)
			setProducts(result)
			setfetchDone(true)
		}).catch(error => window.location.replace("./serverError"))
		);
	}

	useEffect(() => {
		if(user.isAdmin===false || user.isAdmin===true) {
	       setRender(true)
	   }
	     }, [user.isAdmin]);

	
/*	useEffect( () => {
			
		console.log(user.isAdmin)
			if(user.isAdmin===true) {
				// console.log("Try")
				allFalseOrders()

			} else if (user.isAdmin===false) {
				fetchData()	
				
			}
		


	}, [user.isAdmin])
*/

	useEffect( () => {
			
		if(render === true) {
				if(user.isAdmin===true) {
					// console.log("Try")
					allFalseOrders()

				} else if (user.isAdmin===false) {
					fetchData()	
					
				}

		// } else if (!email) {
		// 	fetchData()	
			
		// }
	}

	}, [render])
	console.log("render",render)
	console.log("user.isAdmin",user.isAdmin)
	console.log("length:",products.length)
	console.log("products:",products)
	return(
		(fetchDone === true) 
		?(products.length === 0)
			?<div class="center-screen">
		    		<img src={emptyhistory} class="emptycart" />	
		    		<h2 class="mt-3" style={{ color: '#e63c49' }}>No orders yet</h2>
		    		<h6 style={{ color: '#5f87bb' }}>Looks like there is no order yet</h6>
		    		{ (user.isAdmin!==true) 
		   			?<Link to={"./products"}>
		    			         <a id="buy" style={{borderRadius: "2rem"}} class="btn btn-primary mt-3" href="#about" role="button">SHOP NOW <i class="fa fa-long-arrow-alt-right"></i></a>
		    			          </Link>
		    			          :null
		    			      }
		    </div>
			:
			<Container className="pb-4">
				{/*{ProductCards}*/}
				{ (user.isAdmin===true) 
					? <UserHistory productData = {products} allFalseOrders={allFalseOrders} user = {user}/>
					: <UserHistory productData = {products} fetchData={fetchData}  user = {user}/>
				}
			</Container>
		:null
			
	)
}
