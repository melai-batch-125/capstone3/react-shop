import React, {useState, useEffect,useContext} from 'react';
/*react-bootstrap component*/
import {Container} from 'react-bootstrap'

import AdminView from './../components/AdminView';
import UserView from './../components/UserView';
import ServerErrorPage from './../components/ServerErrorPage';
import UserContext from './../UserContext';
import { trackPromise } from 'react-promise-tracker';

export default function Products(){
	
	const [products, setProducts] = useState([]);
	const [allproducts, setAllProducts] = useState([]);
	const component = "./products"
	const [render, setRender] = useState(false)
	const [fetchDone, setfetchDone] = useState(false)
	const {user} = useContext(UserContext);
	const email = localStorage.getItem("email")

	const fetchData = () => {
		console.log("wew")
		
		trackPromise(fetch('https://fast-bastion-88049.herokuapp.com/api/products/active',{ //pang user lang to
		/*fetch('https://shrouded-brook-21767.herokuapp.com/api/products/products',{*/
				

			method: "GET"
		})
		.then(result => result.json())
		.then(result => {
			// console.log(result)
			setProducts(result)
			setfetchDone(true)
		}).catch(error => window.location.replace("./serverError")))
		
	}

	const allProductData = () => {
		let token = localStorage.getItem('token')
		// console.log(token);
		trackPromise(fetch('https://fast-bastion-88049.herokuapp.com/api/products/all',{ //pang admin lang to
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			// console.log(result)
			setAllProducts(result)
			setfetchDone(true)
		}).catch(error => window.location.replace("./serverError")))
		
	}
	useEffect(() => {
		if(user.isAdmin===false || user.isAdmin===true) {
	       setRender(true)
	   }
	     }, [user.isAdmin]);

	useEffect( () => {
			console.log(render)
		if(render === true) {
				if(user.isAdmin===true) {
					// console.log("Try")
					allProductData()

				} else if (user.isAdmin===false) {
					fetchData()	
					
				}

		} else if (!email) {
			fetchData()	
			
		}

	}, [render,user.isAdmin])

	return(
		(fetchDone === false)
		?null
		:
		<Container className="p-3" id="containerProduct">
			{/*{ProductCards}*/}
			{ (user.isAdmin===true ) 
				? <AdminView productData = {allproducts} allProductData={allProductData}/>
				: <UserView productData = {products}/>
			}
		</Container>
	)
}
