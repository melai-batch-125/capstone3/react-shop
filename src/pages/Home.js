import React from 'react';
import Banner from './../components/Banner';
import Highlights from './../components/Highlights';
import Map from './../components/Map';

import {
	Container

} from 'react-bootstrap';

export default function Home() {
	return (
		<Container fluid>
			<Banner/>
			<Highlights/>
			<Map/>
		</Container>


		)
}