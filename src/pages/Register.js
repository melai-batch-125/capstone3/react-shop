import React, {useState,useEffect,useContext} from 'react';

import {
	Form,
	Button,
	InputGroup

} from 'react-bootstrap';
import { MDBInput} from "mdbreact"
//state
import UserContext from './../UserContext';
// import {useHistory} from 'react-router-dom';
import {useHistory,Redirect} from 'react-router-dom';

import Swal from 'sweetalert2';
import { trackPromise } from 'react-promise-tracker';

export default function Register() {
	const [facebookName, setFacebookName] = useState('');
	const [contactNo, setContactNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	const {user} = useContext(UserContext);
	let history = useHistory();

	  const numberOnly = (e) => {
		 const re = /^[0-9\b]+$/;
	  if (e === '' || re.test(e)) {
	  	if (e.length > 11) {
	           setContactNo(e.substring(0,11))
	        } else 
	        {
	           setContactNo(e)
	        }
	     
	  }
	  	}

	useEffect(()  => {
			document.body.style.backgroundImage = "url('images/bgg.png')";
			document.body.style.backgroundRepeat = "no-repeat";
			document.body.style.backgroundSize = "cover";
			document.body.style.backgroundPosition = "top center";
			document.body.style.minWidth = "100vw";
			document.body.style.minHeight = "100vh";
	    return () => {
	        document.body.style.backgroundImage = "none";
	    };
	});

	useEffect(()=>{
		if (facebookName !== '' && email !== '' && contactNo.length === 11  && password !== '' && verifyPassword !== '' ) {
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}

	}, [facebookName, email, password, verifyPassword,contactNo]);

	function register(e) {
		if(password !== verifyPassword) {
			e.preventDefault();
			Swal.fire({
					title: 'Password verification does not match',
					icon: 'error',
					text: 'Please match the passwords.'
				})
		} else if(contactNo[0] !== "0" || contactNo[1] !== "9"){
			e.preventDefault(); 
			
			Swal.fire({
					title: 'Contact number is not valid.',
					icon: 'error',
					text: 'Please enter a valid contact number.'
				})
		} else
		{
		e.preventDefault();
		trackPromise(
		fetch("https://fast-bastion-88049.herokuapp.com/api/users/checkEmail", 
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email
				})

			}
		)
		.then(result => result.json())
		.then(result => {
			// console.log(result)
			if (result === true) {
				Swal.fire({
					title: 'Duplicate email found',
					icon: 'error',
					text: 'Please choose another email'
				})

				// history.push('/register');
			} else {
				trackPromise(
				fetch("https://fast-bastion-88049.herokuapp.com/api/users/register",
					{
						method: "POST",
						headers: {
							"Content-Type": "application/json"
						},
						body: JSON.stringify({
							facebookName,
							email,
							contactNo,
							password
						})
					}
				)
				.then( result => result.json())
				.then( result => {
					console.log(result)
					if(result === true){
						Swal.fire({
							title: 'Registered Successfully',
							icon: 'success',
							text: 'Welcome to PickMe Shop'
						})

						history.push('/login');
					} else {
						
						Swal.fire({
							title: 'Something went wrong',
							icon: 'error',
							text: 'Please try again'
						})
					}
				}).catch(error => window.location.replace("./serverError")))
			}
		}).catch(error => window.location.replace("./serverError")))
}
	}

	return (
		(user.id !== null || localStorage.getItem('email') !==null )
		? <Redirect to="/"/>
		: 
		
		<div class="mt-3 reg shadow-lg">
			<h3 className="text-center">Register</h3>
			<Form onSubmit={(e) => register(e)}>
			<MDBInput label="Facebook Name" icon="user" maxLength={50} onChange={(e)=>setFacebookName(e.target.value)}/>
			<MDBInput label="Email address" icon="envelope" type="email" validate maxLength={50} onChange={(e)=>setEmail(e.target.value)}/>
			{/*<MDBInput label="Contact Number" icon="mobile-alt" minLength={11} maxLength={11} onChange={(e)=>numberOnly(e.target.value)}/>*/}

			<MDBInput label="Password" icon="lock" type="password" validate minLength={5} maxLength={20} onChange={(e)=>setPassword(e.target.value)}/>
			<MDBInput label="Verify Password" icon="exclamation-triangle" type="password" validate minLength={5} maxLength={20} onChange={(e)=>setVerifyPassword(e.target.value)}/>
			{/*<MDBInput label="Contact number" icon="mobile" type="hidden" minLength={5} validate value = {contactNo} />*/}
			<MDBInput label="Contact number" icon="mobile" type="number"  value = {contactNo} onChange={(e)=>numberOnly(e.target.value)}/>
			{/*<InputGroup className="mb-3">
			    <InputGroup.Text id="basic-addon1"><i class="fas fa-mobile-alt"></i></InputGroup.Text>
			   
			    <Form.Control type="number" minLength={11} maxLength={11} placeholder="Contact number" value = {contactNo}
			    onChange = {(e) => numberOnly(e.target.value)}/>
			  </InputGroup>*/}
			
			 {/* <Form.Group className="mb-3" controlId="formBasicFacebookName">
			    <Form.Label>Facebook Name</Form.Label>
			    <Form.Control type="text" maxLength={50} placeholder="Enter facebook name" value = {facebookName}
			    onChange = {(e) => setFacebookName(e.target.value)}/>
			  </Form.Group> 

			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control type="email" maxLength={50} placeholder="Enter email" value = {email}
			    onChange = {(e) => setEmail(e.target.value)}/>
			  </Form.Group> 

			  <Form.Group className="mb-3" controlId="formBasicContactNo">
			    <Form.Label>Contact Number</Form.Label>
			    <Form.Control type="text" minLength={11} maxLength={11} placeholder="Enter contact number" value = {contactNo}
			    onChange = {(e) => numberOnly(e)}/>
			  </Form.Group> 

			  <Form.Group className="mb-3" controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control type="password" minLength={5} maxLength={20} placeholder="Password" value = {password} onChange = {(e) => setPassword(e.target.value)}/>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="formVerifyPassword">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control type="password" minLength={5} maxLength={20} placeholder="Verify Password" value = {verifyPassword} onChange = {(e) => setVerifyPassword(e.target.value)}/>
			  </Form.Group>*/}
			  <div className="text-center">
			  <Button variant="primary" type="submit" disabled={isDisabled}>
			    Submit
			  </Button>
			  </div>
			</Form>
		</div>
		
		)



}