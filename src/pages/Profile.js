import React from 'react';
/*import Banner from './../components/Banner';
import Highlights from './../components/Highlights';*/
import History from './History';
import Account from './Account';
import Addresses from './Addresses';

import {
	Container, Accordion,Row,Col,Tab,Nav,NavDropdown

} from 'react-bootstrap';

export default function Profile() {
  
  
	return (
		<Container className="py-3">
		<h1>Profile</h1>
		<Tab.Container id="left-tabs-example" defaultActiveKey="first">
  <Row>
    <Col sm={3}>
      <Nav variant="pills" className="flex-column">
        <Nav.Item>
          <Nav.Link eventKey="first">My Account</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="second">Addresses</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="third">My Purchase</Nav.Link>
        </Nav.Item>
        
      </Nav>
    </Col>
    <Col sm={9}>
      <Tab.Content>
        <Tab.Pane eventKey="first">
         <Account/>
        </Tab.Pane>
        <Tab.Pane eventKey="second" id="address">
        <Addresses/>
        </Tab.Pane>
        <Tab.Pane eventKey="third">
        <History/>
        </Tab.Pane>
      </Tab.Content>
    </Col>
  </Row>
</Tab.Container>
		</Container>


		)
}