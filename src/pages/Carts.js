import React, {useState, useEffect,useContext} from 'react';
/*react-bootstrap component*/
import {Container} from 'react-bootstrap'

import ErrorPage from './../components/ErrorPage';
import UserCart from './../components/UserCart';
import UserContext from './../UserContext';

import { Link } from 'react-router-dom'
import emptycart from "./../images/emptycart.png"

import { trackPromise } from 'react-promise-tracker';
export default function Carts(){
	
	const [products, setProducts] = useState([]);
	const {user} = useContext(UserContext);
	let token = localStorage.getItem('token')
	const fetchData = () => {

		trackPromise(
		fetch('https://fast-bastion-88049.herokuapp.com/api/orders/single',{ 
			method: "POST",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			// console.log(result)
			// console.log(result.products[0].name)
			// console.log(result.products[0].description)
			// console.log(result.products[0].price)
			// console.log(result.products[0].name)
			setProducts(result.products)
		}).catch(error => window.location.replace("./serverError"))
		);
	}

	

	
	useEffect( () => {
			
		
			fetchData()	
			console.log("duh?")

		


	}, [])


	/*if (trackPromise) {
		return null;
	}*/

	return(
		(products.length !== 0)
			?<Container className="p-4">
				{/*{ProductCards}*/}
				{ (user.isAdmin===true) 
					? <ErrorPage/>
					: <UserCart productData = {products} fetchData={fetchData}/>
				}
				</Container>
			:


			<div class=" center-screen">
			{/*<h1>Your cart</h1>*/}

					<img src={emptycart} class="emptycart" />	
					<h2 class="mt-3" style={{ color: '#e63c49' }}>Oops! Your cart is empty!</h2>
				<h6 style={{ color: '#5f87bb' }}>Looks like you haven't added <br/>anything to your cart yet</h6>
				<Link to={"./products"}>
							         <a id="buy" style={{borderRadius: "2rem"}} class="btn btn-primary mt-3" href="#about" role="button">SHOP NOW <i class="fa fa-long-arrow-alt-right"></i></a>
							          </Link>
				</div>
		

	)
}
