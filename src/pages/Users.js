import React, {useState, useEffect,useContext} from 'react';
/*react-bootstrap component*/
import {Container} from 'react-bootstrap'
import AdminUsers from './../components/AdminUsers';
import UserContext from './../UserContext';
import { Redirect} from 'react-router-dom';
import { trackPromise } from 'react-promise-tracker';

export default function Users(){
	
	const [specificUser, setSpecificUser] = useState([]);
	const [allusers, setAllUsers] = useState([]);
	const [render, setRender] = useState(false)
	const {user} = useContext(UserContext);
	const email = localStorage.getItem("email")
	const token = localStorage.getItem('token')
	const specificUserData = () => {
		// console.log("wew")
		
		trackPromise(fetch('https://fast-bastion-88049.herokuapp.com/api/users/details',{ //pang user lang to
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			// console.log(result)
			setSpecificUser(result)
		}).catch(error => window.location.replace("./serverError")))
		
	}

	const allUsersData = () => {
		// let token = localStorage.getItem('token')
		// console.log(token);
		trackPromise(fetch('https://fast-bastion-88049.herokuapp.com/api/users/all',{ //pang admin lang to
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			// console.log(result)
			setAllUsers(result)
		}).catch(error => window.location.replace("./serverError")))
		
	}


	useEffect(() => {
		if(user.isAdmin===false || user.isAdmin===true) {
	       setRender(true)
	   }
	     }, [user.isAdmin]);

	useEffect( () => {
			console.log(render)
		if(render === true) {
			if(user.isAdmin===true) {
				// console.log("Try")
				allUsersData()

			} else if (user.isAdmin===false) {
				specificUserData()				
			}
		} else if (!email) {
			specificUserData()				
		}

	}, [render,user.isAdmin])


	console.log("isAdmin",user.isAdmin);


	return(
		<Container className="py-2">
			{ (user.isAdmin===true) 
				? <AdminUsers userData = {allusers} allUsersData={allUsersData}/>
				: <Redirect to="/"/>
			}
		</Container>
	)
}
