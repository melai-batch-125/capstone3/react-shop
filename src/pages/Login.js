import React, {useState,useEffect,useContext} from 'react';
import Swal from 'sweetalert2'
import {
	Form,
	Button,
	InputGroup

} from 'react-bootstrap';

//state
import { MDBInput} from "mdbreact"
//context
import UserContext from './../UserContext';

import {Redirect} from 'react-router-dom';
import { trackPromise } from 'react-promise-tracker';

export default function Login() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	//destructure context object
	const {user,setUser} = useContext(UserContext);
	// const [pwd, setPwd] = useState('');
	const [isRevealPwd, setIsRevealPwd] = useState(false);

	useEffect(()  => {
	document.body.style.backgroundImage = "url('images/bgg.png')";
	document.body.style.backgroundRepeat = "no-repeat";
	document.body.style.backgroundSize = "cover";
	document.body.style.backgroundPosition = "top center";
	document.body.style.minWidth = "100vw";
	document.body.style.minHeight = "100vh";


    return () => {
        document.body.style.backgroundImage = "none";
    };
});

	useEffect(()=>{
		if (email !== '' && password !== '') {
			setIsDisabled(false)


		} else {
			setIsDisabled(true)


		}

	}, [email, password]);

	function login(e) {
		e.preventDefault();
		trackPromise(
		fetch('https://fast-bastion-88049.herokuapp.com/api/users/login',
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					username: email,
					password: password
				})

			}
		)
		.then(result => result.json())
		.then(result => {
			console.log(result); //access:token

			if(typeof result.access !== "undefined") 
			{
				localStorage.setItem("token", result.access);
				setUser({email: email});
				localStorage.setItem('email',email); //can be facebook name

				setEmail('');
				setPassword('');
				userDetails(result.access);
			} else {
				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "The email address/facebook name or password you’ve entered is incorrect."
				})
			}


		}).catch(error => window.location.replace("./serverError")))

		const userDetails = (token) => {
			trackPromise(
			fetch('https://fast-bastion-88049.herokuapp.com/api/users/details', 
				{
					method: "GET",
					headers: {
						"Authorization": `Bearer ${token}`
					}
				}
			)
			.then(result => result.json())
			.then(result => {

				console.log(result)
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				})



				//update
				// setUser({email: email});
				localStorage.setItem('email',result.email);

				setEmail('');
				setPassword('');


			

			}))
		}

		

		// alert('Successfully logged in!')

		

	}

	// if (user.email !== null) {
	// 	return <Redirect to="/"/>
	// }
	return (
		(user.id !== null || localStorage.getItem('email') !==null )
		? <Redirect to="/"/>

		: <div class="my-5 p-4 log shadow-lg" id="logDiv">
			<h3 className="text-center pb-2">Login</h3>
			<Form  onSubmit={(e) => login(e)}>
			<MDBInput label="Email address/FB Name" icon="user" maxLength={50} onChange={(e)=>setEmail(e.target.value)}/>
			  {/*<Form.Group className="mb-3" controlId="formBasicUsername">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control type="text" maxLength={50} placeholder="Enter email or facebook name" value = {email}
			    onChange = {(e) => setEmail(e.target.value)}/>
			  </Form.Group> */}
			  <MDBInput label="Password" icon="lock" maxLength={20} type="password" validate onChange={(e)=>setPassword(e.target.value)}/>
		{/*	   <MDBBtn
			                 color="primary"
			                 className="m-0 px-3 py-2 z-depth-1"
			               >
			                 BUTTON
			               </MDBBtn>*/}
			
			 {/* <Form.Group className="mb-3" controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <InputGroup className="mb-3">
			    <Form.Control type={isRevealPwd ? "text" : "password"} placeholder="Password"  maxLength={20} value = {password} onChange = {(e) => setPassword(e.target.value)}/>
			  	
			  	        <Button onClick={() => setIsRevealPwd(prevState => !prevState)} variant="warning" id="button-addon2">
      {isRevealPwd ? <i class="fa fa-eye-slash" aria-hidden="true"></i> : <i class="fa fa-eye" aria-hidden="true"></i>}
    </Button>
  </InputGroup>

			  </Form.Group>*/}
			  <Button className="m-1" variant="warning" size="sm" 
							onClick={ ()=>  Swal.fire({
		   title:'Admin Credentials',
		   html:'Email: admin@mail.com <br/> Password: admin',
		   icon:'info'
		 
		 }) }>
								Show Admin
							</Button>
			  
			  <div className="text-center pt-3">
			  <Button variant="primary" type="submit" disabled={isDisabled}>
			    Login
			  </Button>
			  <p className="font-small grey-text  pt-2">
			                  Don't have an account?
			                  <a
			                    href="/register"
			                    className="dark-grey-text font-weight-bold ml-1"
			                  >
			                    Sign up
			                  </a>
			                </p>
			  </div>
			</Form>
			
		</div>



		)

}