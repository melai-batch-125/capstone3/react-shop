import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'
import { trackPromise } from 'react-promise-tracker';

export default function AddProduct(){
	
	const { user } = useContext(UserContext);
	const history = useHistory();


	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(true);

	const [image, setImage ] = useState("");
	const [ url, setUrl ] = useState("");


	let token = localStorage.getItem('token')
	// console.log(token)
/*
		useEffect(()  => {
			document.body.style.backgroundImage = "url('images/logdiv.jpg')";
			document.body.style.backgroundRepeat = "no-repeat";
			document.body.style.backgroundSize = "cover";
			document.body.style.backgroundPosition = "top center";
			document.body.style.minWidth = "100vw";
			document.body.style.minHeight = "100vh";


		    return () => {
		        document.body.style.backgroundImage = "none";
		    };
		});	*/

	useEffect(()=>{

		if(name !== '' && description !== '' && price !== 0 && image !==''){
			setIsActive(true);
		}else{
			setIsActive(false);
		}

	}, [name, description, price,image]);



	function addProduct(e){
		e.preventDefault();

		// const uploadImage = () => {
			const data = new FormData()
			data.append("file", image)
			data.append("upload_preset", "productImage")
			data.append("cloud_name","lexusnexus")

			trackPromise(
			fetch("  https://api.cloudinary.com/v1_1/lexusnexus/image/upload",{
					method:"post",
					body: data
				})
			.then(resp => resp.json())
			.then(data => {
				setUrl(data.url)
				console.log(data.url)
				saveProduct(data.url)
			})
			.catch(error => window.location.replace("./serverError")))
		// }

		const saveProduct = (url) => {
			trackPromise(
			fetch('https://fast-bastion-88049.herokuapp.com/api/products/addProduct', {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price,
					imageUrl: url
				})
			})
			.then(res => res.json())
			.then(data => {
			

				if(data === true){

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully added"
					})
					setName('');
					setDescription('');
					setPrice(0);

					history.push('/products');

				} else {

					Swal.fire({
						title: "Failed",
						icon: "error",
						text: "Something went wrong"
					})

				}
			}).catch(error => window.location.replace("./serverError")))
				}
		
	};

	  const numberOnly = (e) => {
			const re = /^[0-9\b]+$/;
	      if (e.target.value === '' || re.test(e.target.value)) {
	         setPrice(e.target.value)
	      }
	  	}
	return(
		(user.isAdmin !== true)
		? <Redirect to="/"/>

		: 
		<div class="my-5 addProd shadow-lg" /*style={{width:"100%"}}*/>
			<h1>Create Product</h1>
			<Form onSubmit={ e => addProduct(e)}>
				<Form.Group>
					<Form.Label>Product Name:</Form.Label>
					<Form.Control
						type="text"
						maxLength={50}
						placeholder="Enter Name of the product"
						value={name}
						onChange={(e) => setName(e.target.value)}
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control
						type="text"
						maxLength={100}
						placeholder="Enter Description"
						value={description}
						onChange={(e) => setDescription(e.target.value)}
					/>
				</Form.Group>


				<Form.Group>
					<Form.Label>Price:</Form.Label>
					<Form.Control
						type="number"
						maxLength={5}
						value={price}
						onChange={(e) => numberOnly(e)}
					/>
				</Form.Group>

				<Form.Group controlId="formFile" className="mb-3">
				    <div>
						<input type="file" onChange= {(e)=> setImage(e.target.files[0])}></input>
						{/*<button onClick={uploadImage}>Upload</button>*/}
					</div>
					{/*<div>
						<h6>Preview: </h6>
						<img src={url} width="50px"/>
					</div>*/}
				  </Form.Group>

				{ 
					(isActive === true) ? 
						<Button type="submit" variant="primary">Submit</Button>
					:
						<Button type="submit" variant="primary" disabled>Submit</Button>
				}
				
				
			</Form>
		</div>
		)
}
