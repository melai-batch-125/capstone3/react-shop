import React, {useState,useEffect} from 'react';
import {regions, provinces, cities, barangays} from 'select-philippines-address';
import {
	Container,Form,Row,Col,Card,Button,Modal

} from 'react-bootstrap';
import { trackPromise } from 'react-promise-tracker';

import gcashCode from "./../images/gcash.png"
import bpiCode from "./../images/bpi.png"

import Swal from 'sweetalert2'
export default function PlaceOrder() {
    const [products, setProducts] = useState([]);
	let totalAmount = localStorage.getItem("total")
    totalAmount = parseInt(totalAmount);
    let token = localStorage.getItem('token')
    let subtotal =0
    let subtotalString = "";
    let dateTime = "";
    let referenceName = "";
    let referenceNum = "";
    let addressLine = "";
    
    const [deliveryOption, setDelOpt] = useState("Pickup");
    const [payment, setPaymentOpt] = useState("Cash");

    const [facebookName, setFacebookName] = useState("");
    const [contactNo, setContactNo] = useState("");

    const [address, setAddress] = useState([]);

      let mobileNoOrder = "";
     let splitAddress = [];
     let fullNameOrder = "";
     let addressLineOrder ="";

    const [isDisabledPlace, setIsDisabledPlace] = useState(true);

    const [pick, setPick] = useState(false);
    const [del, setDel] = useState(false);
    const [ship, setShip] = useState(false);

    const [cash, setCash] = useState(false);
    const [gcash, setGcash] = useState(false);
    const [bpi, setBpi] = useState(false);



    const [selectAddress, setSelectAddress] = useState("");

    const [selectRegion, setSelectRegion] = useState("");
    const [selectProvince, setSelectProvince] = useState("");

    const [selectGcash, setSelectGcash] = useState("");
    const [selectGRef, setSelectGRef] = useState("");

    const [selectBpi, setSelectBpi] = useState("");
    const [selectBRef, setSelectBRef] = useState("");

    const [showAdd, setShowAdd] = useState(false);
    const openAdd = () => setShowAdd(true);
    const closeAdd = () => setShowAdd(false);
    const closeToCart = () => window.location.replace("./cart");
    const defaultChecked = () => {setIsDefault(true);}
    const [fullName, setFullName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [street, setStreet] = useState('');
    const [isDefault, setIsDefault] = useState(false);
    const [isDisabled, setIsDisabled] = useState(true);

    let locShip = ["North Luzon", "Bicol Region"];
    let northShip = [
        'Baguio City',
        'Balanga Bataan',
        'Olongapo City',
        'Iba Zambales',
        'Sta Cruz Zambales',
        'Lingayen',
        'Carmen',
        'Dagupan City',
        'Agoo',
        'Sison',
        'Apalit Pampanga',
        'San Fernado Pampanga',
        'Dau',
        'Tarlac',
        'San Jose',
        'Aritao',
        'Santiago',
        'Roxas',
        'Ilagan',
        'Tabuk',
        'Tuguegarao',
        'Laoag',
        'Vigan',
        'San Fernando La Union',
        'Bangued Abra',
        'Candon'
    ];

    let bicolShip = [
        'Daet',
        'Naga',
        'Legaspi',
        'Bulan',
        'Sta Elena',
        'Camarines Norte'

    ];

        // const [regionData, setRegion] = useState([]);
        const [provinceDataShip, setProvinceShip] = useState([]);

        const [regionAddrShip, setRegionAddrShip] = useState("");
        const [provinceAddrShip, setProvinceShipAddr] = useState("");

        // const region = () => {
        //         setRegion(locShip);   
        // }

        const provinceFunc = (e) => {
            setRegionAddrShip(e.target.value);
            console.log(regionAddrShip)
            if (e.target.value==="North Luzon") {
                document.getElementById("provinceSelectShip").style.display = "block";
                setProvinceShip(northShip.sort());
            } else if (e.target.value==="Bicol Region") {
                document.getElementById("provinceSelectShip").style.display = "block";
                setProvinceShip(bicolShip.sort());
            }
        }


        const [regionData, setRegion] = useState([]);
        const [provinceData, setProvince] = useState([]);
        const [cityData, setCity] = useState([]);
        const [barangayData, setBarangay] = useState([]);

        const [regionAddr, setRegionAddr] = useState("");
        const [provinceAddr, setProvinceAddr] = useState("");
        const [cityAddr, setCityAddr] = useState("");
        const [barangayAddr, setBarangayAddr] = useState("");

        const region = () => {
            regions().then(response => {
                setRegion(response);
            });
        }

        const province = (e) => {
            document.getElementById("provinceSelect").style.display = "block";
            setRegionAddr(e.target.selectedOptions[0].text);
            provinces(e.target.value).then(response => {
                setProvince(response);
                setCity([]);
                setBarangay([]);
            });
        }

        const city = (e) => {
            document.getElementById("citySelect").style.display = "block";
            setProvinceAddr(e.target.selectedOptions[0].text);
            cities(e.target.value).then(response => {
                setCity(response);
            });
        }

        const barangay = (e) => {
            document.getElementById("barSelect").style.display = "block";
            setCityAddr(e.target.selectedOptions[0].text);
            barangays(e.target.value).then(response => {
                setBarangay(response);
            });
        }

        const brgy = (e) => {
            setBarangayAddr(e.target.selectedOptions[0].text);
        }

        useEffect(() => {
            region()
        }, [])

        useEffect(()=>{
            if (fullName !== '' && mobileNo.length === 11 && street !== '' && barangayAddr !== '' && cityAddr !== '' && provinceAddr !== '' && regionAddr !== '') {
                setIsDisabled(false)


            } else {
                setIsDisabled(true)


            }

        }, [fullName,mobileNo,street,barangayAddr,cityAddr,provinceAddr,regionAddr]);

    const months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    let threeDays = [];
    let timePick = [
        "8:00 AM", //0
        "9:00 AM", //1
        "10:00 AM", //2
        "11:00 AM", //3
        "12:00 PM", //4
        "1:00 PM", //5
        "2:00 PM", //6
        "3:00 PM", //7
        "4:00 PM", //8
        "5:00 PM" //9
        ];
    // let timePick =[];
    let timePickDate = new Date();
    if(timePickDate.getHours()>=8 && timePickDate.getHours()<=16) {
        let hourPick = timePickDate.getHours()
        console.log(hourPick)
        timePick = timePick.slice(hourPick-7);
         console.log(timePick)
    } 

    for (let i = 0; i <= 4 ; i++) {
        let d = new Date();
        if(d.getHours()>16)
        {
            d.setDate(d.getDate() + (i+1));
        } else {
             d.setDate(d.getDate() + i);
            
        }
       
       let month = months[d.getMonth()];
            let day =d.getDate();
            threeDays.push(month+" "+day)

    }
     console.log(threeDays)

    const [selectDate, setSelectDate] = useState(threeDays[0]);
    const [selectTime, setSelectTime] = useState(timePick[0]);
    
    const showPickUp = () => {
        document.getElementById("pick").style.display = "block";
        document.getElementById("delivery").style.display = "none";
        document.getElementById("shipping").style.display = "none";
        setDelOpt("Pickup");
        setSelectDate(threeDays[0]);
        setSelectTime(timePick[0]);
        setSelectAddress("");
    }
    const showDelivery = () => {
        document.getElementById("pick").style.display = "none";
        document.getElementById("shipping").style.display = "none";
        if (address.length===0) {
            document.getElementById("delivery").style.display = "none";
            openAdd();
        } else {
            document.getElementById("delivery").style.display = "block";
            setDelOpt("Delivery");
            setSelectDate("");
            setSelectTime("");
            setSelectAddress(fullAddress[0]);
        }
       

    }
    const showShipping = () => {
        document.getElementById("pick").style.display = "none";
        document.getElementById("delivery").style.display = "none";
        document.getElementById("shipping").style.display = "block";
        setDelOpt("Shipping");
        setSelectDate("");
        setSelectTime("");
        setSelectAddress("");
    }

    const showCash = () => {
        document.getElementById("cash").style.display = "block";
        document.getElementById("gcash").style.display = "none";
        document.getElementById("bpi").style.display = "none";
        setPaymentOpt("Cash");
        setSelectGcash("");
        setSelectGRef("");
        setSelectBpi("");
        setSelectBRef("");

    }
    const showGcash = () => {
        document.getElementById("cash").style.display = "none";
        document.getElementById("gcash").style.display = "block";
        document.getElementById("bpi").style.display = "none";
        setPaymentOpt("Gcash");
        setSelectBpi("");
        setSelectBRef("");
    }
    const showBPI = () => {
        document.getElementById("cash").style.display = "none";
        document.getElementById("gcash").style.display = "none";
        document.getElementById("bpi").style.display = "block";
        setPaymentOpt("Bpi");
        setSelectGcash("");
        setSelectGRef("");
      
    }

    const fetchData = () => {

        trackPromise(
        fetch('https://fast-bastion-88049.herokuapp.com/api/orders/single',{ 
            method: "POST",
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
        .then(result => result.json())
        .then(result => {
            setProducts(result.products)
        }).catch(error => window.location.replace("./serverError"))
        );
    }

    

    
    useEffect( () => {
            
        
            fetchData() 
            console.log("duh?")

        


    }, [])
    useEffect(()=>{ //kulang paaaa pang catch ng mga blankong input like gref at gname
       /* if (deliveryOption !== "" && payment !== "") { */ 
            if (((deliveryOption === "Pickup" && selectDate !== "" && selectTime !== "" )||
                (deliveryOption === "Delivery" && selectAddress !== "" )||
                (deliveryOption === "Shipping" &&  regionAddrShip !== "" && provinceAddrShip !== ""))&&
                ((payment === "Cash")||
                (payment === "Gcash" && selectGcash !== "" && selectGRef !== "" )||
                (payment === "Bpi" && selectBpi !== "" && selectBRef !== "" ))) {

                setIsDisabledPlace(false)

            // }
            
        } else {
            setIsDisabledPlace(true)
        }
    }, [deliveryOption,payment,selectGcash,selectGRef,selectBpi,selectBRef,regionAddrShip,provinceAddrShip]);
    const prod = products.map( (element,index) => {

        
        // console.log(total)
        subtotal = element.price*element.quantity
        subtotalString =  subtotal.toLocaleString("en-US")
        
        return (
            <div class="row mb-1 py-3 align-items-center" id="cartrow1" style={{ backgroundColor: (index % 2 === 0) ? '#FFF4E6' : '#fff' }}>  
                <div class="col-5 col-md-2 col-lg-2">
                                <Card.Img variant="top" style={{height:"5rem",objectFit: "contain"}} src={element.imageUrl} />
                            </div>
                            <div class="col-7 col-md-3 col-lg-3" id="cartrow">
                                <p class="wrapEllipsis">{element.name}</p>
                            </div>
                        {/*<h5>Price:</h5>*/}

                        <div class="col-5 col-sm-0 d-block d-sm-none"></div>
                            <div class="col-7 col-sm-4 ml-auto pt-3 pt-md-0  col-md-2 col-lg-2 ">
                                <h6 className="push">Php {element.price}</h6>
                            </div>
                            <div class="col-5 col-sm-0 d-block d-sm-none"></div>
                            <div class="col-7 col-sm-4 pt-3 pt-md-0 col-md-2 col-lg-2">
                          
                               <h5 >x{element.quantity}</h5>
                             
                            </div>
                            <div class="col-5 col-sm-0 d-block d-sm-none"></div>
                            <div class="col-7 col-sm-4 pt-3 pt-md-0 col-md-3 col-lg-3" id="sub">
                            {/*<p>SubTotal:</p>*/}
                                <h5 >Php {subtotalString}</h5>
                            </div>
                                              
            </div>
            )
    })
    const getDetails = () => {

        trackPromise(
        fetch('https://fast-bastion-88049.herokuapp.com/api/users/details',{ 
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)
            setAddress(result.addresses)
            setFacebookName(result.facebookName)
            setContactNo(result.contactNo)

        }).catch(error => window.location.replace("./serverError"))
        );
    }

    useEffect(() => {
        getDetails()
         }, []);
    const fullAddress = address.sort((a, b) => b.isDefault- a.isDefault).map( (element) => {

        return [element.fullName,element.mobileNo,element.addressLine].join(" / ");
    })

    const placeOrder = (e) => {
            // addressLine = [street,barangayAddr,cityAddr,provinceAddrShip,regionAddrShip].join(", ");
        /*    alert("Date: "+selectDate);
            alert("Time: "+selectTime);*/
            
            // alert("Address: "+selectAddress);
         /*   alert("Region: "+regionAddrShip);
             alert("Province: "+provinceAddrShip);*/
            /* alert("deliveryOption: "+deliveryOption);
            alert("payment: "+payment);
             alert("selectGcash: "+selectGcash);
             alert("selectGRef: "+selectGRef);
            alert("selectBpi: "+selectBpi);
             alert("selectBRef: "+selectBRef);*/

             if (payment === "Gcash") {
                referenceName = selectGcash;
                referenceNum = selectGRef;
             } else if(payment === "Bpi") {
                referenceName = selectBpi;
                referenceNum = selectBRef;
             }
             //delivery option
              
               if (deliveryOption === "Pickup") {
                dateTime = [selectDate,selectTime].join(" - ");
                 mobileNoOrder = contactNo;
               } else if (deliveryOption === "Delivery") {
                splitAddress = selectAddress.split(" / ");
                 console.log("array",splitAddress);
                fullNameOrder = splitAddress[0];
                mobileNoOrder = splitAddress[1];
                addressLineOrder = splitAddress[2];
                // alert("Address: "+addressLineOrder);
               } else if (deliveryOption === "Shipping") {
               
                mobileNoOrder = contactNo;
                addressLineOrder = [regionAddrShip,provinceAddrShip].join(" - ");
               }
             

           /*  alert("totalAmount: "+ typeof(total));
             alert("deliveryOption: "+typeof(deliveryOption));
              alert("dateTime: "+typeof(dateTime));
              alert("payment: "+typeof(payment));
             alert("referenceName: "+typeof(referenceName));
              alert("referenceNum: "+typeof(referenceNum));
              alert("fullNameOrder: "+typeof(fullNameOrder));
             alert("mobileNoOrder: "+typeof(mobileNoOrder));
              alert("addressLineOrder: "+typeof(addressLineOrder));
               alert("facebookName: "+typeof(facebookName));*/
             trackPromise(
                        fetch('https://fast-bastion-88049.herokuapp.com/api/orders/checkOut',{ 
                            method: "POST",
                            headers: {
                                "Content-Type": "application/json",
                                "Authorization": `Bearer ${token}`
                            },
                            body: JSON.stringify({
                                totalAmount,
                                deliveryOption,
                                dateTime,
                                payment,
                                referenceName,
                                referenceNum,
                                fullNameOrder,
                                mobileNoOrder,
                                addressLineOrder,
                                facebookName
                            })
                        })
                        .then(result => result.json())
                        .then(result => {
                            console.log(result)
                            localStorage.setItem("cartCount", 0);
                            Swal.fire(
                                      'Order Confirmed!',
                                      'Your order has been placed.',
                                      'success'
                                    ).then((result) => {window.location.replace("./history");})
                        }).catch(error => window.location.replace("./serverError")))


        }
       const addNew = (e) => {
            addressLine = [street,barangayAddr,cityAddr,provinceAddr,regionAddr].join(", ");
            // addressLine = [fullName,mobileNo,addressLine].join(" / ");
            /*alert(fullName);
            alert(mobileNo);
            
            alert(addressLine);
            alert(isDefault);*/

            e.preventDefault()
            closeAdd();
            trackPromise(fetch('https://fast-bastion-88049.herokuapp.com/api/users/addNewAddress', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                },
                body: JSON.stringify({
                    fullName,
                    mobileNo,
                    addressLine,
                    isDefault
                })
            })
            .then(result => result.json())
            .then(result => {
                document.getElementById("delivery").style.display = "block";
                setDelOpt("Delivery");
                setSelectDate("");
                setSelectTime("");
                addressLine = [fullName,mobileNo,addressLine].join(" / ");
                setSelectAddress(addressLine);
                // console.log(result)
                getDetails()

            
                // showDelivery();

                if(result === true){
                    

                    Swal.fire({
                        title: "Success",
                        icon: "success",
                        text: "New Address successfully added"
                    })

                    setFullName('');
                    setMobileNo('');
                    setStreet('');
                    addressLine = "";
                    
                    setIsDefault(false);
                    
                    

                } else {
                    

                    Swal.fire({
                        title: "Failed",
                        icon: "error",
                        text: "Something went wrong"
                    })
                }
            }).catch(error => window.location.replace("./serverError")))

        }

         const numberOnly = (e) => {
            const re = /^[0-9\b]+$/;
          if (e.target.value === '' || re.test(e.target.value)) {
            if (e.target.value.length > 11) {
                   setMobileNo(e.target.value.substring(0,11))
                } else 
                {
                    setMobileNo(e.target.value)
                }
            
          }
        }
	return (
		<Container className="p-4">
            <Card className="shadow">
                <Card.Header style={{backgroundColor: '#FFA900', color: "white"}}>
                    Checkout
                </Card.Header>
                <Card.Body>
                    <Row>
                        <Col md={5}>
                			<h4>Delivery Options:</h4>
                            <Form>
                            
                                <div key='inline-radio' className="mb-3">
                                  <Form.Check
                                  defaultChecked
                                    inline
                                    label="Pick Up"
                                    name="group1"
                                    type="radio"
                                    id='inline-radio-1'
                                    onClick={showPickUp}
                                  />
                                  <Form.Check
                                    inline
                                    label="Delivery"
                                    name="group1"
                                    type="radio"
                                    id='inline-radio-2'
                                    onClick={showDelivery}
                                  />
                                  <Form.Check
                                    inline
                                name="group1"
                                    label="Shipping"
                                    type="radio"
                                    id='inline-radio-3'
                                    onClick={showShipping}
                                  />
                                </div>
                            </Form>
                        </Col>
                        <Col md={7}>
                        <div id="pick" style={{display: 'block' }}>
                            <h4>Pick Up</h4>
                            <h5>Date:</h5>
                            <Form.Control as="select" id="pickupSelect"
                              value={selectDate}
                              onChange={e => {
                                setSelectDate(e.target.value);
                              }}
                            >
                              <option disabled>Select Date</option>
                            
                              {

                                 threeDays.map((item,index) => <option
                                      key={item.index} value={item}>{item}</option>)
                              }
                          </Form.Control>
                         <h5>Time:</h5>
                            <Form.Control as="select" id="pickupSelect2"
                            value={selectTime}
                              onChange={e => {
                                setSelectTime(e.target.value);
                              }}>
                              <option disabled>Select Time</option>
                            
                              {

                                 timePick.map((item,index) => <option
                                      key={item.index} value={item}>{item}</option>)
                              }
                          </Form.Control>
                        </div>
                         <div id="delivery" style={{display: 'none' }}>
                            <h4>Delivery</h4>
                            <h5>Address:</h5>
                            <Form.Control as="select" style={{ width: "100%" }} 
                            value={selectAddress}
                            onChange={e => {
                              setSelectAddress(e.target.value);}}
                            >
                              <option disabled>Select Address</option>
                            
                              {

                                 fullAddress.map((item,index) => <option
                                      key={item.index} value={item}>{item}</option>)
                              }
                          </Form.Control>
                        </div>
                        <div id="shipping" style={{display: 'none' }}>
                            <h4>Shipping</h4>
                            <h5>Location:</h5>
                             <Row>
                                <Col md className="p-3"> 
                                    <Form.Control as="select" /*value={1}*/ onChange={(e) => provinceFunc(e)}>
                                      <option>Select Region</option>
                                      {
                                          locShip.map((item,index) => <option
                                      key={item.index} value={item}>{item}</option>)
                                      }
                                    </Form.Control>
                                </Col>
                                <Col md id="provinceSelectShip" style={{display: 'none' }} className="p-3">
                                    <Form.Control as="select" onChange={e => {
                              setProvinceShipAddr(e.target.value);}}>
                                      <option>Select Province</option>
                                      {

                                          provinceDataShip.map((item,index) => <option
                                      key={item.index} value={item}>{item}</option>)
                                      }
                                    </Form.Control>
                                </Col>
                            </Row>
                        </div>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
            <Card className="shadow my-3">
                <Card.Body>
                   
                            <h4>Products Ordered</h4>
                     
                            {prod}
                        
                </Card.Body>
            </Card>
            <Card className="shadow">
                <Card.Body>
                    <Row>
                        <Col md={4}>
                            <h4>Payment method</h4>
                            <Form>
                                <div key='inline-radio-group2' className="mb-3">
                                  <Form.Check
                                  defaultChecked
                                    inline
                                    label="Cash"
                                    name="group2"
                                    type="radio"
                                    id='inline-radio-4'
                                    onClick={showCash}
                                  />
                                  <Form.Check
                                    inline
                                    label="Gcash"
                                    name="group2"
                                    type="radio"
                                    id='inline-radio-5'
                                    onClick={showGcash}
                                  />
                                  <Form.Check
                                    inline
                                name="group2"
                                    label="BPI"
                                    type="radio"
                                    id='inline-radio-6'
                                    onClick={showBPI}
                                  />
                                </div>
                            </Form>
                        </Col>
                        <Col md={8}>
                            <div id="cash" style={{display: 'none' }}>
                                {/*<h2>Cash</h2>*/}
                            </div>
                            <div id="gcash" style={{display: 'none' }}>
                           {/* <Row>
                                <h4>Gcash</h4>

                            </Row>*/}
                            <Row>
                            <Col sm={12} md={6}>
                                <img /*class="mx-1 navlogo"*/ src={gcashCode} width="100%" style={{ borderRadius: "10%" }}  />
                            </Col>
                            <Col sm={12} md={6} >
                            <Row className="p-3"> 
                                <Form.Label>Gcash Name:</Form.Label>
                                    <Form.Control type="text" placeholder="Gcash Name"
                                    value={selectGcash}
                                    maxLength={50}
                                     required
                                    onChange={(e) => 
                                      setSelectGcash(e.target.value)}
                                    />
                                </Row>  
                                <Row className="p-3">
                                <Form.Label>Reference Number:</Form.Label>
                                    <Form.Control className="phone" type="text" placeholder="Reference Number"
                                    value={selectGRef}
                                     minLength={13}
                                    maxLength={13}
                                     required
                                    onChange={(e) => 
                                      setSelectGRef(e.target.value)}
                                    />
                                    </Row>
                                    </Col> 
                                </Row>
                            </div>
                            <div id="bpi" style={{display: 'none' }}>
                           {/* <Row>
                                <h4>BPI</h4>
                            </Row>*/}
                                <Row>
                               <Col sm={12} md={6} >
                                   <img /*class="mx-1 navlogo"*/ src={bpiCode} width="100%" style={{ borderRadius: "10%" }}  />
                               </Col>
                               <Col sm={12} md={6} >
                               <Row className="p-3"> 
                                   <Form.Label>BPI Account Name:</Form.Label>
                                       <Form.Control type="text" placeholder="BPI Account Name"
                                       value={selectBpi}
                                       maxLength={50}
                                        required
                                       onChange={e => {
                                         setSelectBpi(e.target.value);}}
                                       />
                                   </Row>  
                                   <Row className="p-3">
                                   <Form.Label>Reference Number:</Form.Label>
                                       <Form.Control className="phone" type="text" placeholder="Reference Number"
                                       value={selectBRef}
                                        minLength={10}
                                       maxLength={14}
                                       required
                                       onChange={e => {
                                         setSelectBRef(e.target.value);}}
                                       />
                                       </Row>
                                       </Col> 
                                   </Row>
                            </div>
                        </Col>
                    </Row>
                </Card.Body>
                <Card.Footer>
                    <h5 class="orderTotal text-right">Total Payment: {/*<br/>*/}Php {totalAmount.toLocaleString("en-US")}</h5>
                </Card.Footer>
                <Card.Footer>
                    <div class="row pull-right mr-1 justify-align-center align-items-center">
                        <Button style={{borderRadius: "2rem"}} onClick={placeOrder} className=" ml-3 my-2 p-2" variant="danger" size="sm" disabled={isDisabledPlace}>
                            Place Order 
                        </Button> {/*disabled to habang walang payment at delivery options*/}
                    </div>
                </Card.Footer>
            </Card>
        {/*Add New Address Modal*/}
                    <Modal centered show={showAdd} onHide={closeAdd} backdrop="static"
        keyboard={false}>
                        <Form onSubmit={ (e)=> addNew(e) }>
                            <Modal.Header>
                                <Modal.Title>New Address</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                            <Row>
                            <Col md>
                            <Form.Group>
                            <Form.Control type="text" placeholder="Full Name" 
                            maxLength={50} value={fullName}
                                            onChange={(e)=> setFullName(e.target.value)}
                                            required/>
                                              </Form.Group>
                            </Col>
                            <Col md>
                            <Form.Group>
                            <Form.Control className="phone" type="number" placeholder="Mobile Number" value={mobileNo}
                                            onChange={(e)=> numberOnly(e)}
                                            required/>
                                            </Form.Group>
                       </Col>
                       </Row>
                      {/*<Form.Control as="select">...</Form.Control>*/}
                      <Row className="py-3">
                        <Col md>
                         
                         <Form.Group>
                      <Form.Control as="select" /*value={1}*/ onChange={province} onSelect={region}>
                          <option>Select Region</option>
                          {
                              regionData && regionData.length > 0 && regionData.map((item) => <option
                                  key={item.region_code} value={item.region_code}>{item.region_name}</option>)
                          }
                      </Form.Control>
                      </Form.Group>
                      </Col>
                      <Col md id="provinceSelect" style={{display: 'none' }}>
                      <Form.Group>
                      <Form.Control as="select" onChange={city}>
                          <option>Select Province</option>
                          {

                              provinceData && provinceData.length > 0 && provinceData.map((item) => <option
                                  key={item.province_code} value={item.province_code}>{item.province_name}</option>)
                          }
                      </Form.Control>
                      </Form.Group>
                      </Col>
                       </Row>
                      <Row>
                    <Col md id="citySelect" style={{display: 'none' }}>
                    <Form.Group>
                      <Form.Control as="select" onChange={barangay}>
                          <option>Select City</option>
                          {
                              cityData && cityData.length > 0 && cityData.map((item) => <option
                                  key={item.city_code} value={item.city_code}>{item.city_name}</option>)
                          }
                      </Form.Control>
                      </Form.Group>
                      </Col>
                      <Col md id="barSelect" style={{display: 'none' }}>
                      <Form.Group>
                      <Form.Control as="select" onChange={brgy}>
                          <option>Select Barangay</option>
                          {
                              barangayData && barangayData.length > 0 && barangayData.map((item) => <option
                                  key={item.brgy_code} value={item.brgy_code}>{item.brgy_name}</option>)
                          }
                      </Form.Control>
                      </Form.Group>
                      </Col>
                       </Row>
                       <div class="py-3">
                       <Form.Group>
                       <Form.Control type="text" maxLength={100} placeholder="Street Name, Building, House No." value={street}
                                            onChange={(e)=> setStreet(e.target.value)}
                                            required/>
                                            </Form.Group>
                       </div>
                      
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={closeToCart}>Cancel</Button>
                                <Button variant="success" type="submit" disabled={isDisabled}>Submit</Button>
                            </Modal.Footer>
                        </Form>
                    </Modal>
		</Container>


		)
}
